/*
 * Copyright (C) 2010 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zxing.decoding;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.ReaderException;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.zxing.ability.CaptureAbility;
import com.zxing.camera.CameraManager;
import com.zxing.camera.PlanarYUVLuminanceSource;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

import java.util.Hashtable;

final class DecodeHandler extends EventHandler {

	private static final String TAG = DecodeHandler.class.getSimpleName();

	private final CaptureAbility activity;
	private final MultiFormatReader multiFormatReader;

	DecodeHandler(CaptureAbility activity, Hashtable<DecodeHintType, Object> hints) {
		super(EventRunner.getMainEventRunner());
		multiFormatReader = new MultiFormatReader();
		multiFormatReader.setHints(hints);
		this.activity = activity;
	}

	@Override
	protected void processEvent(InnerEvent event) {
		super.processEvent(event);

		if (event.eventId != 0) {
			decode((byte[]) event.object, (int)event.object, (int)event.object);
		}
	}


	/**
	 * Decode the data within the viewfinder rectangle, and time how long it took. For efficiency,
	 * reuse the same reader objects from one decode to the next.
	 *
	 * @param data   The YUV preview frame.
	 * @param width  The width of the preview frame.
	 * @param height The height of the preview frame.
	 */
	private void decode(byte[] data, int width, int height) {
		long start = System.currentTimeMillis();
		Result rawResult = null;

		//modify here
		byte[] rotatedData = new byte[data.length];
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++)
				rotatedData[x * height + height - y - 1] = data[x + y * width];
		}
		int tmp = width; // Here we are swapping, that's the difference to #11
		width = height;
		height = tmp;

		PlanarYUVLuminanceSource source = CameraManager.get().buildLuminanceSource(rotatedData, width, height);
		BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
		try {
			rawResult = multiFormatReader.decodeWithState(bitmap);
		} catch (ReaderException re) {
			// continue
			try { //解决条形码与预览框的水平倾角超过30%识别不了，感谢群友 alert(3339238074) 的贡献
				source = CameraManager.get().buildLuminanceSource(data, height, width);
				bitmap = new BinaryBitmap(new HybridBinarizer(source));
				rawResult = multiFormatReader.decodeWithState(bitmap);
			} catch (Exception e) {
				multiFormatReader.reset();
			}
		} finally {
			multiFormatReader.reset();
		}

	}

}
