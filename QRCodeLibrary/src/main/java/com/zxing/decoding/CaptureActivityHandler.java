/*
 * Copyright (C) 2008 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zxing.decoding;

import java.util.Vector;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.zxing.ability.CaptureAbility;
import com.zxing.camera.CameraManager;
import com.zxing.utils.Log;
import com.zxing.view.ViewfinderResultPointCallback;
import com.zxing.view.ViewfinderView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.media.image.PixelMap;
import ohos.utils.net.Uri;

/**
 * This class handles all the messaging which comprises the state machine for capture.
 */
public final class CaptureActivityHandler extends EventHandler {
	private static final String TAG = CaptureActivityHandler.class.getSimpleName();

	/**
	 * 扫描回调
	 */
	public interface DecodeCallback {
		void handleDecode(Result result, PixelMap barcode);
		void drawViewfinder();
	}
	
	private State state;
	private enum State {
		PREVIEW,
		SUCCESS,
		DONE
	}

	private final Ability activity;
	private final DecodeThread decodeThread;
	private DecodeCallback decodeCallback;
	public CaptureActivityHandler(CaptureAbility activity, Vector<BarcodeFormat> decodeFormats,
								  String characterSet, ViewfinderView viewfinderView, DecodeCallback decodeCallback) {
		super(EventRunner.getMainEventRunner());
		this.activity = activity;
		this.decodeCallback = decodeCallback;
		
		decodeThread = new DecodeThread(activity, decodeFormats, characterSet,
				new ViewfinderResultPointCallback(viewfinderView));
		decodeThread.start();
		state = State.SUCCESS;
		// Start ourselves capturing previews and decoding.
		CameraManager.get().startPreview();
		restartPreviewAndDecode();
	}

	@Override
	protected void processEvent(InnerEvent event) {
		super.processEvent(event);
		if (event.eventId ==1) {
			// When one auto focus pass finishes, start another. This is the closest thing to
			// continuous AF. It does seem to hunt a bit, but I'm not sure what else to do.
			if (state == State.PREVIEW) {
				CameraManager.get().requestAutoFocus(this, 0);
			}
		}else if (event.eventId ==2) {
			Log.debug(TAG, "Got restart preview message");
			restartPreviewAndDecode();
		}else if (event.eventId ==3) {
			Log.debug(TAG, "Got decode succeeded message");
			state = State.SUCCESS;

			decodeCallback.handleDecode((Result) event.object, null);//���ؽ��
			/***********************************************************************/
		}else if (event.eventId ==4) {
			// We're decoding as fast as possible, so when one decode fails, start another.
			state = State.PREVIEW;
			CameraManager.get().requestPreviewFrame(decodeThread.getHandler(), 0);
		}else if (event.eventId ==5) {
			Log.debug(TAG, "Got return scan result message");
			activity.setResult(0, (Intent) event.object);
			activity.terminateAbility();
		}else if (event.eventId ==6) {
			Log.debug(TAG, "Got product query message");
			String url = (String) event.object;
			Intent intent = new Intent();
			activity.startAbility(intent);
		}
	}


	/**
	 * 退出
	 */
	public void quitSynchronously() {
		state = State.DONE;
		CameraManager.get().stopPreview();
		try {
			decodeThread.join();
		} catch (InterruptedException e) {
			// continue
		}

		// Be absolutely sure we don't send any queued up messages
		removeAllEvent();
	}

	private void restartPreviewAndDecode() {
		if (state == State.SUCCESS) {
			state = State.PREVIEW;
			CameraManager.get().requestPreviewFrame(decodeThread.getHandler(), 1);
			CameraManager.get().requestAutoFocus(this, 2);
			decodeCallback.drawViewfinder();
		}
	}

}
