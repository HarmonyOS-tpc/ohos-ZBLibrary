package com.zxing.utils;

import ohos.app.Context;
import ohos.global.resource.ResourceManager;

/**
 * Created by aaron on 16/8/3.
 */
public class DisplayUtil {
    /**
     * 屏幕宽 px
     */
    public static int screenWidthPx;
    /**
     * 屏幕高 px
     */
    public static int screenHeightPx;
    /**
     * 屏幕密度
     */
    public static float density;
    /**
     * 屏幕密度
     */
    public static int densityDPI;
    /**
     * vp单位
     */
    public static float screenWidthVp;
    /**
     * vp单位
     */
    public static float screenHeightVp;

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     * @param context 上下文
     * @param vp 数值
     * @return 转成px数值
     */
    public static int vp2px(Context context, int vp) {
        return Math.round(vp * context.getResourceManager().getDeviceCapability().screenDensity);
    }

    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     * @param context 上下文
     * @param pxValue 数值
     * @return 转成vp数值
     */
    public static int px2vp(Context context, int pxValue) {
        ResourceManager resourceManager = context.getResourceManager();
        return Math.round(pxValue / resourceManager.getDeviceCapability().screenDensity);
    }
}
