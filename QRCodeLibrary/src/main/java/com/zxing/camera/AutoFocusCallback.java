/*
 * Copyright (C) 2010 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zxing.camera;

import com.zxing.utils.Log;
import ohos.eventhandler.EventHandler;
import ohos.media.camera.device.Camera;


final class AutoFocusCallback {

  private static final String TAG = AutoFocusCallback.class.getSimpleName();

  private static final int AUTOFOCUS_INTERVAL_MS = 1500;

  private EventHandler autoFocusHandler;
  private int autoFocusMessage;

  void setHandler(EventHandler autoFocusHandler, int autoFocusMessage) {
    this.autoFocusHandler = autoFocusHandler;
    this.autoFocusMessage = autoFocusMessage;
  }

  /**
   * 目前鸿蒙已经有自动聚焦功能
   * @param success 成功
   * @param camera camera示例
   */
  public void onAutoFocus(boolean success, Camera camera) {
    if (autoFocusHandler != null) {

      autoFocusHandler.sendEvent(AUTOFOCUS_INTERVAL_MS);
      autoFocusHandler = null;
    } else {
      Log.debug(TAG, "Got auto-focus callback, but no handler for it");
    }
  }

}
