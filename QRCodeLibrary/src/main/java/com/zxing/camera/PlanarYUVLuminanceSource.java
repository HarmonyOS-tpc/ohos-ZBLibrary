/*
 * Copyright 2009 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zxing.camera;

import com.google.zxing.LuminanceSource;

import ohos.media.image.PixelMap;
import ohos.media.image.common.Rect;

/**
 * This object extends LuminanceSource around an array of YUV data returned from the camera driver,
 * with the option to crop to a rectangle within the full data. This can be used to exclude
 * superfluous pixels around the perimeter and speed up decoding.
 *
 * It works for any pixel format where the Y channel is planar and appears first, including
 * YCbCr_420_SP and YCbCr_422_SP.
 *
 * @author dswitkin@google.com (Daniel Switkin)
 */
public final class PlanarYUVLuminanceSource extends LuminanceSource {
  private byte[] bitmapPixels;

  private  byte[] yuvData;
  private  int dataWidth;
  private  int dataHeight;
  private  int left;
  private  int top;

  public PlanarYUVLuminanceSource(byte[] yuvData, int dataWidth, int dataHeight, int left, int top,
                                  int width, int height) {
    super(width, height);

    if (left + width > dataWidth || top + height > dataHeight) {
      throw new IllegalArgumentException("Crop rectangle does not fit within image data.");
    }

    this.yuvData = yuvData;
    this.dataWidth = dataWidth;
    this.dataHeight = dataHeight;
    this.left = left;
    this.top = top;
  }

  /**
   * 生成二维码
   * @param pixelMap 图片
   */
  public PlanarYUVLuminanceSource(PixelMap pixelMap) {
    super(pixelMap.getImageInfo().size.width, pixelMap.getImageInfo().size.height);

    // 首先，要取得该图片的像素数组内容
    int[] data = new int[getWidth() * getHeight()];
    this.bitmapPixels = new byte[getWidth() * getHeight()];
    pixelMap.readPixels(data, 0, getWidth(), new Rect(0, 0, getWidth(), getHeight()));
    // 将int数组转换为byte数组，也就是取像素值中蓝色值部分作为辨析内容
    for (int i = 0; i < data.length; i++) {
      this.bitmapPixels[i] = (byte) data[i];
    }
  }

  @Override
  public byte[] getMatrix() {
    // 返回我们生成好的像素数据
    return bitmapPixels;
  }

  @Override
  public byte[] getRow(int y, byte[] row) {
    // 这里要得到指定行的像素数据
    System.arraycopy(bitmapPixels, y * getWidth(), row, 0, getWidth());
    return row;
  }




  @Override
  public boolean isCropSupported() {
    return true;
  }

  public int getDataWidth() {
    return dataWidth;
  }

  public int getDataHeight() {
    return dataHeight;
  }

  public PixelMap renderCroppedGreyscaleBitmap() {
    int width = getWidth();
    int height = getHeight();
    int[] pixels = new int[width * height];
    byte[] yuv = yuvData;
    int inputOffset = top * dataWidth + left;

    for (int y = 0; y < height; y++) {
      int outputOffset = y * width;
      for (int x = 0; x < width; x++) {
        int grey = yuv[inputOffset + x] & 0xff;
        pixels[outputOffset + x] = 0xFF000000 | (grey * 0x00010101);
      }
      inputOffset += dataWidth;
    }

    PixelMap bitmap = PixelMap.create(null);
    return bitmap;
  }
}
