package com.zxing;

import com.zxing.utils.DisplayUtil;

import ohos.aafwk.ability.AbilityPackage;
import ohos.global.configuration.DeviceCapability;

/**
 * package
 */
public class ZxingApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        initDisplayOpinion();
    }

    private void initDisplayOpinion() {
        DeviceCapability dm = getResourceManager().getDeviceCapability();
        DisplayUtil.density = dm.screenDensity;
        DisplayUtil.screenWidthPx = dm.width;
        DisplayUtil.screenHeightPx = dm.height;
        DisplayUtil.screenWidthVp = DisplayUtil.px2vp(getApplicationContext(), dm.width);
        DisplayUtil.screenHeightVp = DisplayUtil.px2vp(getApplicationContext(), dm.height);
    }
}
