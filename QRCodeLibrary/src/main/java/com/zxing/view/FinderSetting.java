package com.zxing.view;

import java.io.Serializable;

/**
 * 设置model
 */
public class FinderSetting implements Serializable {
    // 控制扫描框的宽度
    private int frameSize;
    // 控制扫描框距离顶部的距离
    private int marginTop = -1;
    // 控制扫描框四角的颜色
    private int cornerColor;

    // 控制扫描框四角的长度
    private int cornerHeight;
    // 控制扫描框四角的宽度
    private int cornerWidth;
    // 控制扫描框四角的宽度
    private int scanSpeed;
    // 控制扫描框四角的宽度
    private int scanBitmap;

    public FinderSetting() {}

    public int getFrameSize() {
        return frameSize;
    }

    public void setFrameSize(int width) {
        this.frameSize = width;
    }

    public int getMarginTop() {
        return marginTop;
    }

    public void setMarginTop(int marginTop) {
        this.marginTop = marginTop;
    }

    public int getCornerColor() {
        return cornerColor;
    }

    public void setCornerColor(int cornerColor) {
        this.cornerColor = cornerColor;
    }

    public int getCornerHeight() {
        return cornerHeight;
    }

    public void setCornerHeight(int cornerHeight) {
        this.cornerHeight = cornerHeight;
    }

    public int getCornerWidth() {
        return cornerWidth;
    }

    public void setCornerWidth(int cornerWidth) {
        this.cornerWidth = cornerWidth;
    }

    public int getScanSpeed() {
        return scanSpeed;
    }

    public void setScanSpeed(int scanSpeed) {
        this.scanSpeed = scanSpeed;
    }

    public int getScanBitmap() {
        return scanBitmap;
    }

    public void setScanBitmap(int scanBitmap) {
        this.scanBitmap = scanBitmap;
    }
}
