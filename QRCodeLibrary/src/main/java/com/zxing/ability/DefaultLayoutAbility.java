package com.zxing.ability;
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import static ohos.media.camera.device.Camera.FrameConfigType.FRAME_CONFIG_PREVIEW;
import static ohos.media.camera.params.Metadata.FlashMode.*;

import com.zxing.ResourceTable;
import com.zxing.slice.CodeUtils;
import com.zxing.utils.Log;
import com.zxing.view.FinderSetting;
import com.zxing.view.ViewfinderView;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.Text;
import ohos.agp.components.surfaceprovider.SurfaceProvider;
import ohos.agp.graphics.Surface;
import ohos.agp.graphics.SurfaceOps;
import ohos.app.Environment;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.global.resource.RawFileEntry;
import ohos.media.camera.CameraKit;
import ohos.media.camera.device.*;
import ohos.media.image.Image;
import ohos.media.image.ImageReceiver;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.ImageFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;
import ohos.media.player.Player;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.UUID;

/**
 * 二维码扫描 ability
 */
public class DefaultLayoutAbility extends Ability implements ImageReceiver.IImageArrivalListener, Component.ClickedListener {
    private static final int SCREEN_WIDTH = 2340;
    private static final int SCREEN_HEIGHT = 1080;
    private static final int IMAGE_RCV_CAPACITY = 5;
    private SurfaceProvider surfaceProvider;
    private ImageReceiver imageReceiver;
    private Camera cameraDevice;
    private Surface previewSurface;
    private EventHandler creamEventHandler;
    private Text txtClose;
    Text txtOpenFlash;
    private boolean isOpen = true;
    private FrameConfig.Builder framePreviewConfigBuilder;
    private FinderSetting setting;
    private CodeUtils.AnalyzeCallback analyzeCallback;
    private boolean isShowTopBar = true;
    private DependentLayout topBar;
    private boolean isSound = true;
    private Player player;

    /**
     * 扫描监听
     * @param analyzeCallback 回调
     */
    public void setResultCallback(CodeUtils.AnalyzeCallback analyzeCallback) {
        this.analyzeCallback = analyzeCallback;
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_capture);

        setting = intent.getSerializableParam("setting");
        isShowTopBar = intent.getBooleanParam("isShowTopBar", true);
        isSound = intent.getBooleanParam("isSound", true);

        txtOpenFlash = (Text) findComponentById(ResourceTable.Id_txt_open_flash);
        txtClose = (Text) findComponentById(ResourceTable.Id_txt_close);
        topBar = (DependentLayout) findComponentById(ResourceTable.Id_top_bar);

        topBar.setVisibility(isShowTopBar ? Component.VISIBLE : Component.HIDE);

        getWindow().setTransparent(true);
        if (creamEventHandler == null) {
            creamEventHandler = new EventHandler(EventRunner.create("CameraBackground"));
        }
        if (isSound) {
            initBeepSound();
        }

        initSurface();

        txtOpenFlash.setClickedListener(this);
        txtClose.setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        if (component.getId() == ResourceTable.Id_txt_open_flash) {
            if (isOpen) {
                isOpen = false;
                txtOpenFlash.setText("关闭闪光灯");
                openFlashlight();
            } else {
                isOpen = true;
                txtOpenFlash.setText("打开闪光灯");
                closeFlashlight();
            }
        } else if (component.getId() == ResourceTable.Id_txt_close) {
            terminateAbility();
        }
    }

    /**
     * 打开或关闭闪关灯
     *
     * @param on 是否打开
     */
    protected void switchLight(boolean on) {
        if (on) {
            openFlashlight();
        } else {
            closeFlashlight();
        }
    }

    void openFlashlight() {
        if (framePreviewConfigBuilder != null&&cameraDevice !=null) {
            framePreviewConfigBuilder.setFlashMode(FLASH_ALWAYS_OPEN);
            cameraDevice.triggerLoopingCapture(framePreviewConfigBuilder.build());
        }

    }

    void closeFlashlight() {
        if (framePreviewConfigBuilder != null&&cameraDevice !=null) {
            framePreviewConfigBuilder.setFlashMode(FLASH_CLOSE);
            cameraDevice.triggerLoopingCapture(framePreviewConfigBuilder.build());
        }
    }

    private ViewfinderView viewfinderView;

    private void initSurface() {
        if (surfaceProvider != null) {
            return;
        }
        DependentLayout dependentLayout = (DependentLayout) findComponentById(ResourceTable.Id_surface_container);
        surfaceProvider = new SurfaceProvider(this);
        dependentLayout.addComponent(
                surfaceProvider,
                new DependentLayout.LayoutConfig(
                        ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT));
        if (setting != null) {
            viewfinderView = new ViewfinderView(this, setting);
        } else {
            viewfinderView = new ViewfinderView(this);
        }
        dependentLayout.addComponent(
                viewfinderView,
                new DependentLayout.LayoutConfig(
                        ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT));
        surfaceProvider.pinToZTop(false);
        if (surfaceProvider != null) {
            surfaceProvider.getSurfaceOps().get().addCallback(new SurfaceCallBack());
        }
    }

    private boolean isAnalyze = false;

    @Override
    public void onImageArrival(ImageReceiver imageReceiver) {
        try {
            Image image = imageReceiver.readLatestImage();
            if (image == null) {
                return;
            }
            if (isAnalyze) {
                image.release();
                return;
            }
            isAnalyze = true;
            Image.Component component = image.getComponent(ImageFormat.ComponentType.JPEG);
            byte[] bytes = new byte[component.remaining()];
            ByteBuffer buffer = component.getBuffer();
            buffer.get(bytes);
            ImageSource imageSource = ImageSource.create(bytes, new ImageSource.SourceOptions());

            ImageSource.DecodingOptions options = new ImageSource.DecodingOptions();
            options.rotateDegrees = 90f;
            options.desiredRegion =
                    new Rect(
                            viewfinderView.getFramingRect().top,
                            viewfinderView.getFramingRect().left,
                            viewfinderView.getFramingRect().getHeight(),
                            viewfinderView.getFramingRect().getWidth());
            PixelMap map = imageSource.createPixelmap(options);
            saveImage(bytes);
            String result = CodeUtils.parseInfoFromBitmap(map);
            Log.debug("result=" + result);
            if (result != null) {
                showMap(map, result);
                return;
            }
            image.release();
            isAnalyze = false;

        } catch (Exception e) {
            Log.debug(e.getMessage());
        }
    }

    private int time = 0;

    private static final String IMG_FILE_PREFIX = "IMG_";

    private static final String IMG_FILE_TYPE = ".jpg";

    private void saveImage(byte[] bitmapPixels) {
        FileOutputStream output = null;
        try {
            String fileName = IMG_FILE_PREFIX + UUID.randomUUID() + IMG_FILE_TYPE;
            File targetFile = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), fileName);
            output = new FileOutputStream(targetFile);
            output.write(bitmapPixels);
        } catch (IOException e) {
            Log.debug("saveImage ERROR =" + e.getMessage());
        } finally {
            try {
                if (output != null) {
                    output.close();
                }

            } catch (IOException e) {
                Log.error(e.getMessage());
            }
        }
    }

    private void initBeepSound() {
        try {
            player = new Player(this);

            String data = getResourceManager().getMediaPath(ResourceTable.Media_beep);
            RawFileEntry fileEntry = getResourceManager().getRawFileEntry(data);
            player.setSource(fileEntry.openRawFileDescriptor());

            player.setVolume(0.10f);
            player.prepare();
        } catch (Exception e) {
            Log.error(e.getMessage());
        }
    }

    private void showMap(PixelMap map, String result) {
        if (isSound && player != null) {
            player.play();
        }

        if (analyzeCallback != null) {
            analyzeCallback.onAnalyzeSuccess(map, result);
        }
        Intent intent = new Intent();
        intent.setParam("result", result);
        setResult(1, intent);
        terminateAbility();
    }

    /**
     * SurfaceCallBack
     *
     * @since 2020-09-03
     */
    class SurfaceCallBack implements SurfaceOps.Callback {
        @Override
        public void surfaceCreated(SurfaceOps callbackSurfaceOps) {
            if (callbackSurfaceOps != null) {
                callbackSurfaceOps.setFixedSize(SCREEN_WIDTH,SCREEN_HEIGHT );
            }
            openCamera();
        }

        @Override
        public void surfaceChanged(SurfaceOps callbackSurfaceOps, int format, int width, int height) {}

        @Override
        public void surfaceDestroyed(SurfaceOps callbackSurfaceOps) {}
    }

    private Size getOptimalSize(CameraKit cameraKit, String camId, int screenWidth, int screenHeight) {
        List<Size> sizes = cameraKit.getCameraAbility(camId).getSupportedSizes(ImageFormat.YUV420_888);
        final double aspect_tolerance = 0.1;
        //竖屏screenHeight/screenWidth，横屏是screenWidth/screenHeight
        double targetRatio = (double) screenHeight / screenWidth;
        Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;
        int targetHeight = screenWidth;
        for (Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }
        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    private void openCamera() {
        imageReceiver = ImageReceiver.create(SCREEN_WIDTH, SCREEN_HEIGHT, ImageFormat.JPEG, IMAGE_RCV_CAPACITY);
        imageReceiver.setImageArrivalListener(this);
        CameraKit cameraKit = CameraKit.getInstance(getApplicationContext());
        String[] cameraList = cameraKit.getCameraIds();
        String cameraId = null;
        for (String id : cameraList) {
            if (cameraKit.getCameraInfo(id).getFacingType() == CameraInfo.FacingType.CAMERA_FACING_BACK) {
                cameraId = id;
            }
        }
        if (cameraId == null) {
            return;
        }
        CameraStateCallbackImpl cameraStateCallback = new CameraStateCallbackImpl();
        cameraKit.createCamera(cameraId, cameraStateCallback, creamEventHandler);
    }

    class CameraStateCallbackImpl extends CameraStateCallback {
        CameraStateCallbackImpl() {}

        @Override
        public void onCreated(Camera camera) {
            Log.debug("create camera onCreated");
            Log.debug("surfaceProvider==null  =" + (surfaceProvider));
            if (surfaceProvider == null) {
                return;
            }
            previewSurface = surfaceProvider.getSurfaceOps().get().getSurface();
            if (previewSurface == null) {
                Log.debug("create camera filed, preview surface is null");
                return;
            }
            // Wait until the preview surface is created.
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                Log.debug("Waiting to be interrupted");
            }
            CameraConfig.Builder cameraConfigBuilder = camera.getCameraConfigBuilder();
            cameraConfigBuilder.addSurface(previewSurface);
            cameraConfigBuilder.addSurface(imageReceiver.getRecevingSurface());
            camera.configure(cameraConfigBuilder.build());
            cameraDevice = camera;

            framePreviewConfigBuilder = camera.getFrameConfigBuilder(FRAME_CONFIG_PREVIEW);
            framePreviewConfigBuilder.addSurface(imageReceiver.getRecevingSurface());
            framePreviewConfigBuilder.addSurface(previewSurface);
            camera.triggerLoopingCapture(framePreviewConfigBuilder.build());
        }

        @Override
        public void onConfigured(Camera camera) {
            Log.debug("onConfigured....");

            framePreviewConfigBuilder.setFlashMode(FLASH_OPEN);
            framePreviewConfigBuilder.addSurface(previewSurface);

            camera.triggerLoopingCapture(framePreviewConfigBuilder.build());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (cameraDevice != null) {
            framePreviewConfigBuilder = null;
            try {
                cameraDevice.release();
                cameraDevice = null;
                surfaceProvider.clearFocus();
                surfaceProvider.removeFromWindow();
                surfaceProvider = null;
            } catch (Exception e) {
                Log.debug( e.getMessage());
            }
        }
    }
}
