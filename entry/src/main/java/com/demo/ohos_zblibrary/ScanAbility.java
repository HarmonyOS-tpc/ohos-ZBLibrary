package com.demo.ohos_zblibrary;

import com.demo.ohos_zblibrary.tool.IntentUtil;
import com.zxing.ability.CaptureAbility;
import com.zxing.slice.CodeUtils;
import com.zxing.view.ViewfinderView;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.surfaceprovider.SurfaceProvider;
import ohos.media.image.PixelMap;
import zuo.biao.library.util.CommonUtil;

public class ScanAbility extends CaptureAbility implements Component.ClickedListener {

    private DependentLayout dependentLayout;
    private DirectionalLayout back,light, toqrcode;
    private boolean isOn;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_scan);
        dependentLayout = (DependentLayout) findComponentById(ResourceTable.Id_dependentLayout);
        back = (DirectionalLayout) findComponentById(ResourceTable.Id_back);
        light = (DirectionalLayout) findComponentById(ResourceTable.Id_light);
        toqrcode = (DirectionalLayout) findComponentById(ResourceTable.Id_toqrcode);

        SurfaceProvider  surfaceProvider = new SurfaceProvider(this);
        back.setClickedListener(this);
        light.setClickedListener(this);
        toqrcode.setClickedListener(this);

        dependentLayout.addComponent(
                surfaceProvider,
                new DependentLayout.LayoutConfig(
                        ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT));
        ViewfinderView viewfinderView = new ViewfinderView(this);
        dependentLayout.addComponent(
                viewfinderView,
                new DependentLayout.LayoutConfig(
                        ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT));
        init(this, surfaceProvider, viewfinderView);
        setResultCallback(new CodeUtils.AnalyzeCallback() {
            @Override
            public void onAnalyzeSuccess(PixelMap mBitmap, String result) {
                CommonUtil.showShortToast(getContext(), result);
            }

            @Override
            public void onAnalyzeFailed() {

            }
        });

    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_back:
                terminateAbility();
                break;
                case ResourceTable.Id_light:
                    if (isOn) {
                        isOn = false;
                    }else {
                        isOn = true;
                    }
                    switchLight(isOn);
                break;
                case ResourceTable.Id_toqrcode:
                    IntentUtil.toActivity(getContext(), "QRCodeAbility");
                break;
        }
    }
}
