

package com.demo.ohos_zblibrary.adapter;

import com.demo.ohos_zblibrary.ResourceTable;

import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;


/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * 主要serviceItemProvider extends BaseItemProvider
 *
 */
public class MainItemProvider extends BaseItemProvider {
    private Context context;

    private List<String> items;

    public MainItemProvider(Context context, List<String> itemList) {
        this.context = context;
        items = itemList;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        return getRootView(position);
    }

    private Component getRootView(int position) {
        Component rootView = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_list_mathod, null, false);
        Text txtName = (Text) rootView.findComponentById(ResourceTable.Id_txt_name);
        txtName.setText(items.get(position));

        return rootView;
    }
}
