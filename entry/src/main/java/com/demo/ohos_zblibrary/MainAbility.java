package com.demo.ohos_zblibrary;

import com.demo.ohos_zblibrary.slice.MainAbilitySlice;
import com.demo.ohos_zblibrary.slice.PageSliderAbilitySlice;
import com.demo.ohos_zblibrary.slice.SettingAbilitySlice;
import com.demo.ohos_zblibrary.slice.ThreadPoolAbilitySlice;
import com.demo.ohos_zblibrary.tool.IntentUtil;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.DirectionalLayout;
import ohos.bundle.IBundleManager;

/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * 主页
 */
public class MainAbility extends Ability  {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        permission();

        addActionRoute(IntentUtil.HOMESLICE, ThreadPoolAbilitySlice.class.getName());
        addActionRoute(IntentUtil.MAINSLICE, MainAbilitySlice.class.getName());
        addActionRoute(IntentUtil.PAGESLIDERABILITYSLICE, PageSliderAbilitySlice.class.getName());
        addActionRoute(IntentUtil.SETTINGSLICE, SettingAbilitySlice.class.getName());
    }

    private void permission() {
        if (verifySelfPermission("ohos.permission.CAMERA") != IBundleManager.PERMISSION_GRANTED
                && verifySelfPermission("ohos.permission.WRITE_MEDIA") != IBundleManager.PERMISSION_GRANTED
                && verifySelfPermission("ohos.permission.LOCATION") != IBundleManager.PERMISSION_GRANTED
                && verifySelfPermission("ohos.permission.READ_MEDIA") != IBundleManager.PERMISSION_GRANTED) {
            // 应用未被授予权限
            if (canRequestPermission("ohos.permission.CAMERA")
                    || canRequestPermission("ohos.permission.READ_MEDIA")
                    || canRequestPermission("ohos.permission.WRITE_MEDIA")) {
                // 是否可以申请弹框授权(首次申请或者用户未选择禁止且不再提示)
                requestPermissionsFromUser(
                        new String[] {
                            "ohos.permission.CAMERA",
                            "ohos.permission.READ_MEDIA",
                            "ohos.permission.WRITE_MEDIA",
                            "ohos.permission.LOCATION"
                        },
                        123);
            }
        }
    }
}
