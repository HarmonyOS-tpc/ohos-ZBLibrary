package com.demo.ohos_zblibrary.tool;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.app.Context;
import zuo.biao.library.base.BaseAbility;

/**
 * 跳转封装
 */
public class IntentUtil {
    /**
     * 主要main MainAbilitySlice
     */
    public static final String MAINSLICE = "action.MainAbilitySlice";

    /**
     *主要 pageslider PageSliderAbilitySlice
     */
    public static final String PAGESLIDERABILITYSLICE = "action.PageSliderAbilitySlice";

    /**
     * 主要hone HomeAbilitySlice
     */
    public static final String HOMESLICE = "action.HomeAbilitySlice";

    /**
     * 主要setting SettingAbilitySlice
     */
    public static final String SETTINGSLICE = "action.SettingAbilitySlice";

    /**
     * 跳转封装
     * @param context 上下文
     * @param pathName 路径name
     */
    public static void toActivity(Context context, String pathName) {
        String bundleName = context.getBundleName();
        Intent intent = new Intent();
        Operation operationBuilder =
                new Intent.OperationBuilder()
                        .withAbilityName("com.demo.ohos_zblibrary." + pathName)
                        .withDeviceId("")
                        .withFlags(Intent.FLAG_ABILITY_NEW_MISSION)
                        .withBundleName(bundleName)
                        .build();

        intent.setOperation(operationBuilder);
        BaseAbility.toActivity(context,intent, 12);
    }

    /**
     * 传code
     * @param context c
     * @param pathName 路径名
     * @param RequestCode 请求码
     */
    public static void toActivity(Ability context, String pathName, int RequestCode) {
        String bundleName = context.getBundleName();
        Intent intent = new Intent();
        Operation operationBuilder =
                new Intent.OperationBuilder()
                        .withAbilityName("com.demo.ohos_zblibrary." + pathName)
                        .withDeviceId("")
                        .withBundleName(bundleName)
                        .build();

        intent.setOperation(operationBuilder);
        BaseAbility.toActivity(context,intent, RequestCode);
    }


    /**
     * 无code 跳转slice
     * @param context c
     * @param action action
     */
    public static void toSlice(AbilitySlice context, String action) {
        Intent intent1 = new Intent();
        Operation operation =
                new Intent.OperationBuilder()
                        .withFlags(Intent.FLAG_ABILITYSLICE_FORWARD_RESULT)
                        .withAction(action)
                        .build();
        intent1.setOperation(operation);
        context.startAbility(intent1);
    }
}
