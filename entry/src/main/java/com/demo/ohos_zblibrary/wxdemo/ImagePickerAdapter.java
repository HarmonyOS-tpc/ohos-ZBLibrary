package com.demo.ohos_zblibrary.wxdemo;

import com.demo.ohos_zblibrary.ResourceTable;
import com.lzy.imagepicker.ImagePicker;
import com.lzy.imagepicker.bean.ImageItem;
import com.lzy.imagepicker.view.ViewHolder;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 微信图片选择的Adapter
 */
public class ImagePickerAdapter extends RecycleItemProvider {
    private int maxImgCount;
    private Context mContext;
    private List<ImageItem> mData;
    private LayoutScatter mInflater;
    private OnRecyclerViewItemClickListener listener;
    private boolean isAdded; // 是否额外添加了最后一个图片

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int pos) {
        return mData.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public Component getComponent(int pos, Component component, ComponentContainer componentContainer) {
        SelectedPicViewHolder viewHolder = onCreateViewHolder(componentContainer, 0);
        onBindViewHolder(viewHolder, pos);
        return viewHolder.getRootView();
    }

    /**
     *item 点击接口
     */
    public interface OnRecyclerViewItemClickListener {
        void onItemClick(Component view, int position);
    }

    /**
     * item 点击接口
     * @param listener l
     */
    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.listener = listener;
    }

    /**
     * 设置图片集合
     * @param data d
     */
    public void setImages(List<ImageItem> data) {
        mData = new ArrayList<>(data);
        if (getCount() < maxImgCount) {
            mData.add(new ImageItem());
            isAdded = true;
        } else {
            isAdded = false;
        }
        notifyDataChanged();
    }

    /**
     * 获取图片
     * @return 图片集合
     */
    public List<ImageItem> getImages() {
        // 由于图片未选满时，最后一张显示添加图片，因此这个方法返回真正的已选图片
        if (isAdded) {
            return new ArrayList<>(mData.subList(0, mData.size() - 1));
        } else{
            return mData;
        }
    }

    public ImagePickerAdapter(Context mContext, List<ImageItem> data, int maxImgCount) {
        this.mContext = mContext;
        this.maxImgCount = maxImgCount;
        this.mInflater = LayoutScatter.getInstance(mContext);
        setImages(data);
    }

    /**
     * 创建view
     * @param parent p
     * @param viewType v
     * @return 选择的图片view
     */
    public SelectedPicViewHolder onCreateViewHolder(ComponentContainer parent, int viewType) {
        return new SelectedPicViewHolder(mInflater.parse(ResourceTable.Layout_list_item_image, parent, false));
    }

    /**
     * 绑定的viewholder
     * @param holder h
     * @param position p
     */
    public void onBindViewHolder(SelectedPicViewHolder holder, int position) {
        holder.bind(position);
    }

    /**
     * 选择图片select
     */
    public class SelectedPicViewHolder extends ViewHolder implements Component.ClickedListener {
        private Image iv_img;
        private int clickPosition;

        public SelectedPicViewHolder(Component itemView) {
            iv_img = (Image) itemView.findComponentById(ResourceTable.Id_iv_img);
        }

        /**
         *绑定
         * @param position p
         */
        public void bind(int position) {
            // 设置条目的点击事件
            iv_img.setClickedListener(this);
            // 根据条目位置设置图片
            ImageItem item = mData.get(position);
            iv_img.setPixelMap(ResourceTable.Media_image_add_nor);
            if (isAdded && position == getCount() - 1) {
                iv_img.setComponentStateChangedListener(
                        new Component.ComponentStateChangedListener() {
                            @Override
                            public void onComponentStateChanged(Component component, int status) {
                                switch (status) {
                                    case ComponentState.COMPONENT_STATE_FOCUSED:
                                    case ComponentState.COMPONENT_STATE_PRESSED:
                                    case ComponentState.COMPONENT_STATE_SELECTED:
                                        iv_img.setPixelMap(ResourceTable.Media_image_add_sel);
                                        break;
                                    default:
                                        iv_img.setPixelMap(ResourceTable.Media_image_add_nor);
                                        break;
                                }
                            }
                        });
                clickPosition = WxDemoAbility.IMAGE_ITEM_ADD;
            } else {
                ImagePicker.getInstance()
                        .getImageLoader()
                        .displayImage((Ability) mContext, item.uriSchema, iv_img, 0, 0);
                clickPosition = position;
            }
        }

        @Override
        public void onClick(Component view) {
            if (listener != null){
                listener.onItemClick(view, clickPosition);
            }
        }

        @Override
        public Component getRootView() {
            return iv_img;
        }
    }
}
