package com.demo.ohos_zblibrary.imageloader;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.demo.ohos_zblibrary.ResourceTable;
import com.lzy.imagepicker.loader.ImageLoader;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.Image;
import ohos.utils.net.Uri;

/**
 * ================================================
 * glide 封装
 * ================================================
 */
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * glide使用封装
 */
public class GlideImageLoader implements ImageLoader {
    @Override
    public void displayImage(Ability ability, String path, Image imageView, int width, int height) {
        Glide.with(ability) // 配置上下文
                .load(Uri.parse(path)) // 设置图片路径(fix #8,文件名包含%符号 无法识别和显示)
                .error(ResourceTable.Media_icon) // 设置错误图片
                .placeholder(ResourceTable.Media_icon) // 设置占位图片
                .diskCacheStrategy(DiskCacheStrategy.ALL) // 缓存全尺寸
                .into(imageView);
    }

    @Override
    public void displayImagePreview(Ability ability, String path, Image imageView, int width, int height) {
        Glide.with(ability) // 配置上下文
                .load(Uri.parse(path)) // 设置图片路径(fix #8,文件名包含%符号 无法识别和显示)
                .diskCacheStrategy(DiskCacheStrategy.ALL) // 缓存全尺寸
                .into(imageView);
    }

    @Override
    public void clearMemoryCache() {}
}
