package com.demo.ohos_zblibrary.imageloader;

/**
 * ================================================
 * imageLoader 封装
 * ================================================
 */
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.lzy.imagepicker.loader.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.Image;

/**
 * imageLoader封装
 */
public class UILImageLoader implements ImageLoader {
    @Override
    public void displayImage(Ability ability, String uriScheme, Image imageView, int width, int height) {
        ImageSize size = new ImageSize(width, height);
        com.nostra13.universalimageloader.core.ImageLoader.getInstance().displayImage(uriScheme, imageView, size);
    }

    @Override
    public void displayImagePreview(Ability ability, String uriScheme, Image imageView, int width, int height) {
        ImageSize size = new ImageSize(width, height);
        com.nostra13.universalimageloader.core.ImageLoader.getInstance().displayImage(uriScheme, imageView, size);
    }

    @Override
    public void clearMemoryCache() {}
}
