/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
limitations under the License.*/

package com.demo.ohos_zblibrary.view;

import com.demo.ohos_zblibrary.ResourceTable;
import com.demo.ohos_zblibrary.imageloader.GlideImageLoader;
import com.demo.ohos_zblibrary.model.User;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import zuo.biao.library.base.BaseModel;
import zuo.biao.library.base.BaseView;
import zuo.biao.library.util.ResTUtil;
import zuo.biao.library.util.StringUtil;

/**用户View
 * @author Lemon
 * <br> adapter中使用:[具体参考.BaseViewAdapter(getView使用自定义View的写法)]
 * <br> convertView = userView.createView(inflater);
 * <br> userView.bindView(position, data);
 * <br> 或  其它类中使用:
 * <br> containerView.addView(userView.createView(inflater));
 * <br> userView.bindView(data);
 * <br> 然后
 * <br> userView.setOnDataChangedListener(onDataChangedListener);data = userView.getData();//非必需
 * <br> userView.setOnClickListener(onClickListener);//非必需
 */
public class UserView extends BaseView<User> implements Component.ClickedListener {
    private static final String TAG = "UserView";

    public UserView(Ability context, ComponentContainer parent) {
        super(context, ResourceTable.Layout_userview_layout, parent);
    }

    private Image ivUserViewHead;
    private Image ivUserViewStar;

    private Text tvUserViewSex;

    private Text tvUserViewName;
    private Text tvUserViewId;
    private Text tvUserViewNumber;

    @Override
    public Component createView() {
        ivUserViewHead = findView(ResourceTable.Id_ivUserViewHead, this);
        ivUserViewStar = findView(ResourceTable.Id_ivUserViewStar, this);

        tvUserViewSex = findView(ResourceTable.Id_tvUserViewSex, this);

        tvUserViewName = findView(ResourceTable.Id_tvUserViewName);
        tvUserViewId = findView(ResourceTable.Id_tvUserViewId);
        tvUserViewNumber = findView(ResourceTable.Id_tvUserViewNumber);

        return super.createView();
    }

    @Override
    public void bindView(User data_) {
        super.bindView(data_ != null ? data_ : new User());

        GlideImageLoader loader = new GlideImageLoader();
        loader.displayImage(context, data.getHead(), ivUserViewHead, 0, 0);

        ivUserViewStar.setImageElement(
                data.getStarred()
                        ? ResTUtil.getElement(context, ResourceTable.Media_star_light)
                        : ResTUtil.getElement(context, ResourceTable.Media_star));

        tvUserViewSex.setBackground(
                data.getSex() == User.SEX_FEMALE
                        ? ResTUtil.getElement(context, ResourceTable.Graphic_circle_pick)
                        : ResTUtil.getElement(context, ResourceTable.Graphic_circle_blue));
        tvUserViewSex.setText(data.getSex() == User.SEX_FEMALE ? "女" : "男");
        tvUserViewSex.setTextColor(
                new Color(
                        getColor(
                                data.getSex() == User.SEX_FEMALE
                                        ? ResourceTable.Color_pink
                                        : ResourceTable.Color_blue)));

        tvUserViewName.setText(StringUtil.getTrimedString(data.getName()));
        tvUserViewId.setText("ID:" + data.getId());
        tvUserViewNumber.setText("Phone:" + StringUtil.getNoBlankString(data.getPhone()));
    }

    @Override
    public void onClick(Component view) {
        if (BaseModel.isCorrect(data) == false) {
            return;
        }
        switch (view.getId()) {
            case ResourceTable.Id_ivUserViewHead:
                break;
            default:
                switch (view.getId()) {
                    case ResourceTable.Id_ivUserViewStar:
                        data.setStarred(!data.getStarred());
                        break;
                    case ResourceTable.Id_tvUserViewSex:
                        data.setSex(data.getSex() == User.SEX_FEMALE ? User.SEX_MAIL : User.SEX_FEMALE);
                        break;
                }
                bindView(data);
                break;
        }
    }
}
