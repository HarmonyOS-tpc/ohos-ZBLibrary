/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
limitations under the License.*/

package com.demo.ohos_zblibrary.view;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;
import ohos.app.Context;
import zuo.biao.library.base.BaseView;

/**BaseView布局类，可直接写在layout文件内
 * @author Lemon
 */
public class BaseViewLayout<T> extends StackLayout {
    private static final String TAG = "BaseViewLayout";

    private Ability context;

    public BaseViewLayout(Context context) {
        super(context);
        init((Ability) context);
    }

    public BaseViewLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init((Ability) context);
    }

    public BaseViewLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init((Ability) context);
    }

    /**
     * init初始化
     * @param context c
     */
    protected void init(Ability context) {
        this.context = context;
    }

    /**
     * 可使用bv
     */
    public BaseView<T> bv;

    /**
     * createView
     * @param bv bv
     */
    public void createView(BaseView<T> bv) {
        this.bv = bv;

        removeAllComponents();
        super.addComponent(bv.createView());
        bindView(null);
    }

    @Override
    public void addComponent(Component childComponent) {
        throw new UnsupportedOperationException(TAG + "不支持该方法");
    }

    /**
     * 绑定view
     * @param data d
     */
    public void bindView(T data) {
        bv.bindView(data);
    }
}
