/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
limitations under the License.*/

package com.demo.ohos_zblibrary.view;

import com.demo.ohos_zblibrary.model.User;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.AttrSet;
import ohos.app.Context;

/**UserView的布局类，可直接写在layout文件内。
 * *不如直接使用BaseViewLayout方便
 * @author Lemon
 * @use uvl = (BaseViewLayout<Model>) findViewById(R.id.uvl);
 *      uvl.bindView(model);
 */
public class UserViewLayout extends BaseViewLayout<User> {
    public UserViewLayout(Context context) {
        super(context);
    }

    public UserViewLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public UserViewLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    @Override
    protected void init(Ability context) {
        super.init(context);
        createView(new UserView(context, null));
    }
}
