package com.demo.ohos_zblibrary;

import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.window.dialog.ToastDialog;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import zuo.biao.library.base.BaseAbility;
import zuo.biao.library.interfaces.OnHttpResponseListener;
import zuo.biao.library.manager.HttpManager;
import zuo.biao.library.ui.LoadingDialog;
import zuo.biao.library.util.Log;

import java.util.Map;
import java.util.WeakHashMap;

/**
 * http 请求示例
 */
public class HttpRequestAbility extends BaseAbility {
    Text txt_msg;
    TextField txtField;
    private LoadingDialog loadingDialog;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_http_request);
        HttpManager.init(getContext());
        initView();
        initData();
        initEvent();
    }

    @Override
    public void initView() {
        txt_msg = (Text) findComponentById(ResourceTable.Id_txt_msg);
        txtField = (TextField) findComponentById(ResourceTable.Id_txtField);
        Button button = (Button) findComponentById(ResourceTable.Id_btn_click);
        button.setClickedListener(component -> startRequest());
        txtField.setText("18899999999");
    }

    @Override
    public void initData() {}

    private void startRequest() {
        String phone = txtField.getText();
        if (phone.length() != 11) {
            toastMag("请输入正确的手机号");
        }
        String url = "https://tcc.taobao.com/cc/json/mobile_tel_segment.htm?tel=" + phone;

        Map<String, Object> map = new WeakHashMap<>();
        loading();

        HttpManager.getInstance()
                .post(
                        map,
                        url,
                        123,
                        new OnHttpResponseListener() {
                            @Override
                            public void onHttpResponse(int requestCode, String resultJson, Exception e) {
                                Log.info("HttpManager", resultJson + "1");
                                settextInfo(resultJson);
                            }
                        });
    }

    private void settextInfo(String resultJson) {
        new EventHandler(EventRunner.getMainEventRunner())
                .postTask(
                        new Runnable() {
                            @Override
                            public void run() {
                                txt_msg.setText("请求归属结果：" + resultJson + "");

                                loadingDialog.hide();
                                loadingDialog.destroy();
                            }
                        });
    }

    /**
     * 加载框……
     */
    public void loading() {
        loadingDialog = new LoadingDialog(this);
        loadingDialog.setLoadText("加载中…");
        loadingDialog.setSwipeToDismiss(true);
        loadingDialog.setClickClose(true);
        loadingDialog.show();
    }

    @Override
    public void initEvent() {}

    private void toastMag(String msg) {
        new ToastDialog(getAbility()).setContentText(msg).show();
    }
}
