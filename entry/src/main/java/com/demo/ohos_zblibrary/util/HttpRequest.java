/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
limitations under the License.*/

package com.demo.ohos_zblibrary.util;

import com.demo.ohos_zblibrary.application.DemoApplication;

import zuo.biao.library.interfaces.OnHttpResponseListener;
import zuo.biao.library.manager.HttpManager;
import zuo.biao.library.model.Entry;
import zuo.biao.library.util.MD5Util;
import zuo.biao.library.util.SettingUtil;
import zuo.biao.library.util.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**HTTP请求工具类
 * @author Lemon
 * @use 添加请求方法xxxMethod >> HttpRequest.xxxMethod(...)
 * @must 所有请求的url、请求方法(GET, POST等)、请求参数(key-value方式，必要key一定要加，没提供的key不要加，value要符合指定范围)
 *       都要符合后端给的接口文档
 */
public class HttpRequest {

    /**
    基础URL，这里服务器设置可切换
     */
    public static final String URL_BASE = SettingUtil.getCurrentServerAddress();

    /**
     * page 数
     */
    public static final String PAGE_NUM = "pageNum";

    // 示例代码<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    // user<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    /**
     * 行列
     */
    public static final String RANGE = "range";
    /**
     * id
     */
    public static final String ID = "id";
    /**
     * 用户id
     */
    public static final String USER_ID = "userId";
    /**
     * 连接用户id
     */
    public static final String CURRENT_USER_ID = "currentUserId";
    /**
     * 手机
     */
    public static final String PHONE = "phone";
    /**
     * 密 码
     */
    public static final String PD = "pd";

    /**
     * 翻译，根据有道翻译API文档请求
     *
     * <br > 本Demo中只有这个是真正可用，其它需要自己根据接口文档新增或修改
     * @param word key
     * @param requestCode 请求码
     * @param listener 回调
     */
    public static void translate(String word, final int requestCode, final OnHttpResponseListener listener) {
        Map<String, Object> request = new HashMap<>();
        request.put("q", word);
        request.put("keyfrom", "ZBLibrary");
        request.put("key", 1430082675);
        request.put("type", "data");
        request.put("doctype", "json");
        request.put("version", "1.1");

        HttpManager.getInstance().get(request, "http", requestCode, listener);
    }

    // account<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    /**
     * 注册
     * @param phone 手机
     * @param password pwd
     * @param listener 监听
     * @param requestCode re
     */
    public static void register(
            final String phone, final String password, final int requestCode, final OnHttpResponseListener listener) {
        Map<String, Object> request = new HashMap<>();
        request.put(PHONE, phone);
        request.put(PD, MD5Util.MD5(password));

        HttpManager.getInstance().post(request, URL_BASE + "/register", requestCode, listener);
    }

    /**
     * 登陆
     * @param phone 手机
     * @param password pwd
     * @param listener 监听
     * @param requestCode r
     */
    public static void login(
            final String phone, final String password, final int requestCode, final OnHttpResponseListener listener) {
        Map<String, Object> request = new HashMap<>();
        request.put(PHONE, phone);
        request.put(PD, MD5Util.MD5(password));

        HttpManager.getInstance().post(request, URL_BASE + "/login", requestCode, listener);
    }

    // account>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    /**
     * 获取用户
     * @param userId id
     * @param requestCode requestCode
     * @param listener 监听
     */
    public static void getUser(long userId, final int requestCode, final OnHttpResponseListener listener) {
        Map<String, Object> request = new HashMap<>();
        request.put(CURRENT_USER_ID, DemoApplication.getInstance().getCurrentUserId());
        request.put(USER_ID, userId);

        HttpManager.getInstance().get(request, URL_BASE + "/user", requestCode, listener);
    }


    /**
     * 获取用户列表
     * @param range 数值
     * @param pageNum 页面
     * @param requestCode q
     * @param listener l
     */
    public static void getUserList(
            int range, int pageNum, final int requestCode, final OnHttpResponseListener listener) {
        Map<String, Object> request = new HashMap<>();
        request.put(CURRENT_USER_ID, DemoApplication.getInstance().getCurrentUserId());
        request.put(RANGE, range);
        request.put(PAGE_NUM, pageNum);

        HttpManager.getInstance().get(request, URL_BASE + "/user/list", requestCode, listener);
    }

    // user>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // 示例代码>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    /**
     * 添加请求参数，value为空时不添加，最快在 19.0 删除
     * @param list list
     * @param key key
     * @param value value
     */
    @Deprecated
    public static void addExistParameter(List<Entry> list, String key, Object value) {
        if (list == null) {
            list = new ArrayList<Entry>();
        }
        if (StringUtil.isNotEmpty(key, true) && StringUtil.isNotEmpty(value, true)) {
            list.add(new Entry(key, value));
        }
    }
}
