/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
limitations under the License.*/

package com.demo.ohos_zblibrary.util;

import com.demo.ohos_zblibrary.ResourceTable;

import zuo.biao.library.model.Menu;

import java.util.ArrayList;
import java.util.List;

/**底部菜单工具类
 * @author Lemon
 */
public class MenuUtil {
    /**
     * 发信息
     */
    public static final String NAME_SEND_MESSAGE = "发信息"; // 信息记录显示在通话记录左边
    /**
     * 呼叫
     */
    public static final String NAME_CALL = "呼叫"; // 信息记录显示在通话记录左边
    /**
     * 发送
     */
    public static final String NAME_SEND = "发送";
    /**
     * 二维码
     */
    public static final String NAME_QRCODE = "二维码";
    /**
     * 添加至
     */
    public static final String NAME_ADD_TO = "添加至";
    /**
     * 编辑
     */
    public static final String NAME_EDIT = "编辑";
    /**
     * 编辑所有
     */
    public static final String NAME_EDIT_ALL = "编辑所有";
    /**
     * 删除
     */
    public static final String NAME_DELETE = "删除";
    /**
     * 发邮件
     */
    public static final String NAME_SEND_EMAIL = "发邮件";
    /**
     * 信息记录显示在通话记录左边
     */
    public static final int INTENT_CODE_SEND_MESSAGE = 1; // 信息记录显示在通话记录左边
    /**
     * 信息记录显示在通话记录左边
     */
    public static final int INTENT_CODE_CALL = 2; // 信息记录显示在通话记录左边
    /**
     * 菜单 INTENT_CODE_SEND
     */
    public static final int INTENT_CODE_SEND = 3;
    /**
     * 菜单QRCODE
     */
    public static final int INTENT_CODE_QRCODE = 4;
    /**
     * 菜单ADD
     */
    public static final int INTENT_CODE_ADD_TO = 6;
    /**
     * 菜单 EDIT
     */
    public static final int INTENT_CODE_EDIT = 7;
    /**
     * 菜单 EDIT_ALL
     */
    public static final int INTENT_CODE_EDIT_ALL = 8;
    /**
     * 菜单 DELETE
     */
    public static final int INTENT_CODE_DELETE = 9;
    /**
     * 菜单 SEND_EMAIL
     */
    public static final int INTENT_CODE_SEND_EMAIL = 10;
    /**
     * 菜单 MESSAGE
     */
    public static Menu FSB_SEND_MESSAGE = new Menu(NAME_SEND_MESSAGE,
            ResourceTable.Media_mail_light, INTENT_CODE_SEND_MESSAGE);
    /**
     * 菜单 CALL
     */
    public static Menu FSB_CALL = new Menu(NAME_CALL, ResourceTable.Media_call_light, INTENT_CODE_CALL);
    /**
     * 菜单 SEND
     */
    public static Menu FSB_SEND = new Menu(NAME_SEND, ResourceTable.Media_send_light, INTENT_CODE_SEND);
    /**
     * 菜单 QRCODE
     */
    public static Menu FSB_QRCODE = new Menu(NAME_QRCODE, ResourceTable.Media_qrcode, INTENT_CODE_QRCODE);
    /**
     * 菜单 FSB_ADD_TO
     */
    public static Menu FSB_ADD_TO = new Menu(NAME_ADD_TO, ResourceTable.Media_add_light, INTENT_CODE_ADD_TO);
    /**
     * 菜单 FSB_EDIT
     */
    public static Menu FSB_EDIT = new Menu(NAME_EDIT, ResourceTable.Media_edit_light, INTENT_CODE_EDIT);
    /**
     * 菜单 FSB_EDIT_ALL
     */
    public static Menu FSB_EDIT_ALL = new Menu(NAME_EDIT_ALL, ResourceTable.Media_edit_light, INTENT_CODE_EDIT_ALL);
    /**
     * 菜单 FSB_DELETE
     */
    public static Menu FSB_DELETE = new Menu(NAME_DELETE, ResourceTable.Media_delete_light, INTENT_CODE_DELETE);
    /**
     * 菜单 FSB_SEND_EMAIL
     */
    public static Menu FSB_SEND_EMAIL =
            new Menu(NAME_SEND_EMAIL, ResourceTable.Media_mail_light, INTENT_CODE_SEND_EMAIL);
    /**
     * 菜单 CONTACT_LIST_FRAGMENT_MULTI
     */
    public static final int CONTACT_LIST_FRAGMENT_MULTI = 1;
    /**
     * 菜单 USER
     */
    public static final int USER = 1;

    /**
     * 菜单 getMenuList
     * @param which w
     * @return list
     */
    public static List<Menu> getMenuList(int which) {
        List<Menu> list = new ArrayList<Menu>();
        switch (which) {
            case USER:
                list.add(FSB_SEND_MESSAGE);
                list.add(FSB_CALL);
                list.add(FSB_SEND_EMAIL);
                list.add(FSB_SEND);
                list.add(FSB_QRCODE);
                break;
            default:
                break;
        }

        return list;
    }
}
