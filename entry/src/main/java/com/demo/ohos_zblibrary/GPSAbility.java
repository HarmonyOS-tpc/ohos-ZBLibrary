package com.demo.ohos_zblibrary;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Text;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.location.*;
import zuo.biao.library.ui.LoadingDialog;
import zuo.biao.library.util.CommonUtil;
import zuo.biao.library.util.Log;

import java.io.IOException;
import java.util.List;

/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * GPsdemo类
 */
public class GPSAbility extends Ability {

    Text txtGpsInfo;
    Text txtGeo;
    Text txtGeoList;

    Location loc;

    LoadingDialog loadingDialog;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_gps);
        txtGpsInfo = (Text) findComponentById(ResourceTable.Id_txt_gps_info);
        txtGeo = (Text) findComponentById(ResourceTable.Id_txt_geo);
        txtGeoList = (Text) findComponentById(ResourceTable.Id_txt_geo_list);

        Button button = (Button) findComponentById(ResourceTable.Id_btn_start);

        button.setClickedListener(component -> geo());

        location();
    }

    private void location() {
        Locator locator = new Locator(this);
        RequestParam requestParam = new RequestParam(RequestParam.PRIORITY_ACCURACY, 0, 0);

        locator.requestOnce(requestParam, locatorCallback);
        loading();
    }

    MyLocatorCallback locatorCallback = new MyLocatorCallback();

    private class MyLocatorCallback implements LocatorCallback {
        @Override
        public void onLocationReport(Location location) {
            loc = location;
            locationInfoShow();
        }

        @Override
        public void onStatusChanged(int type) {}

        @Override
        public void onErrorReport(int type) {
            CommonUtil.showShortToast(GPSAbility.this,"地理信息仅仅在中国境内使用");
            loadingDialog.hide();
        }
    }

    private void locationInfoShow() {
        if (loc == null) {
            CommonUtil.showShortToast(this,"请使用手机或重新定位");
            loadingDialog.hide();
            return;
        }
        txtGpsInfo.setText("经纬度：lat=" + loc.getLatitude() + "----lon=" + loc.getLongitude());
        new EventHandler(EventRunner.getMainEventRunner())
                .postTask(
                        new Runnable() {
                            @Override
                            public void run() {
                                loadingDialog.hide();
                                loadingDialog.destroy();
                            }
                        });
    }

    List<GeoAddress> addressList;

    private void geo() {
        if (loc == null) {
            return;
        }
        GeoConvert geoConvert = new GeoConvert();

        try {
            addressList = geoConvert.getAddressFromLocation(loc.getLatitude(), loc.getLongitude(), 5);
            if (addressList != null) {
                txtGeo.setText(addressList.size() + "地理编码信息：" + addressList.toString());

                List<GeoAddress> placeList =
                        geoConvert.getAddressFromLocationName(addressList.get(0).getPlaceName(), 5);
                txtGeoList.setText("附近位置列表" + placeList.size() + "个：" + placeList.toString());
            }
        } catch (IOException e) {
            Log.error("IOException",e.getMessage());
        }
    }

    /**
     * 加载框……
     */
    public void loading() {
        loadingDialog = new LoadingDialog(this);
        loadingDialog.setLoadText("加载中…");
        loadingDialog.setSwipeToDismiss(true);
        loadingDialog.setClickClose(true);
        loadingDialog.show();
        new EventHandler(EventRunner.getMainEventRunner()).postTask(new Runnable() {
            @Override
            public void run() {
                CommonUtil.showShortToast(GPSAbility.this,"定位失败，使用手机且在国内");
                loadingDialog.hide();
            }
        },10000);
    }
}
