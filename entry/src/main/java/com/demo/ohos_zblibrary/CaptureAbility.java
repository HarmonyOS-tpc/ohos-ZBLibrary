package com.demo.ohos_zblibrary;

import com.demo.ohos_zblibrary.slice.QrTestAbilitySlice;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.window.dialog.ToastDialog;

/**
 * 扫描二维码
 */
public class CaptureAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);

        setMainRoute(QrTestAbilitySlice.class.getName());
    }
}
