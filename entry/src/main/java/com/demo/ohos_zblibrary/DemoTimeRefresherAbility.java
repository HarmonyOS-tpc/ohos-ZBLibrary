package com.demo.ohos_zblibrary;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.window.dialog.ToastDialog;
import zuo.biao.library.manager.TimeRefresher;
import zuo.biao.library.util.StringUtil;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 倒计时
 */
public class DemoTimeRefresherAbility extends Ability implements TimeRefresher.OnTimeRefreshListener {

    private static final String TAG = "CountDownTimeAbility";

    Text txtCountDownTime;
    Text txtTimerTask;
    TextField field;
    int downTimeCount = 0;
    int midTime = 30;
    Button button;
    Button btn_stop;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_count_down_time);

        txtCountDownTime = (Text) findComponentById(ResourceTable.Id_txt_time);
        txtTimerTask = (Text) findComponentById(ResourceTable.Id_txt_timer_task);
        field = (TextField) findComponentById(ResourceTable.Id_txtField);
        button = (Button) findComponentById(ResourceTable.Id_btn_click);
        btn_stop = (Button) findComponentById(ResourceTable.Id_btn_stop);

        button.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        clear();

                        String number = StringUtil.getNumber(field);
                        if (StringUtil.isNotEmpty(number, true)) {
                            TimeRefresher.getInstance()
                                    .addTimeRefreshListener(
                                            TAG, Integer.valueOf(number), DemoTimeRefresherAbility.this);
                        }
                    }
                });
        btn_stop.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        clear();
                    }
                });

        setTimeTxt();
    }

    Timer timer;

    private void setTimeTxt() {
        timer = new Timer();
        timer.schedule(
                new TimerTask() {
                    /**
                     * 运行
                     */
                    public void run() {
                        midTime--;
                        long hh = midTime / 60 / 60 % 60;
                        long mm = midTime / 60 % 60;
                        long ss = midTime % 60;
                        if (midTime < 0 && timer != null) {
                            timer.cancel();
                            timer = null;
                            return;
                        }
                        setTimeScroll(hh, mm, ss);
                    }
                },
                0,
                1000);
    }

    private void setTimeScroll(long h, long m, long s) {
        getUITaskDispatcher()
                .asyncDispatch(
                        new Runnable() {
                            @Override
                            public void run() {
                                txtTimerTask.setText("还剩" + h + "小时" + m + "分钟" + s + "秒");
                            }
                        });
    }

    private void clear() {
        TimeRefresher.getInstance().removeTimeRefreshListener(TAG);
        downTimeCount = 0;
        txtCountDownTime.setText("0");
    }

    @Override
    public void onTimerStart() {
        toastMag("start");
    }

    @Override
    public void onTimerRefresh() {
        String number = StringUtil.getNumber(field);
        downTimeCount++;
        txtCountDownTime.setText(number + "秒后再刷新一次数字：" + downTimeCount);
    }

    @Override
    public void onTimerStop() {
        toastMag("stop");
    }

    private void toastMag(String msg) {
        new ToastDialog(this).setContentText(msg).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        TimeRefresher.getInstance().removeTimeRefreshListener(TAG);
        timer.cancel();
        timer = null;
    }
}
