package com.demo.ohos_zblibrary;

import com.demo.ohos_zblibrary.tool.IntentUtil;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import zuo.biao.library.base.BaseAbility;

public class ZxingAbility extends BaseAbility {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_zxing);
        findComponentById(ResourceTable.Id_custom_layout).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                IntentUtil.toActivity(getContext(), "ScanAbility");
            }
        });

        //如果你想要快速开发，并一个页面搞定，请参考DefaultLayoutAbility页面
        findComponentById(ResourceTable.Id_default_layout).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                IntentUtil.toActivity(getContext(), "CaptureAbility");
            }
        });
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }

    @Override
    public void initEvent() {

    }
}
