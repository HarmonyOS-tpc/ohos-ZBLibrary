package com.demo.ohos_zblibrary.slice;

import com.demo.ohos_zblibrary.ResourceTable;
import com.demo.ohos_zblibrary.tool.IntentUtil;

import com.zxing.slice.CaptureAbilitySlice;
import com.zxing.view.FinderSetting;

import ohos.aafwk.ability.AbilitySlice;

import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.media.image.PixelMap;
import zuo.biao.library.util.Log;

/**
 * 二维码扫描
 */
public class QrTestAbilitySlice extends AbilitySlice {
    private static final int CODE = 10;
    Text text;
    Button button;
    DirectionalLayout dlMain;
    DirectionalLayout dlMsg;
    DirectionalLayout dlFind;
    DirectionalLayout dlSetting;

    Text txt_title;
    Text txt_main;
    Text txt_msg;
    Text txt_find;
    Text txt_setting;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_slice_qr_code);
        text = (Text) findComponentById(ResourceTable.Id_txt_one);
        button = (Button) findComponentById(ResourceTable.Id_btn_one);
        initTopAndBottomView();
        button.setClickedListener(
                component -> {
                    toCaptureAbility(); // ability页面
                });
    }

    private void setInit() {
        CaptureAbilitySlice captureAbilitySlice = new CaptureAbilitySlice(new FinderSetting());
        presentForResult(captureAbilitySlice, new Intent(), CODE);
    }

    private void toCaptureAbility() {
        Intent intent = new Intent();
        Operation operationBuilder =
                new Intent.OperationBuilder()
                        .withAbilityName("com.zxing.ability.DefaultLayoutAbility")
                        .withDeviceId("")
                        .withBundleName(getBundleName())
                        .build();

        intent.setParam("isShowTopBar", true); // 设置顶部栏是否显示，不传默认显示，false不显示
        intent.setOperation(operationBuilder);
        startAbilityForResult(intent, CODE);
    }

    /**
     * 跳转ability时，使用的回调接口接收方法
     *
     * @param requestCode 请求码
     * @param resultCode  结果码
     * @param resultData  数据
     */
    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        try {
            if (resultData != null) {
                if (requestCode == CODE) {
                    String result = resultData.getStringParam("result");
                    text.setText("解析结果：" + result);

                    ToastDialog dialog = new ToastDialog(this);
                    dialog.setContentText("解析结果:" + result);
                    dialog.setDuration(5000);
                    dialog.show();
                }
            }
        } catch (Exception e) {
            Log.error("Exception", e.toString());
        }
    }

    /**
     * 该方式是使用跳转到slice获取到的回调结果
     *
     * @param requestCode  code
     * @param resultIntent intent
     */
    @Override
    protected void onResult(int requestCode, Intent resultIntent) {
        super.onResult(requestCode, resultIntent);
        try {
            if (resultIntent == null) {
                return;
            }
            if (requestCode == CODE) {
                String result = resultIntent.getStringParam("result");
                text.setText("解析结果：" + result);

                ToastDialog dialog = new ToastDialog(this);
                dialog.setContentText("解析结果:" + result);
                dialog.setDuration(5000);
                dialog.show();
            }
        } catch (Exception e) {
            Log.error("Exception", e.toString());
        }

    }

    private void initTopAndBottomView() {
        txt_title = (Text) findComponentById(ResourceTable.Id_txt_title);
        txt_main = (Text) findComponentById(ResourceTable.Id_txt_main);
        txt_msg = (Text) findComponentById(ResourceTable.Id_txt_msg);
        txt_find = (Text) findComponentById(ResourceTable.Id_txt_find);
        txt_setting = (Text) findComponentById(ResourceTable.Id_txt_setting);
        dlMain = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_main);
        dlMsg = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_msg);
        dlFind = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_find);
        dlSetting = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_setting);

        txt_title.setText("扫描");
        txt_find.setTextColor(Color.BLUE);
        dlMain.setClickedListener(component -> showMain());
        dlMsg.setClickedListener(component -> showMsg());
        dlFind.setClickedListener(component -> showFind());
        dlSetting.setClickedListener(component -> showSetting());
    }

    private void toast(String result) {
        new EventHandler(EventRunner.getMainEventRunner())
                .postTask(
                        new Runnable() {
                            @Override
                            public void run() {
                                ToastDialog dialog = new ToastDialog(getAbility());
                                dialog.setContentText("解析结果:" + result);
                                dialog.setDuration(5000);
                                dialog.show();
                            }
                        });
    }

    private void showMain() {
        IntentUtil.toSlice(this, IntentUtil.MAINSLICE);
    }

    private void showMsg() {
        IntentUtil.toSlice(this, IntentUtil.HOMESLICE);
    }

    private void showFind() {
    }

    private void showSetting() {
        IntentUtil.toSlice(this, IntentUtil.SETTINGSLICE);
    }
}
