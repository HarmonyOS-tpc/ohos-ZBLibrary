package com.demo.ohos_zblibrary.slice;

import com.demo.ohos_zblibrary.ResourceTable;
import com.demo.ohos_zblibrary.tool.IntentUtil;

import com.zxing.utils.Log;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.global.resource.RawFileEntry;
import ohos.media.player.Player;
import ohos.vibrator.agent.VibratorAgent;
import ohos.vibrator.bean.VibrationPattern;
import zuo.biao.library.util.ResTUtil;

/**
 * setting
 */
public class SettingAbilitySlice extends AbilitySlice {

    DirectionalLayout dlMain;

    DirectionalLayout dlMsg;

    DirectionalLayout  dlFind;

    DirectionalLayout  dlSetting;

    Text txt_title;

    Text txt_main;

    Text txt_msg;

    Text txt_find;

    Text txt_setting;
    Image oneImg;
    Image towImg;
    Image threeImg;
    Image fourImg;
    private boolean oneImgBool = true;
    private boolean twoImgBool = true;
    private boolean threeImgBool = true;
    private boolean fourImgBool = true;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_slice_setting);
        initTopAndBottomView();
    }

    private void initTopAndBottomView() {
        txt_title = (Text) findComponentById(ResourceTable.Id_txt_title);
        txt_main = (Text) findComponentById(ResourceTable.Id_txt_main);
        txt_msg = (Text) findComponentById(ResourceTable.Id_txt_msg);
        txt_find = (Text) findComponentById(ResourceTable.Id_txt_find);
        txt_setting = (Text) findComponentById(ResourceTable.Id_txt_setting);
        dlMain = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_main);
        dlMsg = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_msg);
        dlFind = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_find);
        dlSetting = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_setting);

        txt_title.setText("设置");
        txt_setting.setTextColor(Color.BLUE);
        dlMain.setClickedListener(component -> showMain());
        dlMsg.setClickedListener(component -> showMsg());
        dlFind.setClickedListener(component -> showFind());
        dlSetting.setClickedListener(component -> showSetting());

        oneImg = (Image) findComponentById(ResourceTable.Id_switch_one);
        towImg = (Image) findComponentById(ResourceTable.Id_switch_two);
        threeImg = (Image) findComponentById(ResourceTable.Id_switch_three);
        fourImg = (Image) findComponentById(ResourceTable.Id_switch_four);
        oneImg.setClickedListener(component -> initBeepSound());
        towImg.setClickedListener(component -> {
            if (twoImgBool) {
                twoImgBool = false;
                towImg.setPixelMap(ResTUtil.getPixelMap(this, ResourceTable.Media_on).get());
                VibrationPattern pattern = VibrationPattern.createSingle(2000, 200);
                VibratorAgent agent = new VibratorAgent();
                agent.start(pattern);
            }else {
                twoImgBool = true;
                towImg.setPixelMap(ResTUtil.getPixelMap(this, ResourceTable.Media_off).get());
            }
        });

        threeImg.setClickedListener(component -> {
            if (threeImgBool) {
                threeImgBool = false;
                threeImg.setPixelMap(ResTUtil.getPixelMap(this, ResourceTable.Media_on).get());
            }else {
                threeImgBool = true;
                threeImg.setPixelMap(ResTUtil.getPixelMap(this, ResourceTable.Media_off).get());
            }
        });
        fourImg.setClickedListener(component -> {
            if (fourImgBool) {
                fourImgBool = false;
                fourImg.setPixelMap(ResTUtil.getPixelMap(this, ResourceTable.Media_on).get());
            }else {
                fourImgBool = true;
                fourImg.setPixelMap(ResTUtil.getPixelMap(this, ResourceTable.Media_off).get());
            }
        });
    }

    private void initBeepSound() {
        if (oneImgBool) {
            oneImgBool = false;
            oneImg.setPixelMap(ResTUtil.getPixelMap(this, ResourceTable.Media_off).get());
        }else {
            try {
                Player player = new Player(this);

                String data = getResourceManager().getMediaPath(com.zxing.ResourceTable.Media_beep);
                RawFileEntry fileEntry = getResourceManager().getRawFileEntry(data);
                player.setSource(fileEntry.openRawFileDescriptor());

                player.setVolume(0.10f);
                player.prepare();
            } catch (Exception e) {
                Log.error(e.getMessage());
            }
            oneImgBool = true;
            oneImg.setPixelMap(ResTUtil.getPixelMap(this, ResourceTable.Media_on).get());
        }
    }

    private void showMain() {
        IntentUtil.toSlice(this, IntentUtil.MAINSLICE);
    }

    private void showMsg() {
        IntentUtil.toSlice(this, IntentUtil.HOMESLICE);
    }

    private void showFind() {
        present(new PageSliderAbilitySlice(), new Intent());
    }

    private void showSetting() {
    }
}
