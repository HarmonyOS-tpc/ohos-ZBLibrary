package com.demo.ohos_zblibrary.slice;

import com.demo.ohos_zblibrary.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import zuo.biao.library.util.ResTUtil;

import java.util.ArrayList;

/**
 * pageslider
 */
public class PageSliderAbilitySlice extends AbilitySlice {
    TabList tabList;

    private ArrayList<Component> pageview;

    RadioContainer radioContainer;

    Button btn_one;
    Button btn_tow;
    Button btn_three;

    private PageSlider view_pager;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        Component rootView =
                LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_tab_use_slice, null, false);
        super.setUIContent((ComponentContainer) rootView);
        radioContainer = (RadioContainer) findComponentById(ResourceTable.Id_radio_container);
        btn_one = (Button) findComponentById(ResourceTable.Id_btn_one);
        btn_tow = (Button) findComponentById(ResourceTable.Id_btn_tow);
        btn_three = (Button) findComponentById(ResourceTable.Id_btn_three);

        view_pager = (PageSlider) findComponentById(ResourceTable.Id_pager_slider);
        LayoutScatter dc = LayoutScatter.getInstance(getContext());
        DependentLayout view0 = (DependentLayout) dc.parse(ResourceTable.Layout_layout_page_0, null, false);
        DependentLayout view1 = (DependentLayout) dc.parse(ResourceTable.Layout_layout_page_1, null, false);
        DependentLayout view2 = (DependentLayout) dc.parse(ResourceTable.Layout_layout_page_2, null, false);
        // 将view装入数组
        pageview = new ArrayList<Component>();
        pageview.add(view0);
        pageview.add(view1);
        pageview.add(view2);
        initPageSlider();
    }

    private void initPageSlider() {
        // 数据适配器
        PageSliderProvider mPagerAdapter =
                new PageSliderProvider() {
                    @Override
                    // 获取当前窗体界面数
                    public int getCount() {
                        return pageview.size();
                    }

                    // 返回一个对象，这个对象表明了PagerAdapter适配器选择哪个对象放在当前的ViewPager中
                    @Override
                    public Object createPageInContainer(ComponentContainer componentContainer, int pos) {
                        componentContainer.addComponent(pageview.get(pos));
                        return pageview.get(pos);
                    }

                    // 是从ViewGroup中移出当前View
                    @Override
                    public void destroyPageFromContainer(ComponentContainer componentContainer, int pos, Object o) {
                        (componentContainer).removeComponent(pageview.get(pos));
                    }

                    // 断是否由对象生成界面
                    @Override
                    public boolean isPageMatchToObject(Component component, Object o) {
                        return component == o;
                    }
                };
        ((RadioButton) radioContainer.getComponentAt(0))
                .setTextColor(new Color(ResTUtil.getColor(getContext(), ResourceTable.Color_md_material_blue_800)));
        setPager(mPagerAdapter);
        radioContainer.setMarkChangedListener((radioContainer, pos) -> radioClick(pos, true));

        btn_one.setClickedListener(component -> view_pager.setCurrentPage(0));
        btn_tow.setClickedListener(component -> view_pager.setCurrentPage(1));
        btn_three.setClickedListener(component -> view_pager.setCurrentPage(2));
    }

    private void setPager(PageSliderProvider mPagerAdapter) {
        // 绑定适配器
        view_pager.setProvider(mPagerAdapter);
        view_pager.addPageChangedListener(
                new PageSlider.PageChangedListener() {
                    @Override
                    public void onPageSliding(int i, float v, int i1) {
                    }

                    @Override
                    public void onPageSlideStateChanged(int pos) {
                    }

                    @Override
                    public void onPageChosen(int pos) {
                        ((RadioButton) radioContainer.getComponentAt(pos)).setChecked(true);
                        radioClick(pos, false);
                        switch (pos) {
                            case 0:
                                btn_one.setTextColor(
                                        new Color(ResTUtil.getColor(getContext(), ResourceTable.Color_material_black)));
                                btn_tow.setTextColor(
                                        new Color(ResTUtil.getColor(getContext(), ResourceTable.Color_white_n)));
                                btn_three.setTextColor(
                                        new Color(ResTUtil.getColor(getContext(), ResourceTable.Color_white_n)));
                                break;
                            case 1:
                                btn_one.setTextColor(
                                        new Color(ResTUtil.getColor(getContext(), ResourceTable.Color_white_n)));
                                btn_tow.setTextColor(
                                        new Color(ResTUtil.getColor(getContext(), ResourceTable.Color_material_black)));
                                btn_three.setTextColor(
                                        new Color(ResTUtil.getColor(getContext(), ResourceTable.Color_white_n)));
                                break;
                            case 2:
                                btn_one.setTextColor(
                                        new Color(ResTUtil.getColor(getContext(), ResourceTable.Color_white_n)));
                                btn_tow.setTextColor(
                                        new Color(ResTUtil.getColor(getContext(), ResourceTable.Color_white_n)));
                                btn_three.setTextColor(
                                        new Color(ResTUtil.getColor(getContext(), ResourceTable.Color_material_black)));
                                break;
                        }
                    }
                });
    }

    private void radioClick(int pos, boolean isClick) {
        if (isClick) {
            view_pager.setCurrentPage(pos);
            colorSwitch(pos);
        } else {
            getAbility()
                    .getUITaskDispatcher()
                    .asyncDispatch(
                            new Runnable() {
                                @Override
                                public void run() {
                                    colorSwitch(pos);
                                }
                            });
        }
    }

    private void colorSwitch(int pos) {
        switch (pos) {
            case 0:
                ((RadioButton) radioContainer.getComponentAt(0))
                        .setTextColor(
                                new Color(ResTUtil.getColor(getContext(), ResourceTable.Color_md_material_blue_800)));
                ((RadioButton) radioContainer.getComponentAt(1))
                        .setTextColor(new Color(ResTUtil.getColor(getContext(), ResourceTable.Color_black)));
                ((RadioButton) radioContainer.getComponentAt(2))
                        .setTextColor(new Color(ResTUtil.getColor(getContext(), ResourceTable.Color_black)));
                break;
            case 1:
                ((RadioButton) radioContainer.getComponentAt(0))
                        .setTextColor(new Color(ResTUtil.getColor(getContext(), ResourceTable.Color_black)));
                ((RadioButton) radioContainer.getComponentAt(1))
                        .setTextColor(
                                new Color(ResTUtil.getColor(getContext(), ResourceTable.Color_md_material_blue_800)));
                ((RadioButton) radioContainer.getComponentAt(2))
                        .setTextColor(new Color(ResTUtil.getColor(getContext(), ResourceTable.Color_black)));
                break;
            case 2:
                ((RadioButton) radioContainer.getComponentAt(0))
                        .setTextColor(new Color(ResTUtil.getColor(getContext(), ResourceTable.Color_black)));
                ((RadioButton) radioContainer.getComponentAt(1))
                        .setTextColor(new Color(ResTUtil.getColor(getContext(), ResourceTable.Color_black)));
                ((RadioButton) radioContainer.getComponentAt(2))
                        .setTextColor(
                                new Color(ResTUtil.getColor(getContext(), ResourceTable.Color_md_material_blue_800)));
                break;
        }
    }
}
