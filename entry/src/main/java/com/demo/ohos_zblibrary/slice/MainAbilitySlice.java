package com.demo.ohos_zblibrary.slice;

import com.demo.ohos_zblibrary.ResourceTable;
import com.demo.ohos_zblibrary.adapter.MainItemProvider;
import com.demo.ohos_zblibrary.tool.IntentUtil;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;
import ohos.bundle.IBundleManager;
import ohos.security.SystemPermission;
import zuo.biao.library.model.PlaceBean;
import zuo.biao.library.ui.*;
import zuo.biao.library.util.CommonUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * 主页
 */
public class MainAbilitySlice extends AbilitySlice implements ListContainer.ItemClickedListener {

    private static final int QRSCANRESULT = 1102;
    private static final int PERMISSIONCAMERA = 1103;

    DirectionalLayout dlMain;
    DirectionalLayout dlMsg;
    DirectionalLayout dlFind;
    DirectionalLayout dlSetting;

    Text txt_title;
    Text txt_main;
    Text txt_msg;
    Text txt_find;
    Text txt_setting;

    MainItemProvider provider;
    ListContainer listContainer;

    private String bundleName = "";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initTopAndBottomView();
        bundleName = getBundleName();

        List<String> list = new ArrayList<>();
        list.add("二维码扫描功能");
        list.add("生成二维码");
        list.add("选择地区功能");
        list.add("日期选择器");
        list.add("退出弹框");
        list.add("列表选择弹框");
        list.add("输入弹框");
        list.add("http请求");
        list.add("加载框");
        list.add("选择图片或拍照");
        list.add("倒计时或刷新器");
        list.add("定位和地理编码");

        listContainer = (ListContainer) findComponentById(ResourceTable.Id_listContainer);
        provider = new MainItemProvider(this, list);
        listContainer.setItemProvider(provider);

        listContainer.setItemClickedListener(this);
    }

    @Override
    public void onItemClicked(ListContainer listContainer, Component component, int position, long l) {
        switch (position) {
            case 0:
                toCaptureAbility();
                break;
            case 1:
                toQRCodeAbility();
                break;
            case 2:
                selectPlacePick();
                break;
            case 3:
                timeSelect();
                break;
            case 4:
                quitDialog();
                break;
            case 5:
                dialogSelector();
                break;
            case 6:
                inputDialog();
                break;
            case 7:
                httpRequest();
                break;
            case 8:
                loading();
                break;
            case 9:
                bottomSelectDialog();
                break;
            case 10:
                toDemoTimeRefresherAbility();
                break;
            case 11:
                toGPSAbility();
                break;
        }
    }

    /**
     * 二维码扫描功能
     */
    public void toCaptureAbility() {
        if (IBundleManager.PERMISSION_GRANTED != verifySelfPermission(SystemPermission.CAMERA)) {
            requestPermissionsFromUser(new String[] {SystemPermission.CAMERA}, PERMISSIONCAMERA);
            return;
        }

        IntentUtil.toActivity(getAbility(), "ZxingAbility");
    }

    @Override
    protected void onResult(int requestCode, Intent resultIntent) {
        super.onResult(requestCode, resultIntent);
        if (requestCode == QRSCANRESULT) {
            String result = resultIntent.getStringParam("result");
            toastMag(result);
        }
    }

    /**
     * 二维码生成功能
     */
    public void toQRCodeAbility() {
        IntentUtil.toActivity(this, "QRCodeAbility");
    }

    /**
     * 选择地区功能
     */
    private void selectPlacePick() {
        String[] tss = new String[] {"北京", "上海", "天津", "重庆"};
        List<PlaceBean> placeBeans = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            PlaceBean bean = new PlaceBean();
            bean.code = "100" + i;
            bean.name = tss[i];
            placeBeans.add(bean);
        }
        List<PlaceBean> as = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            PlaceBean bean = new PlaceBean();
            bean.code = "1000" + i;
            bean.name = "北京" + i + "区";
            as.add(bean);
        }
        List<PlaceBean> bs = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            PlaceBean bean = new PlaceBean();
            bean.code = "1000" + i;
            bean.name = "上海" + i + "区";
            bs.add(bean);
        }
        List<PlaceBean> cs = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            PlaceBean bean = new PlaceBean();
            bean.code = "1000" + i;
            bean.name = "天津" + i + "区";
            cs.add(bean);
        }
        List<PlaceBean> ds = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            PlaceBean bean = new PlaceBean();
            bean.code = "1000" + i;
            bean.name = "重庆" + i + "区";
            ds.add(bean);
        }
        Map<String, List<PlaceBean>> towPlaceMap = new HashMap<>();
        towPlaceMap.put("1000", as);
        towPlaceMap.put("1001", bs);
        towPlaceMap.put("1002", cs);
        towPlaceMap.put("1003", ds);
        dialogUp(placeBeans, towPlaceMap);
    }

    /**
     * 弹框
     * @param placeBeans 市 地址集合
     * @param towPlaceMap 区 map
     */
    private void dialogUp(List<PlaceBean> placeBeans ,Map<String, List<PlaceBean>> towPlaceMap) {
        PlacePickWindow.createIntent(getAbility(), placeBeans, towPlaceMap, new PlacePickWindow.OnSureSelectListener() {
            @Override
            public void select(String oneSelect, String towSelect) {
                CommonUtil.showShortToast(getContext(), "地址：" + oneSelect + "-" + towSelect);
            }
        });
//        new PlacePickWindow.Builder(getAbility())
//                .setData(placeBeans, towPlaceMap)
//                .setSelectListener(
//                        new PlacePickWindow.OnSureSelectListener() {
//                            @Override
//                            public void select(String oneSelect, String towSelect) {
//                                CommonUtil.showShortToast(getContext(), "地址：" + oneSelect + "-" + towSelect);
//                            }
//                        })
//                .show();
    }

    /**
     * 时间选择器
     */
    private void timeSelect() {
        new DatePickWindow.Builder(this)
                .setSelectListener(
                        new DatePickWindow.OnSureSelectListener() {
                            @Override
                            public void select(String yearSelect, String monthSelect, String daySelect) {
                                toastMag(yearSelect + "年" + monthSelect + "月" + daySelect + "日");
                            }
                        })
                .show();
    }

    /**
     * 退出弹框
     */
    public void quitDialog() {
        new AlertDialog(getContext(),"退出登录","确定要退出登录？",
                "sure","cancel",1,new AlertDialog.OnDialogButtonClickListener(){

            @Override
            public void onDialogButtonClick(int requestCode, boolean isPositive) {
                toastMag(isPositive?"sure":"cancel");
            }
        })
                .show();
    }

    /**
     * 弹框选择器
     */
    private void dialogSelector() {
        String[] strings = new String[] {"黄色", "蓝色", "白色", "绿色"};

        new ItemDialog.Builder(getAbility())
                .title("选择颜色")
                .items(strings)
                .itemsCallback(
                        new ItemDialog.ListCallback() {
                            @Override
                            public void onSelection(
                                    ItemDialog dialog, Component itemView, int position, String text) {
                                toastMag(text);
                            }
                        })
                .show();
    }

    /**
     * 输入弹框
     */
    private void inputDialog() {
        new EditTextInfoWindow.Builder(getAbility())
                .title("输入框")
                .inputType()
                .inputRange()
                .input(
                        "请输入15个以内的字",
                        "",
                        false,
                        new EditTextInfoWindow.InputCallback() {
                            @Override
                            public void onInput(EditTextInfoWindow dialog, String input) {
                                toastMag(input);
                            }
                        })
                .positiveText(
                        "确定",
                        new EditTextInfoWindow.OnPositiveListener() {
                            @Override
                            public void onClick(Component component) {}
                        })
                .negativeText("取消")
                .show();
    }

    /**
     * http 请求
     */
    private void httpRequest() {
        IntentUtil.toActivity(getAbility(), "HttpRequestAbility");
    }

    /**
     * 加载框……
     */
    public void loading() {
        LoadingDialog loadingDialog = new LoadingDialog(this);
        loadingDialog.setLoadText("加载中…");
        loadingDialog.setSwipeToDismiss(true);
        loadingDialog.setClickClose(true);
        loadingDialog.show();
    }

    /**
     * 选择图片弹框
     */
    private void bottomSelectDialog() {
        new BottomMenuWindow(this)
                .setTitle("选择图片")
                .setClickListener(
                        new BottomMenuWindow.ItemClickListener() {
                            @Override
                            public void click(int position) {
                                toastMag(position + "");
                                switch (position) {
                                    case 0:
                                        selectCamera();
                                        break;
                                    case 1:
                                        selectPhoto();
                                        break;
                                }
                            }
                        })
                .show();
    }

    /**
     * \
     * 选择相册
     */
    private void selectPhoto() {
        IntentUtil.toActivity(getAbility(), "ImagePickerAbility");
    }

    /**
     * \
     * 拍照
     */
    private void selectCamera() {
        IntentUtil.toActivity(getAbility(), "ShowCameraPhotoAbility");
    }

    /**
     * 时间刷新器
     */
    private void toDemoTimeRefresherAbility() {
        IntentUtil.toActivity(getAbility(), "DemoTimeRefresherAbility");
    }

    private void toGPSAbility() {
        IntentUtil.toActivity(getAbility(), "GPSAbility");
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void toastMag(String msg) {
        new ToastDialog(this).setContentText(msg).show();
    }

    private void initTopAndBottomView() {
        txt_title = (Text) findComponentById(ResourceTable.Id_txt_title);
        txt_main = (Text) findComponentById(ResourceTable.Id_txt_main);
        txt_msg = (Text) findComponentById(ResourceTable.Id_txt_msg);
        txt_find = (Text) findComponentById(ResourceTable.Id_txt_find);
        txt_setting = (Text) findComponentById(ResourceTable.Id_txt_setting);
        dlMain = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_main);
        dlMsg = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_msg);
        dlFind = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_find);
        dlSetting = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_setting);

        txt_title.setText("首页");
        txt_main.setTextColor(Color.BLUE);
        dlMain.setClickedListener(component -> showMain());
        dlMsg.setClickedListener(component -> showMsg());
        dlFind.setClickedListener(component -> showFind());
        dlSetting.setClickedListener(component -> showSetting());
    }

    private void showMain() {
    }

    private void showMsg() {
        IntentUtil.toSlice(this, IntentUtil.HOMESLICE);
    }

    private void showFind() {
        present(new PageSliderAbilitySlice(), new Intent());
    }

    private void showSetting() {
        IntentUtil.toSlice(this, IntentUtil.SETTINGSLICE);
    }
}
