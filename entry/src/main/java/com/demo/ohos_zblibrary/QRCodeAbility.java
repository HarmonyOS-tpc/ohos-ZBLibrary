package com.demo.ohos_zblibrary;

import com.google.zxing.WriterException;
import com.zxing.encoding.EncodingHandler;
import com.zxing.slice.CodeUtils;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Image;
import ohos.media.image.PixelMap;

/**
 * 生成二维码
 */
public class QRCodeAbility extends Ability {
    Image image;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_code);
        image = (Image) findComponentById(ResourceTable.Id_img_one);
        setQrIcon();
    }

    private void setQrIcon() {
        String textContent = "这句话我要生成二维码";
        if (textContent == null || textContent.length() == 0) {
            return;
        }
        PixelMap pixelMap = null;
        try {
            pixelMap = EncodingHandler.createQRCode(textContent, 400);
        } catch (WriterException e) {
            e.printStackTrace();
        }
        image.setPixelMap(pixelMap);
    }
}
