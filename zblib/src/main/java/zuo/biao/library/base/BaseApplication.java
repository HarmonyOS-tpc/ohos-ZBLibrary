package zuo.biao.library.base;

import ohos.aafwk.ability.AbilityPackage;
import ohos.app.Context;
import zuo.biao.library.ResourceTable;
import zuo.biao.library.util.Log;
import zuo.biao.library.util.ResTUtil;

/**
 * BaseApplication类
 */
public class BaseApplication extends AbilityPackage {
    private static Context context;

    @Override
    public void onInitialize() {
        super.onInitialize();
        context = getAbilityPackageContext();
    }

    /**
     * 获取上下文
     * @return 上下文
     */
    public static Context getInstance() {
        return context;
    }

    /**初始化方法
     * @param application application
     * @must 调用init方法且只能调用一次，如果extends BaseApplication会自动调用
     */
    public static void init(Context application) {
        context = application;
        if (context == null) {
            Log.error("BaseApplication", "\n\n\n\n\n !!!!!! 调用BaseApplication中的init方法，instance不能为null !!!" +
                    "\n <<<<<< init  instance == null ！！！ >>>>>>>> \n\n\n\n");
        }
    }

    /**获取应用名
     * @return getAppName
     */
    public String getAppName() {
        return ResTUtil.getString(context, ResourceTable.String_app_name);
    }
    /**获取应用版本名(显示给用户看的)
     * @return getAppVersion
     */
    public String getAppVersion() {
        return ResTUtil.getString(context, ResourceTable.String_app_version);
    }

}
