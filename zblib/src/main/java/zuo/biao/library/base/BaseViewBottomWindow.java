/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
limitations under the License.*/

package zuo.biao.library.base;

import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import org.jetbrains.annotations.Nullable;
import zuo.biao.library.ResourceTable;
import zuo.biao.library.interfaces.ViewPresenter;
import zuo.biao.library.util.StringUtil;

/**基础带标签的FragmentActivity
 * @author Lemon
 * @see #setContentView
 * @use extends BaseViewBottomWindow, 具体参考.DemoTabActivity
 * @must 在子类onCreate中调用initView();initData();initEvent();
 */
public abstract class BaseViewBottomWindow<T, BV extends BaseView<T>> extends BaseBottomWindow
        implements ViewPresenter {


    /**
     * @return
     * @must 1.不要在子类重复这个类中onCreate中的代码;
     *       2.在子类onCreate中super.onCreate(savedInstanceState);
     *       initView();initData();initEvent();
     */

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent( ResourceTable.Layout_base_view_bottom_window);
    }



    //UI显示区(操作UI，但不存在数据获取或处理代码，也不存在事件监听代码)<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



    protected ComponentContainer llBaseViewBottomWindowContainer;

    protected Button tvBaseViewBottomWindowReturn;
    protected Button tvBaseViewBottomWindowForward;
    /**
     * 如果在子类中调用(即super.initView());则view必须含有initView中初始化用到的id(非@Nullable标记)且id对应的View的类型全部相同；
     * 否则必须在子类initView中重写这个类中initView内的代码(所有id替换成可用id)
     */
    @Override
    public void initView() {//必须调用
        super.initView();
        autoSetTitle();

        llBaseViewBottomWindowContainer = findView(ResourceTable.Id_llBaseViewBottomWindowContainer);

        tvBaseViewBottomWindowReturn = findView(ResourceTable.Id_tvBaseViewBottomWindowReturn);
        tvBaseViewBottomWindowForward = findView(ResourceTable.Id_tvBaseViewBottomWindowForward);
    }


    //UI显示区(操作UI，但不存在数据获取或处理代码，也不存在事件监听代码)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>










    //Data数据区(存在数据获取或处理代码，但不存在事件监听代码)<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    protected T data;
    protected BV containerView;
    @Override
    public void initData() {//必须调用
        super.initData();

        if (tvBaseTitle != null) {
            String title = getIntent().getStringParam(INTENT_TITLE);
            if (StringUtil.isNotEmpty(title, true) == false) {
                title = getTitleName();
            }
            tvBaseTitle.setVisibility(StringUtil.isNotEmpty(title, true) ? Component.VISIBLE : Component.HIDE);
            tvBaseTitle.setText(StringUtil.getTrimedString(title));
        }

        if (tvBaseViewBottomWindowReturn != null && StringUtil.isNotEmpty(getReturnName(), true)) {
            tvBaseViewBottomWindowReturn.setText(StringUtil.getCurrentString());
        }
        if (tvBaseViewBottomWindowForward != null && StringUtil.isNotEmpty(getForwardName(), true)) {
            tvBaseViewBottomWindowForward.setText(StringUtil.getCurrentString());
        }


        llBaseViewBottomWindowContainer.removeAllComponents();
        if (containerView == null) {
            containerView = createView();
            llBaseViewBottomWindowContainer.addComponent(containerView.createView());
        }
        containerView.bindView(null);
    }

    /**
     * 创建新的内容View
     * @return createView
     */
    protected abstract BV createView();

    //Data数据区(存在数据获取或处理代码，但不存在事件监听代码)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>










    //Event事件区(只要存在事件监听代码就是)<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    @Override
    public void initEvent() {//必须调用
        super.initEvent();

    }


    //生命周期、onActivityResult<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


    @Override
    protected void onStop() {

        data = null;
        llBaseViewBottomWindowContainer.removeAllComponents();
        if (containerView != null) {
            containerView.onDestroy();
        }
        super.onStop();

        llBaseViewBottomWindowContainer = null;
        containerView = null;
    }


    //生命周期、onActivityResult>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


    //Event事件区(只要存在事件监听代码就是)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>








    //内部类,尽量少用<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



    //内部类,尽量少用>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

}
