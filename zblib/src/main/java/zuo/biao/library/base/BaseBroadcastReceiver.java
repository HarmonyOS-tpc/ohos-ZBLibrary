/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
limitations under the License.*/

package zuo.biao.library.base;

import ohos.app.Context;
import ohos.event.commonevent.*;
import ohos.event.intentagent.IntentAgent;
import ohos.rpc.RemoteException;
import org.jetbrains.annotations.Nullable;
import zuo.biao.library.util.Log;
import zuo.biao.library.util.StringUtil;

import java.util.Arrays;
import java.util.List;

/**
 * 基础广播接收器
 *
 * @author Lemon
 * @use 自定义BroadcastReceiver - extends BaseBroadcastReceiver；其它 - 直接使用里面的静态方法
 * @must 调用register和unregister方法
 */
public abstract class BaseBroadcastReceiver extends CommonEventSubscriber {
    private static final String TAG = "BaseBroadcastReceiver";
    private static OnReceiveListener onReceiveListener = null;
    private Context context = null;

    /**
     * 订阅者
     */
    public static class MyCommonEventSubscriber extends CommonEventSubscriber {
        MyCommonEventSubscriber(CommonEventSubscribeInfo info) {
            super(info);
        }

        @Override
        public void onReceiveEvent(CommonEventData commonEventData) {
            onReceiveListener.onReceive(commonEventData);
        }
    }

    public BaseBroadcastReceiver(Context context, CommonEventSubscribeInfo subscribeInfo) {
        super(subscribeInfo);
        this.context = context;
    }
    /**
     * 接收信息监听回调
     */
    public interface OnReceiveListener {
        void onReceive(CommonEventData intent);
    }

    /**
     * 注册接收信息监听
     *
     * @param onReceiveListener onReceiveListener
     * @must 在register后，unregister前调用
     */
    public void setOnReceiveListener(OnReceiveListener onReceiveListener) {
        this.onReceiveListener = onReceiveListener;
    }

    /**
     * 注册广播接收器
     *
     * @return BaseBroadcastReceiver
     * @use 一般在Activity或Fragment的onCreate中调用
     */
    public abstract BaseBroadcastReceiver register();

    /**
     * 取消注册广播接收器
     *
     * @use 一般在Activity或Fragment的onDestroy中调用
     */
    public abstract void unregister();

    /**
     * 注册广播接收器
     *
     * @param context context
     * @return CommonEventSubscriber
     */
    public CommonEventSubscriber register(Context context) {
        String event = "com.my.test";
        MatchingSkills matchingSkills = new MatchingSkills();
        matchingSkills.addEvent(event); // 自定义事件

        CommonEventSubscribeInfo subscribeInfo = new CommonEventSubscribeInfo(matchingSkills);
        subscribeInfo.setPriority(100); // 设置优先级，优先级取值范围[-1000，1000]，值默认为 0。
        MyCommonEventSubscriber subscriber =
                new MyCommonEventSubscriber(subscribeInfo) {
                    @Override
                    public void onReceiveEvent(CommonEventData commonEventData) {
                        super.onReceiveEvent(commonEventData);
                        onReceiveListener.onReceive(commonEventData);
                    }
                };

        try {
            CommonEventManager.subscribeCommonEvent(subscriber);
        } catch (RemoteException e) {
            Log.error("RemoteException", e.getMessage());
        }

        return subscriber;
    }

    /**注册广播接收器
     * 暂不支持该类型
     * @param context 上下文
     * @param receiver CommonEventSubscriber
     * @param action action
     * @return CommonEventSubscriber
     */
    @Deprecated
    public static CommonEventSubscriber register(Context context,  CommonEventSubscriber receiver, String action) {
        return register(context, receiver, new String[] {action});
    }
    /**注册广播接收器
     * 暂不支持该类型
     * @param context 上下文
     * @param receiver CommonEventSubscriber
     * @param actions actions
     * @return CommonEventSubscriber
     */
    @Deprecated
    public static CommonEventSubscriber register(Context context,  CommonEventSubscriber receiver, String[] actions) {
        return register(context, receiver, actions == null ? null : Arrays.asList(actions));
    }
    /**注册广播接收器
     * 暂不支持该类型
     * @param context 上下文
     * @param receiver CommonEventSubscriber
     * @param actionList actionList
     * @return CommonEventSubscriber
     */
    @Deprecated
    public static CommonEventSubscriber register(Context context, CommonEventSubscriber receiver, List<String> actionList) {
        return null;
    }
    /**注册广播接收器
     * 暂不支持该类型
     * @param context 上下文
     * @param receiver CommonEventSubscriber
     * @param filter filter
     * @return
     */
    @Deprecated
    public static CommonEventSubscriber register(Context context,  CommonEventSubscriber receiver, IntentAgent filter) {

        return receiver;
    }

    /**
     * 取消注册广播接收器
     *
     * @param subscriber 订阅者
     */
    public static void unregister(CommonEventSubscriber subscriber) {
        try {
            CommonEventManager.unsubscribeCommonEvent(subscriber);
        } catch (Exception e) {
            Log.error(TAG, "unregister  try { context.unregisterReceiver(receiver);" + " } catch (Exception e) { " + e.getMessage());
        }
    }
}
