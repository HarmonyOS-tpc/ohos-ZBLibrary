/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
limitations under the License.*/

package zuo.biao.library.base;

import ohos.agp.components.Component;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import zuo.biao.library.ResourceTable;
import zuo.biao.library.util.Log;

/**基础底部弹出界面Activity
 * @author Lemon
 * @warn 不要在子类重复这个类中onCreate中的代码
 * @use extends BaseBottomWindow, 具体参考.DemoBottomWindow
 */
public abstract class BaseBottomWindow extends BaseAbility {
    private static final String TAG = "BaseBottomWindow";

    /**
     * INTENT_ITEMS
     */
    public static final String INTENT_ITEMS = "INTENT_ITEMS";
    /**
     * INTENT_ITEM_IDS
     */
    public static final String INTENT_ITEM_IDS = "INTENT_ITEM_IDS";

    // UI显示区(操作UI，但不存在数据获取或处理代码，也不存在事件监听代码)<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    /**
     * 子Activity全局背景View
     */
    protected Component vBaseBottomWindowRoot; // 子Activity全局背景View

    /**
     * 否则必须在子类initView中重写这个类中initView内的代码(所有id替换成可用id)
     */
    @Override
    public void initView() { // 必须调用
        vBaseBottomWindowRoot = findView(ResourceTable.Id_vBaseBottomWindowRoot);
    }

    // UI显示区(操作UI，但不存在数据获取或处理代码，也不存在事件监听代码)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // Data数据区(存在数据获取或处理代码，但不存在事件监听代码)<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    @Override
    public void initData() { // 必须调用
    }

    /**
     * 设置需要返回的结果
     */
    protected abstract void setResult();

    // Data数据区(存在数据获取或处理代码，但不存在事件监听代码)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // Event事件区(只要存在事件监听代码就是)<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    @Override
    public void initEvent() { // 必须调用
    }

    @Override
    public void onForwardClick(Component v) {
        setResult();
        terminateAbility();
    }

    /**
     * 线程
     */
    public EventHandler exitHandler =
            new EventHandler(EventRunner.getMainEventRunner()) {
                @Override
                protected void processEvent(InnerEvent event) {
                    super.processEvent(event);
                    BaseBottomWindow.super.terminateAbility();
                }
            };

    // 生命周期、onActivityResult<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    private boolean isExit = false;

    @Override
    public void terminateAbility(int requestCode) {
        super.terminateAbility(requestCode);
        Log.debug(TAG, "finish >>> isExit = " + isExit);
        if (isExit) {
            return;
        }
        isExit = true;

        vBaseBottomWindowRoot.setVisibility(Component.HIDE);

        exitHandler.sendEvent(0, 200);
    }

    @Override
    protected void onStop() {
        super.onStop();
        vBaseBottomWindowRoot = null;
    }

    // 生命周期、onActivityResult>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // Event事件区(只要存在事件监听代码就是)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // 内部类,尽量少用<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    // 内部类,尽量少用>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

}
