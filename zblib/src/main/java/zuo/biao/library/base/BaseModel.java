package zuo.biao.library.base;

import java.io.Serializable;

/**
 * 基础model
 */
public abstract class BaseModel implements Serializable {
    /**
     * id属性
     */
    public long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    /**数据正确性校验
     * @param data data
     * @return 是否正确
     */
    public static boolean isCorrect(BaseModel data) {
        return data != null && data.isCorrect();
    }

    /**
     * 数据正确性校验
     * @return true
     */
    protected abstract boolean isCorrect(); // public导致JSON.toJSONString会添加correct字段
}
