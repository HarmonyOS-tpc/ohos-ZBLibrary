package zuo.biao.library.base;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.*;
import ohos.data.rdb.DataObservable;
import ohos.data.rdb.DataObserver;
import ohos.global.resource.ResourceManager;
import zuo.biao.library.interfaces.AdapterViewPresenter;
import zuo.biao.library.interfaces.OnLoadListener;

import java.util.ArrayList;
import java.util.List;

/**基础Adapter，基于SmartRefreshLayout的BaseRecyclerAdapter修改
 * <br> 适用于几乎所有列表、表格，包括：
 * <br> 1.RecyclerView及其子类
 * <br> 2.ListView,GridView等AbsListView的子类
 * <br> 3.刷新用 refresh 或 notifyListDataSetChanged，notifyDataSetChanged 可能无效
 * <br> 4.出于性能考虑，里面很多方法对变量(比如list)都没有判断，应在adapter外判断
 * @author SCWANG
 * @author Lemon
 * @param <T> 数据模型(model/JavaBean)类
 * @param <BV> BaseView的子类，相当于ViewHolder
 * @see #notifyListDataSetChanged
 * @use extends BaseAdapter<T, BV>, 具体参考.DemoAdapter
 */
public abstract class BaseAdapter<T, BV extends BaseView<T>> extends BaseItemProvider implements AdapterViewPresenter {
    private BaseView.OnViewClickListener onViewClickListener;

    /**
     * 设置view监听
     * @param onViewClickListener 监听
     * @return  BaseAdapter
     */
    public BaseAdapter<T, BV> setOnViewClickListener(BaseView.OnViewClickListener onViewClickListener) {
        this.onViewClickListener = onViewClickListener;
        return this;
    }

    private ListContainer.ItemClickedListener onItemClickListener;

    /**
     * 设置item点击监听
     * @param onItemClickListener 监听
     * @return  BaseAdapter
     */
    public BaseAdapter<T, BV> setOnItemClickListener(ListContainer.ItemClickedListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
        return this;
    }

    private ListContainer.ItemLongClickedListener onItemLongClickListener;

    /**
     * 设置item长按监听
     * @param onItemLongClickListener 监听
     * @return BaseAdapter
     */
    public BaseAdapter<T, BV> setOnItemLongClickListener(
            ListContainer.ItemLongClickedListener onItemLongClickListener) {
        this.onItemLongClickListener = onItemLongClickListener;
        return this;
    }

    /**
     * 管理整个界面的Activity实例
     */
    public Ability context;
    /**
     * 布局解释器,用来实例化列表的item的界面
     */
    public LayoutScatter inflater;
    /**
     * 资源获取器，用于获取res目录下的文件及文件中的内容等
     */
    public ResourceManager resources;

    public BaseAdapter(Ability context) {
        this.context = context;
        this.inflater = LayoutScatter.getInstance(context);
        this.resources = context.getResourceManager();
    }

    // 预加载，可不使用 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    /**
     * 加载监听
     */
    protected OnLoadListener onLoadListener;
    /**设置加载更多的监听
     * @param onLoadListener onLoadListener
     */
    public void setOnLoadListener(OnLoadListener onLoadListener) {
        this.onLoadListener = onLoadListener;
    }

    /**
     * 预加载提前数。
     * <br > = 0 - 列表滚到底部(最后一个Item View显示)时加载更多
     * <br > < 0 - 禁用加载更多
     * <br > > 0 - 列表滚到倒数第preloadCount个Item View显示时加载更多
     * @use 可在子类getView被调用前(可以是在构造器内)赋值
     */
    public int preloadCount = 0;

    // 预加载，可不使用 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    /**bv的显示方法
     * @param position position
     * @param bv bv
     */
    //    @Override
    public void bindView(int position, BV bv) {
        bv.selected = isSelected(position);
        bv.bindView(getItem(position), position, getItemViewType(position));
    }

    /**
     * 已选中项的位置，一般可在onItemClick回调中：
     * <br /> adapter.selectedPosition = position;
     * <br /> adapter.notifyListDataSetChanged();
     */
    public int selectedPosition = -1;
    /**是否已被选中，默认实现单选，重写可自定义实现多选
     * @param position position
     * @return boolean
     */
    public boolean isSelected(int position) {
        return selectedPosition == position;
    }

    /**
     * 传进来的数据列表
     */
    protected List<T> list;

    /**
     * 获取list
     * @return list
     */
    public List<T> getList() {
        return list;
    }

    /**
     * 刷新列表
     * @param list list
     */
    public synchronized void refresh(List<T> list) {
        this.list = list == null ? null : new ArrayList<T>(list);
        notifyListDataSetChanged(); // 仅对 AbsListView 有效
    }

    // RecyclerAdapter <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    //    @Override

    /**
     * onCreateViewHolder bind view
     * @param parent parent
     * @param viewType viewType
     * @return BV
     */
    public BV onCreateViewHolder(final ComponentContainer parent, int viewType) {
        final BV bv = (BV) createView(viewType, parent);
        bv.createView();
        bv.setOnViewClickListener(onViewClickListener);
        bv.itemView.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        if (onItemClickListener != null) {
                            onItemClickListener.onItemClicked(null, bv.itemView, bv.position, getItemId(bv.position));
                        }
                    }
                });
        return bv;
    }

    //    @Override

    /**
     * bindview数据绑定
     * @param bv  b
     * @param position p
     */
    public void onBindViewHolder(BV bv, int position) {
        bindView(position, bv);
    }
    //

    // RecyclerAdapter >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // ListAdapter <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    private final DataObservable mDataSetObservable = new DataObservable();


    /**
     * 注册数据观察者
     * @param observer o
     */
    public void registerDataSetObserver(DataObserver observer) {
        mDataSetObservable.add(observer);
    }

    /**
     * 取消注册数据观察者
     * @param observer o
     */
    public void unregisterDataSetObserver(DataObserver observer) {
        mDataSetObservable.remove(observer);
    }

    /**
     * Notifies the attached observers that the underlying data has been changed
     * and any View reflecting the data set should refresh itself.
     */
    public void notifyListDataSetChanged() {
        notifyDataChanged();
    }

    /**
     * Notifies the attached observers that the underlying data is no longer valid
     * or available. Once invoked this adapter is no longer valid and should
     * not report further data set changes.
     */
    public void notifyDataSetInvalidated() {
        mDataSetObservable.notifyObservers();
    }

    public boolean areAllItemsEnabled() {
        return true;
    }

    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        BV bv = convertView == null ? null : (BV) convertView.getTag();
        if (bv == null) {
            bv = onCreateViewHolder(parent, getItemViewType(position));
            convertView = bv.itemView;
            convertView.setTag(bv);
        }
        onBindViewHolder(bv, position);
        return convertView;
    }

    /**
     * 获取item类型
     * @param position p
     * @return int
     */
    public int getItemViewType(int position) {
        return 0;
    }

    /**
     * 获取type数量
     * @return int
     */
    public int getViewTypeCount() {
        return 1;
    }

    /**
     * 是否为空
     * @return boolean
     */
    public boolean isEmpty() {
        return getCount() <= 0;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }
    /**获取item数据
     * @param position position
     * @return T
     */
    @Override
    public T getItem(int position) {
        return list.get(position);
    }
    /**获取item的id，如果不能满足需求可在子类重写
     * @param position position
     * @return position
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    // ListAdapter >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

}
