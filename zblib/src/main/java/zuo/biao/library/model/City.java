package zuo.biao.library.model;

import java.io.Serializable;

/**
 * city类
 */
public class City implements Serializable {
    private static final long serialVersionUID = 1L;
    private String province;
    private String city;

    /**
     * 经度
     */
    private Double longitude;

    /**
     * 纬度
     */
    private Double latitude;

    public City() {}

    public City(String province, String city, Double lat, Double lon) {
        super();
        this.province = province;
        this.city = city;
        this.latitude = lat;
        this.longitude = lon;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
}
