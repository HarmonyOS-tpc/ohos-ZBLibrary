package zuo.biao.library.model;

import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

import java.io.Serializable;

/**
 * ================================================
 * 作    者：jeasonlzy（廖子尧 Github地址：https://github.com/jeasonlzy0216
 * 版    本：1.0
 * 创建日期：2016/5/19
 * 描    述：图片信息
 * 修订历史：
 * ================================================
 */
public class ImageItem implements Serializable, Sequenceable {
    /**
     * 图片的名字
     */
    public String name;
    /**
     * 图片的路径
     */
    public String uriSchema;
    /**
     * 图片的大小
     */
    public long size;
    /**
     * 图片的宽度
     */
    public int width;
    /**
     *图片的高度
     */
    public int height;
    /**
     * 图片的类型
     */
    public String mimeType;
    /**
     * 图片的创建时间
     */
    public long addTime;

    /**
     * 图片的路径和创建时间相同就认为是同一张图片
     * @param o o
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof ImageItem) {
            ImageItem item = (ImageItem) o;
            return this.uriSchema.equalsIgnoreCase(item.uriSchema) && this.addTime == item.addTime;
        }

        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public ImageItem() {}

    protected ImageItem(Parcel in) {
        this.name = in.readString();
        this.uriSchema = in.readString();
        this.size = in.readLong();
        this.width = in.readInt();
        this.height = in.readInt();
        this.mimeType = in.readString();
        this.addTime = in.readLong();
    }

    /**
     * createFromParcel
     */
    public static final Producer<ImageItem> PRODUCER =
            new Producer<ImageItem>() {
                @Override
                public ImageItem createFromParcel(Parcel source) {
                    return new ImageItem(source);
                }
            };

    @Override
    public boolean marshalling(Parcel parcel) {
        parcel.writeString(this.name);
        parcel.writeString(this.uriSchema);
        parcel.writeLong(this.size);
        parcel.writeInt(this.width);
        parcel.writeInt(this.height);
        parcel.writeString(this.mimeType);
        parcel.writeLong(this.addTime);
        return true;
    }

    @Override
    public boolean unmarshalling(Parcel in) {
        this.name = in.readString();
        this.uriSchema = in.readString();
        this.size = in.readLong();
        this.width = in.readInt();
        this.height = in.readInt();
        this.mimeType = in.readString();
        this.addTime = in.readLong();
        return true;
    }

    @Override
    public String toString() {
        return "ImageItem{"
                + "name='"
                + name
                + '\''
                + ", path='"
                + uriSchema
                + '\''
                + ", size="
                + size
                + ", width="
                + width
                + ", height="
                + height
                + ", mimeType='"
                + mimeType
                + '\''
                + ", addTime="
                + addTime
                + '}';
    }
}
