package zuo.biao.library.model;

import ohos.agp.components.element.Element;

/**
 * Date: 3 MAR 2018
 * Time: 13:58 MSK
 *
 * @author Michael Bel
 */
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * 底部弹框类
 */
public class BottomSheetItem {
    /**
     * 图标
     */
    public Element icon;
    /**
     * 字符
     */
    public String text;

    public BottomSheetItem() {}

    public BottomSheetItem(String text, Element icon) {
        this.text = text;
        this.icon = icon;
    }
}
