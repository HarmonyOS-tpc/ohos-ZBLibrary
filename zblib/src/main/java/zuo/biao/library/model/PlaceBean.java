package zuo.biao.library.model;

/**
 * 地址类
 */
public class PlaceBean {
    /**
     * 地址code
     */
    public String code;

    /**
     * 地址name
     */
    public String name;
}
