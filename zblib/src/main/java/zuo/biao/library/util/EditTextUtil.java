/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
limitations under the License.*/

package zuo.biao.library.util;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.Component;
import ohos.agp.components.TextField;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**通用密码、手机号、验证码输入框输入字符判断及错误提示 类
 * @author Lemon
 * @use EditTextUtil.xxxMethod(...);
 */
public class EditTextUtil {
    private static final String TAG = "EditTextUtil";


    /**隐藏输入法
     * @param context 上下文
     * @param toGetWindowTokenView view
     */
    public static void hideKeyboard(Context context, Component toGetWindowTokenView){
        showKeyboard(context, null, toGetWindowTokenView, false);
    }
    /**显示输入法
     * @param context 上下文
     * @param et TextField
     */
    public static void showKeyboard(Context context, TextField et){
        showKeyboard(context, et, true);
    }
    /**显示/隐藏输入法
     * @param context 上下文
     * @param et TextField
     * @param show 是否展示
     */
    public static void showKeyboard(Context context, TextField et, boolean show){
        showKeyboard(context, et, null, show);
    }
    /**显示输入法
     * @param context 上下文
     * @param et TextField
     * @param toGetWindowTokenView(为null时toGetWindowTokenView = et) 包含et的父View，键盘根据toGetWindowTokenView的位置来弹出/隐藏
     */
    public static void showKeyboard(Context context, TextField et, Component toGetWindowTokenView) {
        showKeyboard(context, et, toGetWindowTokenView, true);
    }
    /**显示/隐藏输入法
     * @param context 上下文
     * @param et TextField
     * @param toGetWindowTokenView(为null时toGetWindowTokenView = et) 包含et的父View，键盘根据toGetWindowTokenView的位置来弹出/隐藏
     * @param show 是否展示
     */
    @Deprecated
    public static void showKeyboard(Context context, TextField et, Component toGetWindowTokenView, boolean show){

    }

    //显示/隐藏输入法>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    private static final int TYPE_NOT_ALLOWED_EMPTY = 0;

    private static final int TYPE_VERIFY = 1;
    private static final int TYPE_PASSWORD = 2;
    private static final int TYPE_PHONE = 3;
    private static final int TYPE_MAIL = 4;

    /**判断edittext输入文字是否合法
     * @param context context
     * @param et et
     * @return boolean
     */
    public static boolean isInputedCorrect(Ability context, TextField et) {
        return isInputedCorrect(context, et, TYPE_NOT_ALLOWED_EMPTY, null);
    }
    /**判断edittext输入文字是否合法
     * @param context c
     * @param et e
     * @param errorRemind e
     * @return isInputedCorrect
     */
    public static boolean isInputedCorrect(Ability context, TextField et, String errorRemind) {
        return isInputedCorrect(context, et, TYPE_NOT_ALLOWED_EMPTY, errorRemind);
    }
    /**判断edittext输入文字是否合法
     * @param context c
     * @param et e
     * @param type  t
     * @return boolean
     */
    public static boolean isInputedCorrect(Ability context, TextField et, int type) {
        return isInputedCorrect(context, et, type, null);
    }
    /**判断edittext输入文字是否合法
     * @param context c
     * @param stringResId s
     * @param et e
     * @return boolean
     */
    public static boolean isInputedCorrect(Ability context, int stringResId, TextField et) {
        return isInputedCorrect(context, et, TYPE_NOT_ALLOWED_EMPTY, stringResId);
    }
    /**判断edittext输入文字是否合法
     * @param context c
     * @param et e
     * @param type t
     * @param stringResId s
     * @return boolean
     */
    public static boolean isInputedCorrect(Ability context, TextField et, int type, int stringResId) {
        try {
            if (context != null && stringResId > 0) {
                return isInputedCorrect(context, et, type, ResTUtil.getString(context, stringResId));
            }
        } catch (Exception e) {
            Log.error(
                    TAG, "isInputedCorrect try { if (context != null && stringResId > 0) {catch (Exception e) " + e.getMessage());
        }
        return false;
    }
    /**判断edittext输入文字是否合法
     * @param context c
     * @param et e
     * @param type t
     * @param errorRemind e
     * @return boolean
     */
    public static boolean isInputedCorrect(Ability context, TextField et, int type, String errorRemind) {
        if (context == null || et == null) {
            Log.error(TAG, "isInputedCorrect context == null || et == null >> return false;");
            return false;
        }
        Color co = et.getHintColor();

        String inputed = StringUtil.getTrimedString(et);
        switch (type) {
            case TYPE_VERIFY:
                if (type == TYPE_VERIFY && inputed.length() < 4) {
                    return showInputedError(
                            context, et, StringUtil.isNotEmpty(errorRemind, true) ? errorRemind : "验证码不能小于4位");
                }
                break;
            case TYPE_PASSWORD:
                if (inputed.length() < 6) {
                    return showInputedError(
                            context, et, StringUtil.isNotEmpty(errorRemind, true) ? errorRemind : "密码不能小于6位");
                }
                if (StringUtil.isNumberOrAlpha(inputed) == false) {
                    return showInputedError(
                            context, et, StringUtil.isNotEmpty(errorRemind, true) ? errorRemind : "密码只能含有字母或数字");
                }
                break;
            case TYPE_PHONE:
                if (inputed.length() != 11) {
                    return showInputedError(
                            context, et, StringUtil.isNotEmpty(errorRemind, true) ? errorRemind : "请输入11位手机号");
                }
                if (StringUtil.isPhone(inputed) == false) {
                    CommonUtil.showShortToast(context, inputed);
                    return false;
                }
                break;
            case TYPE_MAIL:
                if (StringUtil.isEmail(inputed) == false) {
                    return showInputedError(context, "您输入的邮箱格式不对哦~");
                }
                break;
            default:
                if (StringUtil.isNotEmpty(inputed, true) == false
                        || inputed.equals(StringUtil.getTrimedString(et.getHint()))) {
                    return showInputedError(
                            context,
                            et,
                            StringUtil.isNotEmpty(errorRemind, true) ? errorRemind : StringUtil.getTrimedString(et));
                }
                break;
        }

        et.setHintColor(co);
        return true;
    }

    /**字符不合法提示(toast)
     * @param context c
     * @param resId id
     * @return boolean
     */
    public static boolean showInputedError(Ability context, int resId) {
        return showInputedError(context, null, resId);
    }
    /**字符不合法提示(et == null ? toast : hint)
     * @param context context
     * @param et et
     * @param resId resId
     * @return boolean
     */
    public static boolean showInputedError(Ability context, TextField et, int resId) {
        try {
            return showInputedError(context, et, ResTUtil.getString(context, resId));
        } catch (Exception e) {
            Log.error(TAG, "" + e.getMessage());
        }
        return false;
    }
    /**字符不合法提示(toast)
     * @param context context
     * @param string string
     * @return boolean
     */
    public static boolean showInputedError(Ability context, String string) {
        return showInputedError(context, null, string);
    }
    /**字符不合法提示(et == null ? toast : hint)
     * @param context context
     * @param et et
     * @param string string
     * @return boolean
     */
    public static boolean showInputedError(Ability context, TextField et, String string) {
        if (context == null || StringUtil.isNotEmpty(string, false) == false) {
            Log.error(
                    TAG,
                    "showInputedError  context == null || et == null || StringUtil.isNotEmpty(string, false) == false"
                            + " >> return false;");
            return false;
        }
        if (et == null) {
            CommonUtil.showShortToast(context, string);
        } else {
            et.setText("");
            et.setHint(string);
            et.setHintColor(Color.RED);
        }
        return false;
    }

    // 对输入字符判断>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

}
