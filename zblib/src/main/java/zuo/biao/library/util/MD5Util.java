/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
limitations under the License.*/

package zuo.biao.library.util;

/**MD5加密工具类
 */
public class MD5Util {
    /**
     * 对字符串进行 MD5 加密
     *
     * @param str s
     *            待加密字符串
     * @return 加密后字符串
     */
    public static String MD5(String str) {
        if (StringUtil.isNotEmpty(str, false) == false) {
            return "";
        }
        ECodeMD5 md = new ECodeMD5();
        return md.digest(str);
    }
}
