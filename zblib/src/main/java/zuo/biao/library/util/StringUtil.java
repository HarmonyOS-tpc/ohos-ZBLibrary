/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
limitations under the License.*/

package zuo.biao.library.util;

import ohos.agp.components.Text;

import java.io.File;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 通用字符串(String)相关类,为null时返回""
 *
 * @author Lemon
 * @use StringUtil.xxxMethod(...);
 */
public class StringUtil {
    private static final String TAG = "StringUtil";

    private static String currentString = "";

    /**
     * 获取刚传入处理后的string
     *
     * @return str
     * @must 上个影响currentString的方法 和 这个方法都应该在同一线程中，否则返回值可能不对
     */
    public static String getCurrentString() {
        return currentString == null ? "" : currentString;
    }

    // 获取string,为null时返回"" <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    /**
     * 获取string,为null则返回""
     *
     * @param tv tv
     * @return str
     */
    public static String get(Text tv) {
        if (tv == null || tv.getText() == null) {
            return "";
        }
        return tv.getText().toString();
    }

    /**
     * 获取string,为null则返回""
     *
     * @param object object
     * @return str
     */
    public static String get(Object object) {
        return object == null ? "" : object.toString();
    }

    /**
     * 获取string,为null则返回""
     *
     * @param cs cs
     * @return String
     */
    public static String get(CharSequence cs) {
        return cs == null ? "" : cs.toString();
    }

    /**
     * 获取string,为null则返回""
     *
     * @param s s
     * @return str
     */
    public static String get(String s) {
        return s == null ? "" : s;
    }

    /**
     * deprecated 用get代替，这个保留到17.0
     *
     * @param tv tv
     * @return str
     */
    public static String getString(Text tv) {
        return get(tv);
    }

    /**
     * deprecated 用get代替，这个保留到17.0
     *
     * @param object object
     * @return str
     */
    public static String getString(Object object) {
        return get(object);
    }

    /**
     * deprecated 用get代替，这个保留到17.0
     *
     * @param cs cs
     * @return str
     */
    public static String getString(CharSequence cs) {
        return get(cs);
    }

    /**
     * deprecated 用get代替，这个保留到17.0
     *
     * @param s s
     * @return str
     */
    public static String getString(String s) {
        return get(s);
    }

    // 获取string,为null时返回"" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // 获取去掉前后空格后的string<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    /**
     * 获取去掉前后空格后的string,为null则返回""
     *
     * @param tv tv
     * @return str
     */
    public static String trim(Text tv) {
        return trim(get(tv));
    }

    /**
     * 获取去掉前后空格后的string,为null则返回""
     *
     * @param object object
     * @return str
     */
    public static String trim(Object object) {
        return trim(get(object));
    }

    /**
     * 获取去掉前后空格后的string,为null则返回""
     *
     * @param cs cs
     * @return str
     */
    public static String trim(CharSequence cs) {
        return trim(get(cs));
    }

    /**
     * 获取去掉前后空格后的string,为null则返回""
     *
     * @param s s
     * @return str
     */
    public static String trim(String s) {
        return s == null ? "" : s.trim();
    }

    /**
     * deprecated 用trim代替，这个保留到17.0
     *
     * @param tv tv
     * @return str
     */
    public static String getTrimedString(Text tv) {
        return trim(tv);
    }

    /**
     * deprecated 用trim代替，这个保留到17.0
     *
     * @param object object
     * @return str
     */
    public static String getTrimedString(Object object) {
        return trim(object);
    }

    /**
     * deprecated 用trim代替，这个保留到17.0
     *
     * @param cs cs
     * @return str
     */
    public static String getTrimedString(CharSequence cs) {
        return trim(get(cs));
    }

    /**
     * deprecated 用trim代替，这个保留到17.0
     *
     * @param s s
     * @return str
     */
    public static String getTrimedString(String s) {
        return trim(s);
    }

    // 获取去掉前后空格后的string>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // 获取去掉所有空格后的string <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    /**
     * 获取去掉所有空格后的string,为null则返回""
     *
     * @param tv tv
     * @return str
     */
    public static String noBlank(Text tv) {
        return noBlank(get(tv));
    }

    /**
     * 获取去掉所有空格后的string,为null则返回""
     *
     * @param object object
     * @return str
     */
    public static String noBlank(Object object) {
        return noBlank(get(object));
    }

    /**
     * 获取去掉所有空格后的string,为null则返回""
     *
     * @param cs cs
     * @return str
     */
    public static String noBlank(CharSequence cs) {
        return noBlank(get(cs));
    }

    /**
     * 获取去掉所有空格后的string,为null则返回""
     *
     * @param s s
     * @return str
     */
    public static String noBlank(String s) {
        return get(s).replaceAll(" ", "");
    }

    /**
     * deprecated 用noBlank代替，这个保留到17.0
     *
     * @param tv tv
     * @return str
     */
    public static String getNoBlankString(Text tv) {
        return noBlank(get(tv));
    }

    /**
     * deprecated 用noBlank代替，这个保留到17.0
     *
     * @param object object
     * @return str
     */
    public static String getNoBlankString(Object object) {
        return noBlank(get(object));
    }

    /**
     * deprecated 用noBlank代替，这个保留到17.0
     *
     * @param cs cs
     * @return str
     */
    public static String getNoBlankString(CharSequence cs) {
        return noBlank(get(cs));
    }

    /**
     * deprecated 用noBlank代替，这个保留到17.0
     *
     * @param s s
     * @return str
     */
    public static String getNoBlankString(String s) {
        return noBlank(s);
    }

    // 获取去掉所有空格后的string >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // 获取string的长度<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    /**
     * 获取string的长度,为null则返回0
     *
     * @param tv tv
     * @return int
     */
    public static int length(Text tv) {
        return length(get(tv));
    }

    /**
     * 获取string的长度,为null则返回0
     *
     * @param object object
     * @return int
     */
    public static int length(Object object) {
        return length(get(object));
    }

    /**
     * 获取string的长度,为null则返回0
     *
     * @param cs cs
     * @return int
     */
    public static int length(CharSequence cs) {
        return length(get(cs));
    }

    /**
     * 获取string的长度,为null则返回0
     *
     * @param s s
     * @return int
     */
    public static int length(String s) {
        return get(s).length();
    }

    /**
     * deprecated 用length代替，这个保留到17.0
     *
     * @param tv tv
     * @param trim trim
     * @return int
     */
    public static int getLength(Text tv, boolean trim) {
        return getLength(get(tv), trim);
    }

    /**
     * deprecated 用length代替，这个保留到17.0
     *
     * @param object object
     * @param trim trim
     * @return int
     */
    public static int getLength(Object object, boolean trim) {
        return getLength(get(object), trim);
    }

    /**
     * deprecated 用length代替，这个保留到17.0
     *
     * @param trim trim
     * @param cs cs
     * @return int
     */
    public static int getLength(CharSequence cs, boolean trim) {
        return getLength(get(cs), trim);
    }

    /**
     * deprecated 用length代替，这个保留到17.0
     *
     * @param trim trim
     * @param s s
     * @return int
     */
    public static int getLength(String s, boolean trim) {
        s = trim ? getTrimedString(s) : s;
        return length(s);
    }

    // 获取string的长度>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // 判断字符是否为空 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    /**
     * 判断字符是否为空
     * trim = true
     *
     * @param s s
     * @return boolean
     */
    public static boolean isEmpty(String s) {
        return isEmpty(s, true);
    }

    /**
     * 判断字符是否为空
     *
     * @param tv tv
     * @param trim trim
     * @return boolean
     */
    public static boolean isEmpty(Text tv, boolean trim) {
        return isEmpty(get(tv), trim);
    }

    /**
     * 判断字符是否为空
     *
     * @param object object
     * @param trim trim
     * @return boolean
     */
    public static boolean isEmpty(Object object, boolean trim) {
        return isEmpty(get(object), trim);
    }

    /**
     * 判断字符是否为空
     *
     * @param cs cs
     * @param trim trim
     * @return boolean
     */
    public static boolean isEmpty(CharSequence cs, boolean trim) {
        return isEmpty(get(cs), trim);
    }

    /**
     * 判断字符是否为空
     *
     * @param s s
     * @param trim trim
     * @return boolean
     */
    public static boolean isEmpty(String s, boolean trim) {
        //		//Log.i(TAG, "isEmpty   s = " + s);
        if (s == null) {
            return true;
        }
        if (trim) {
            s = s.trim();
        }
        if (s.length() <= 0) {
            return true;
        }

        currentString = s;

        return false;
    }
    // 判断字符是否为空 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // 判断字符是否非空 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    /**
     * 判断字符是否非空
     *
     * @param tv tv
     * @param trim trim
     * @return boolean
     */
    public static boolean isNotEmpty(Text tv, boolean trim) {
        return isNotEmpty(get(tv), trim);
    }

    /**
     * 判断字符是否非空
     *
     * @param object object
     * @param trim trim
     * @return boolean
     */
    public static boolean isNotEmpty(Object object, boolean trim) {
        return isNotEmpty(get(object), trim);
    }

    /**
     * 判断字符是否非空
     *
     * @param cs cs
     * @param trim trim
     * @return boolean
     */
    public static boolean isNotEmpty(CharSequence cs, boolean trim) {
        return isNotEmpty(get(cs), trim);
    }

    /**
     * 判断字符是否非空
     *
     * @param s s
     * @param trim trim
     * @return boolean
     */
    public static boolean isNotEmpty(String s, boolean trim) {
        return !isEmpty(s, trim);
    }

    // 判断字符是否非空 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // 判断字符类型 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    // 判断手机格式是否正确

    /**
     * 判断手机格式是否正确
     *
     * @param phone phone
     * @return boolean
     */
    public static boolean isPhone(String phone) {
        if (isEmpty(phone, true)) {
            return false;
        }

        Pattern p = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0-2,5-9])|(17[0-9]))\\d{8}$");

        currentString = phone;

        return p.matcher(phone).matches();
    }

    /**
     * 判断email格式是否正确
     *
     * @param email email
     * @return boolean
     */
    public static boolean isEmail(String email) {
        if (isEmpty(email, true)) {
            return false;
        }

        String str =
                "^([a-zA-Z0-9_\\-.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(]?)$";
        Pattern p = Pattern.compile(str);

        currentString = email;

        return p.matcher(email).matches();
    }

    /**
     * deprecated，保留到17.0
     *
     * @param number number
     * @return boolean
     */
    public static boolean isNumer(String number) {
        return isNumber(number);
    }

    /**
     * 判断是否全是数字
     * @param number number
     * @return boolean
      */

    public static boolean isNumber(String number) {
        if (isEmpty(number, true)) {
            return false;
        }

        Pattern pattern = Pattern.compile("[0-9]*");
        Matcher isNum = pattern.matcher(number);
        if (isNum.matches() == false) {
            return false;
        }

        currentString = number;

        return true;
    }

    /**
     * 判断字符类型是否是号码或字母
     *
     * @param s s
     * @return bo
     */
    public static boolean isNumberOrAlpha(String s) {
        if (s == null) {
            return false;
        }
        Pattern pNumber = Pattern.compile("[0-9]*");
        Matcher mNumber;
        Pattern pAlpha = Pattern.compile("[a-zA-Z]");
        Matcher mAlpha;
        for (int i = 0; i < s.length(); i++) {
            mNumber = pNumber.matcher(s.substring(i, i + 1));
            mAlpha = pAlpha.matcher(s.substring(i, i + 1));
            if (!mNumber.matches() && !mAlpha.matches()) {
                return false;
            }
        }

        currentString = s;
        return true;
    }

    /**
     * 判断字符类型是否是身份证号
     *
     * @param idCard idCard
     * @return boolean
     */
    public static boolean isIDCard(String idCard) {
        if (isNumberOrAlpha(idCard) == false) {
            return false;
        }
        idCard = get(idCard);
        if (idCard.length() == 15) {
            currentString = idCard;
            return true;
        }
        if (idCard.length() == 18) {
            currentString = idCard;
            return true;
        }

        return false;
    }

    private static final String HTTP = "http";
    private static final String URL_PREFIX = "http://";
    private static final String URL_PREFIXS = "https://";
    private static final String URL_STAFFIX = URL_PREFIX;
    private static final String URL_STAFFIXs = URL_PREFIXS;

    /**
     * 判断字符类型是否是网址
     *
     * @param url url
     * @return boolean
     */
    public static boolean isUrl(String url) {
        if (isEmpty(url, true)) {
            return false;
        } else if (!url.startsWith(URL_PREFIX) && !url.startsWith(URL_PREFIXS)) {
            return false;
        }

        currentString = url;
        return true;
    }

    private static final String FILE_PATH_PREFIX = "file://";

    /**
     * 判断文件路径是否存在
     *
     * @param path path
     * @return boolean
     */
    public static boolean isFilePathExist(String path) {
        return StringUtil.isFilePath(path) && new File(path).exists();
    }

    /**
     * 判断字符类型是否是路径
     *
     * @param path path
     * @return boolean
     */
    public static boolean isFilePath(String path) {
        if (isEmpty(path, true)) {
            return false;
        }

        if (!path.contains(".") || path.endsWith(".")) {
            return false;
        }

        currentString = path;

        return true;
    }

    // 判断字符类型 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // 提取特殊字符<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    /**
     * 去掉string内所有非数字类型字符
     *
     * @param tv tv
     * @return String
     */
    public static String getNumber(Text tv) {
        return getNumber(get(tv));
    }

    /**
     * 去掉string内所有非数字类型字符
     *
     * @param object object
     * @return String
     */
    public static String getNumber(Object object) {
        return getNumber(get(object));
    }

    /**
     * 去掉string内所有非数字类型字符
     *
     * @param cs cs
     * @return String
     */
    public static String getNumber(CharSequence cs) {
        return getNumber(get(cs));
    }

    /**
     * 去掉string内所有非数字类型字符
     *
     * @param s s
     * @return String
     */
    public static String getNumber(String s) {
        if (isEmpty(s, true)) {
            return "";
        }

        String numberString = "";
        String single;
        for (int i = 0; i < s.length(); i++) {
            single = s.substring(i, i + 1);
            if (isNumer(single)) {
                numberString += single;
            }
        }

        return numberString;
    }

    // 提取特殊字符>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // 校正（自动补全等）字符串<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    /**
     * 获取网址，自动补全
     *
     * @param tv tv
     * @return String
     */
    public static String getCorrectUrl(Text tv) {
        return getCorrectUrl(get(tv));
    }

    /**
     * 获取网址，自动补全
     *
     * @param url url
     * @return String
     */
    public static String getCorrectUrl(String url) {
        if (isEmpty(url, true)) {
            return "";
        }


        return isUrl(url) ? url : URL_PREFIX + url;
    }

    /**
     * 获取去掉所有 空格 、"-" 、"+86" 后的phone
     *
     * @param tv tv
     * @return String
     */
    public static String getCorrectPhone(Text tv) {
        return getCorrectPhone(get(tv));
    }

    /**
     * 获取去掉所有 空格 、"-" 、"+86" 后的phone
     *
     * @param phone phone
     * @return String
     */
    public static String getCorrectPhone(String phone) {
        if (isEmpty(phone, true)) {
            return "";
        }

        phone = noBlank(phone);
        phone = phone.replaceAll("-", "");
        if (phone.startsWith("+86")) {
            phone = phone.substring(3);
        }
        return phone;
    }

    /**
     * 获取邮箱，自动补全
     *
     * @param tv tv
     * @return String
     */
    public static String getCorrectEmail(Text tv) {
        return getCorrectEmail(get(tv));
    }

    /**
     * 获取邮箱，自动补全
     *
     * @param email email
     * @return String
     */
    public static String getCorrectEmail(String email) {
        if (isEmpty(email, true)) {
            return "";
        }

        email = noBlank(email);
        if (isEmail(email) == false && !email.endsWith(".com")) {
            email += ".com";
        }

        return email;
    }

    private static final int PRICE_FORMAT_DEFAULT = 0;
    private static final int PRICE_FORMAT_PREFIX = 1;
    private static final int PRICE_FORMAT_SUFFIX = 2;
    private static final int PRICE_FORMAT_PREFIX_WITH_BLANK = 3;
    private static final int PRICE_FORMAT_SUFFIX_WITH_BLANK = 4;
    private static final String[] PRICE_FORMATS = {"", "￥", "元", "￥ ", " 元"};

    /**
     * 获取价格，保留两位小数
     *
     * @param price price
     * @return String
     */
    public static String getPrice(String price) {
        return getPrice(price, PRICE_FORMAT_DEFAULT);
    }

    /**
     * 获取价格，保留两位小数
     *
     * @param price price
     * @param formatType 添加单位（元）
     * @return String
     */
    public static String getPrice(String price, int formatType) {
        if (isEmpty(price, true)) {
            return getPrice(0, formatType);
        }

        // 单独写到getCorrectPrice? <<<<<<<<<<<<<<<<<<<<<<
        String correctPrice = "";
        String s;
        for (int i = 0; i < price.length(); i++) {
            s = price.substring(i, i + 1);
            if (".".equals(s) || isNumer(s)) {
                correctPrice += s;
            }
        }
        // 单独写到getCorrectPrice? >>>>>>>>>>>>>>>>>>>>>>

        if (correctPrice.contains(".")) {
            if (correctPrice.endsWith(".")) {
                correctPrice = correctPrice.replaceAll(".", "");
            }
        }

        return isEmpty(correctPrice, true)
                ? getPrice(0, formatType)
                : getPrice(new BigDecimal(0 + correctPrice), formatType);
    }

    /**
     * 获取价格，保留两位小数
     *
     * @param price price
     * @return String
     */
    public static String getPrice(BigDecimal price) {
        return getPrice(price, PRICE_FORMAT_DEFAULT);
    }

    /**
     * 获取价格，保留两位小数
     *
     * @param price price
     * @return String
     */
    public static String getPrice(double price) {
        return getPrice(price, PRICE_FORMAT_DEFAULT);
    }

    /**
     * 获取价格，保留两位小数
     *
     * @param price price
     * @param formatType 添加单位（元）
     * @return String
     */
    public static String getPrice(BigDecimal price, int formatType) {
        return getPrice(price == null ? 0 : price.doubleValue(), formatType);
    }

    /**
     * 获取价格，保留两位小数
     *
     * @param price price
     * @param formatType 添加单位（元）
     * @return String
     */
    public static String getPrice(double price, int formatType) {
        String s = new DecimalFormat("#########0.00").format(price);
        switch (formatType) {
            case PRICE_FORMAT_PREFIX:
                return PRICE_FORMATS[PRICE_FORMAT_PREFIX] + s;
            case PRICE_FORMAT_SUFFIX:
                return s + PRICE_FORMATS[PRICE_FORMAT_SUFFIX];
            case PRICE_FORMAT_PREFIX_WITH_BLANK:
                return PRICE_FORMATS[PRICE_FORMAT_PREFIX_WITH_BLANK] + s;
            case PRICE_FORMAT_SUFFIX_WITH_BLANK:
                return s + PRICE_FORMATS[PRICE_FORMAT_SUFFIX_WITH_BLANK];
            default:
                return s;
        }
    }

    // 校正（自动补全等）字符串>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

}
