/// *Copyright ©2015 TommyLemon(https://github.com/TommyLemon)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.*/
//
package zuo.biao.library.util;

import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;

/**
 * 应用设置工具类
 *
 * @author Lemon
 * @must application中在DataKeeper.init();后SettingUtil.init(...);
 * @warn 修改服务器地址（URL_SERVER_ADDRESS_NORMAL_HTTP等）
 */
public final class SettingUtil {
    private static final String TAG = "SettingUtil";

    /**
     * 应用已发布
     */
    public static final boolean ISRELEASED = false; //

    /**
     * 建议改成你自己项目的路径
     */
    public static final String APP_SETTING = "SHARE_PREFS_" + "APP_SETTING";

    private SettingUtil() {
        /* 不能实例化* */
    }

    private static final String KEY_CACHE = "KEY_CACHE"; // 开启缓存
    private static final String KEY_PRELOAD = "KEY_PRELOAD"; // 开启预加载

    private static final String KEY_VOICE = "KEY_VOICE"; // 开启通知声
    private static final String KEY_VIBRATE = "KEY_VIBRATE"; // 开启震动
    private static final String KEY_NO_DISTURB = "KEY_NO_DISTURB"; // 夜间防打扰

    /**
     * 测试模式
     */
    public static final String KEY_IS_ON_TEST_MODE = "KEY_IS_ON_TEST_MODE"; // 测试模式
    private static final String KEY_IS_FIRST_START = "KEY_IS_FIRST_START"; // 第一次打开应用

    private static final String[] KEYS = {
            KEY_CACHE, KEY_PRELOAD, KEY_VOICE, KEY_VIBRATE, KEY_NO_DISTURB, KEY_IS_ON_TEST_MODE, KEY_IS_FIRST_START,
    };


    /**
     * 开启缓存
     */
    public static boolean cache = true; // 开启缓存
    private static boolean preload = true; // 开启预加载

    private static boolean voice = true; // 开启通知声
    private static boolean vibrate = true; // 开启震动
    private static boolean nDisturb = true; // 夜间防打扰

    /**
     * 测试模式
     */
    public static boolean isOnTestMode = false; // 测试模式
    /**
     * 第一次打开应用
     */
    public static boolean isFirstStart = true;

    /**
     * 默认defaultValues
     */
    public static  boolean[] defaultValues;

    /**
     * 默认defaultValues数组
     */
    static {
        defaultValues =
                new boolean[]{
                        cache, // 开启缓存
                        preload, // 开启预加载
                        voice, // 开启通知声
                        vibrate, // 开启震动
                        nDisturb, // 夜间防打扰
                        isOnTestMode, // 测试模式
                        isFirstStart, // 第一次打开应用
                };
    }

    private static Context context;

    /**
     * 初始化
     *
     * @param context_ context_
     */
    public static void init(Context context_) {
        context = context_;

        cache = getBoolean(KEY_CACHE, cache);
        preload = getBoolean(KEY_PRELOAD, preload);

        voice = getBoolean(KEY_VOICE, voice);
        vibrate = getBoolean(KEY_VIBRATE, vibrate);
        nDisturb = getBoolean(KEY_NO_DISTURB, nDisturb);

        isOnTestMode = getBoolean(KEY_IS_ON_TEST_MODE, isOnTestMode);
        isFirstStart = getBoolean(KEY_IS_FIRST_START, isFirstStart);
    }

    /**
     * 恢复默认
     */
    public static void restoreDefault() {
        for (int i = 0; i < KEYS.length; i++) {
            putBoolean(KEYS[i], defaultValues[i]);
        }

        init(context);
    }

    /**
     * 判断是否存在key
     *
     * @param key key
     * @return boolean
     */
    public static boolean isContainKey(String key) {
        return getKeyIndex(key) >= 0;
    }

    /**
     * 获取key在KEYS中的位置
     *
     * @param key key
     * @return int
     */
    public static int getKeyIndex(String key) {
        key = StringUtil.getTrimedString(key);
        for (int i = 0; i < KEYS.length; i++) {
            if (key.equals(KEYS[i])) {
                return i;
            }
        }

        return -1;
    }

    /**
     * getBoolean
     *
     * @param key key
     * @param defaultValue defaultValue
     * @return getBoolean
     */
    public static boolean getBoolean(String key, boolean defaultValue) {
        if (isContainKey(key) == false) {
            Log.error(TAG, "writeBoolean  isContainKey(key) == false >> return defaultValue;");
            return defaultValue;
        }

        Preferences sdf = new DatabaseHelper(context).getPreferences(APP_SETTING);
        return sdf.getBoolean(key, defaultValue);
    }

    /**
     * 设置所有boolean
     *
     * @param values values
     */
    public static void putAllBoolean(boolean[] values) {
        if (values == null || values.length != KEYS.length) {
            Log.error(TAG, "putAllBoolean  values == null || values.length != KEYS.length >> return;");
            return;
        }

        Preferences sdf = new DatabaseHelper(context).getPreferences(APP_SETTING);

        sdf.clear();
        for (int i = 0; i < values.length; i++) {
            sdf.putBoolean(KEYS[i], values[i]);
        }
        sdf.flushSync();
        init(context);
    }

    /**
     * boolean类型
     *
     * @param key key
     * @param value value
     */
    public static void putBoolean(String key, boolean value) {
        int keyIndex = getKeyIndex(key);
        if (keyIndex <= 0) {
            Log.error(TAG, "writeBoolean  keyIndex <= 0 >> return;");
            return;
        }

        new DatabaseHelper(context)
                .getPreferences(APP_SETTING)
                .delete(key) // 防止因类型不同导致崩溃
                .putBoolean(key, value)
                .flushSync();
        init(context);
    }

    /**
     * 获取所有boolean值
     *
     * @param context context
     * @return boolean
     */
    public static boolean[] getAllBooleans(Context context) {
        init(context);
        return new boolean[]{
                cache, preload, voice, vibrate, nDisturb, isOnTestMode, isFirstStart,
        };
    }

    private static final int[] NO_DISTURB_START_TIME = {23, 0};
    private static final int[] NO_DISTURB_END_TIME = {6, 0};

    /**
     * 免打扰
     *
     * @return boolean
     */
    public static boolean noDisturb() {
        return getBoolean(KEY_NO_DISTURB, nDisturb)
                && TimeUtil.isNowInTimeArea(NO_DISTURB_START_TIME, NO_DISTURB_END_TIME);
    }

    /**
     * 改为你的存图片的服务器地址
     */
    public static final String IMAGE_BASE_URL = "";

    private static final String KEY_SERVER_ADDRESS_NORMAL = "KEY_SERVER_ADDRESS_NORMAL";
    private static final String KEY_SERVER_ADDRESS_TEST = "KEY_SERVER_ADDRESS_TEST";

    /**
     * 改为你的正式服务器地址
     */
    public static final String URL_SERVER_ADDRESS_NORMAL_HTTP = ""; // 正式服务器
    /**
     * 改为你的正式服务器地址
     */
    public static final String URL_SERVER_ADDRESS_NORMAL_HTTPS = ""; // 正式服务器
    /**
     * 改为你的测试服务器地址,如果有的话
     */
    public static final String URL_SERVER_ADDRESS_TEST = ""; // 测试服务器

    /**
     * 获取当前服务器地址
     * isHttps = false
     *
     * @return String
     */
    public static String getCurrentServerAddress() {
        return getCurrentServerAddress(false);
    }

    /**
     * 获取当前服务器地址
     *
     * @param isHttps isHttps
     * @return String
     */
    public static String getCurrentServerAddress(boolean isHttps) {
        return isHttps ? URL_SERVER_ADDRESS_NORMAL_HTTPS : getServerAddress(isOnTestMode);
    }

    /**
     * 获取服务器地址
     * isHttps = false
     *
     * @param isTest isTest
     * @return String
     */
    public static String getServerAddress(boolean isTest) {
        return getServerAddress(isTest, false);
    }

    /**
     * 获取服务器地址
     *
     * @param isTest isTest
     * @param isHttps isHttps
     * @return String
     */
    public static String getServerAddress(boolean isTest, boolean isHttps) {
        Preferences sdf = new DatabaseHelper(context).getPreferences(APP_SETTING);
        if (sdf == null) {
            return null;
        }
        if (isTest) {
            return sdf.getString(KEY_SERVER_ADDRESS_TEST, URL_SERVER_ADDRESS_TEST);
        }
        return sdf.getString(
                KEY_SERVER_ADDRESS_NORMAL, isHttps ? URL_SERVER_ADDRESS_NORMAL_HTTPS : URL_SERVER_ADDRESS_NORMAL_HTTP);
    }
}
