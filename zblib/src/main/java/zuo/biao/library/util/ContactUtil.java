/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
limitations under the License.*/

package zuo.biao.library.util;

/**联系人公用类
 * @author Lemon
 */
public class ContactUtil {
    /**
     * 称呼
     */
    public static final String NAME_NICK = "称呼";
    /**
     *姓名
     */
    public static final String NAME_NAME = "姓名";
    /**
     *生日
     */
    public static final String NAME_BIRTHDAY = "生日";
    /**
     *电话
     */
    public static final String NAME_PHONE = "电话";
    /**
     *邮箱
     */
    public static final String NAME_EMAIL = "邮箱";
    /**
     *网址
     */
    public static final String NAME_WEBSITE = "网址";
    /**
     *传真
     */
    public static final String NAME_FAX = "传真";
    /**
     *常用地址
     */
    public static final String NAME_USUALADDRESS = "常用地址";
    /**
     *收件地址
     */
    public static final String NAME_MAILADDRESS = "收件地址";
    /**
     *学校
     */
    public static final String NAME_SCHOOL = "学校";
    /**
     *公司
     */
    public static final String NAME_COMPANY = "公司";
    /**
     *职业
     */
    public static final String NAME_PROFESSION = "职业";
    /**
     *备注
     */
    public static final String NAME_NOTE = "备注";
    /**
     *其它
     */
    public static final String NAME_OTHER = "其它";
    /**
     *默认TYPE_NICK
     */
    public static final int TYPE_NICK = 0;
    /**
     *默认TYPE_NAME
     */
    public static final int TYPE_NAME = 1;
    /**
     *默认TYPE_BIRTHDAY
     */
    public static final int TYPE_BIRTHDAY = 2;
    /**
     *默认TYPE_PHONE
     */
    public static final int TYPE_PHONE = 3;
    /**
     *默认TYPE_WEBSITE
     */
    public static final int TYPE_WEBSITE = 4;
    /**
     *默认TYPE_EMAIL
     */
    public static final int TYPE_EMAIL = 5;
    /**
     *默认TYPE_FAX
     */
    public static final int TYPE_FAX = 6;
    /**
     *默认TYPE_USUALADDRESS
     */
    public static final int TYPE_USUALADDRESS = 7;
    /**
     * 默认TYPE_MAILADDRESS
     */
    public static final int TYPE_MAILADDRESS = 8;
    /**
     * 默认TYPE_SCHOOL
     */
    public static final int TYPE_SCHOOL = 9;
    /**
     * 默认TYPE_COMPANY
     */
    public static final int TYPE_COMPANY = 10;
    /**
     * 默认TYPE_PROFESSION
     */
    public static final int TYPE_PROFESSION = 11;
    /**
     * 默认TYPE_NOTE
     */
    public static final int TYPE_NOTE = 12;
}
