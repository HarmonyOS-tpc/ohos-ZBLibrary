/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
limitations under the License.*/

package zuo.biao.library.util;

import ohos.app.Context;

/**屏幕相关类
 * @author Lemon
 * @use ScreenUtil.xxxMethod(...);
 */
public class ScreenUtil {
    /**
     * 屏幕大小
     */
    public static int[] screenSize;

    /**
     * 获取屏幕size
     * @param context 上下文
     * @return getScreenSize
     */
    public static int[] getScreenSize(Context context){
        if (screenSize == null || screenSize[0] <= 480 || screenSize[1] <= 800) {//小于该分辨率会显示不全
            screenSize = new int[2];

            int dm = 0;
            dm =context.getResourceManager().getDeviceCapability().screenDensity;

            screenSize[0] = dm* context.getResourceManager().getDeviceCapability().width/ 160;// 屏幕宽（像素，如：480px）
            screenSize[1] = dm* context.getResourceManager().getDeviceCapability().height/ 160;// 屏幕高（像素，如：800px）
        }

        return screenSize;
    }
    /**
     * 获取屏幕宽
     * @param context c
     * @return int
     */
    public static int getScreenWidth(Context context) {
        return context.getResourceManager().getDeviceCapability().width
                * context.getResourceManager().getDeviceCapability().screenDensity
                / 160;
    }

    /**
     * 获取屏幕高
     * @param context c
     * @return int
     */
    public static int getScreenHeight(Context context) {
        return context.getResourceManager().getDeviceCapability().height
                * context.getResourceManager().getDeviceCapability().screenDensity
                / 160;
    }
}
