/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
limitations under the License.*/

package zuo.biao.library.util;

import ohos.agp.components.Component;
import ohos.agp.components.Text;

/**带清除按钮EditText或TextView套件，如果输入为空则隐藏清除按钮
 * @author Lemon
 * @use new TextClearSuit().addClearListener(...);
 */
public class TextClearSuit {
    private static final String TAG = "TextClearSuit";

    private Text tv;
    private Component clearView;

    private String inputedString;
    private int cursorPosition = 0;

    private static final int BLANK_TYPE_DEFAULT = 0;
    private static final int BLANK_TYPE_TRIM = 1;
    private static final int BLANK_TYPE_NO_BLANK = 2;
    /**默认trim，隐藏方式为gone
     * @param tv t
     * @param clearView c
     */
    public void addClearListener(final Text tv, final Component clearView) {
        addClearListener(tv, BLANK_TYPE_TRIM, clearView, false);
    }
    /**默认隐藏方式为gone
     * @param tv t
     * @param blankType blankType
     * @param clearView clearView
     */
    public void addClearListener(final Text tv, final int blankType, final Component clearView) {
        addClearListener(tv, blankType, clearView, false);
    }
    /**addClearListener
     * @param tv 输入框
     * @param blankType et内容前后是否不能含有空格
     * @param clearView 清除输入框内容按钮
     * @param isClearViewInvisible  如果et输入为空，隐藏clearView的方式为gone(false)还是invisible(true)
     */
    public void addClearListener(
            final Text tv, final int blankType, final Component clearView, final boolean isClearViewInvisible) {
        if (tv == null || clearView == null) {
            Log.error(TAG, "addClearListener  (tv == null || clearView == null)  >> return;");
            return;
        }

        this.tv = tv;
        this.clearView = clearView;
        if (tv.getText() != null) {
            inputedString = tv.getText().toString();
        }

        clearView.setVisibility(StringUtil.isNotEmpty(tv, false) ? Component.VISIBLE : Component.HIDE);
        clearView.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        tv.setText("");
                        tv.requestFocus();
                    }
                });
        tv.setComponentStateChangedListener(
                new Component.ComponentStateChangedListener() {
                    @Override
                    public void onComponentStateChanged(Component component, int i) {}
                });
        tv.addTextObserver(
                new Text.TextObserver() {
                    @Override
                    public void onTextUpdated(String s, int i, int i1, int i2) {
                        if (s == null || StringUtil.isNotEmpty(s.toString(), false) == false) {
                            inputedString = "";
                            if (isClearViewInvisible == false) {
                                clearView.setVisibility(Component.HIDE);
                            } else {
                                clearView.setVisibility(Component.INVISIBLE);
                            }
                        } else {
                            inputedString = "" + s.toString();
                            clearView.setVisibility(Component.VISIBLE);
                        }
                    }
                });
    }

    /**
     * txt监听
     */
    public interface OnTextChangedListener {
        /**
         * 监听回调
         * @param s s
         * @param start 开始
         * @param before 之前
         * @param count 数量
         */
        void onTextChanged(CharSequence s, int start, int before, int count);
    }
}
