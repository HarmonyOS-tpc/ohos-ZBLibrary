/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
limitations under the License.*/

package zuo.biao.library.util;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**测试用Log,不要使用android.util.Log
 * @modifier Lemon
 */
public class Log {
    /**
     * debug
     * @param TAG t
     * @param msg m
     */
    public static void debug(String TAG, String msg) {
        if (!SettingUtil.ISRELEASED) {
            HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0, TAG);
            HiLog.debug(label, msg);
        }
    }
    /**
     * debug
     * @param TAG t
     * @param msg m
     * @param tr t
     */
    public static void debug(String TAG, String msg, Throwable tr) {
        if (!SettingUtil.ISRELEASED) {
            HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0, TAG);
            HiLog.debug(label, msg);
        }
    }

    /**
     * fatal
     * @param TAG t
     * @param msg m
     */
    public static void v(String TAG, String msg) {
        if (!SettingUtil.ISRELEASED) {
            HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0, TAG);
            HiLog.fatal(label, msg);
        }
    }

    /**
     * info
     * @param TAG t
     * @param msg m
     */
    public static void info(String TAG, String msg) {
        if (!SettingUtil.ISRELEASED) {
            HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0, TAG);
            HiLog.info(label, msg);
        }
    }

    /**
     * error
     * @param TAG t
     * @param msg m
     */
    public static void error(String TAG, String msg) {
        if (!SettingUtil.ISRELEASED) {
            HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0, TAG);
            HiLog.error(label, msg);
        }
    }

    /**
     * warn
     * @param TAG t
     * @param msg m
     */
    public static void warn(String TAG, String msg) {
        if (!SettingUtil.ISRELEASED) {
            HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0, TAG);
            HiLog.warn(label, msg);
        }
    }
}
