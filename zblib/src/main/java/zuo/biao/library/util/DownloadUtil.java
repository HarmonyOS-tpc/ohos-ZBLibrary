package zuo.biao.library.util;

import ohos.aafwk.ability.Ability;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * 下载工具类
 *
 * @author Lemon
 */
public class DownloadUtil {
    private static final String TAG = "DownloadUtil";

    /**
     * 下载文件
     *
     * @param context 上下文
     * @param name    文件名
     * @param suffix  保存地址
     * @param httpUrl 下载地址
     * @return 文件
     */
    public static File downLoadFile(Ability context, String name, String suffix, String httpUrl) {
        final String fileName = name + StringUtil.getTrimedString(suffix);
        final File file = new File(DataKeeper.fileRootPath + fileName);
        try {
            httpUrl = StringUtil.getCorrectUrl(httpUrl);
            if (httpUrl.endsWith("/")) {
                httpUrl = httpUrl.substring(0, httpUrl.length() - 1);
            }
            URL url = new URL(httpUrl);
            InputStream stream = null;
            FileOutputStream fos = null;
            try {
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                stream = conn.getInputStream();
                fos = new FileOutputStream(file);
                byte[] buf = new byte[256];
                conn.connect();
                if (conn.getResponseCode() >= 400) {
                    CommonUtil.showShortToast(context, "连接超时");
                } else {
                    while (true) {
                        if (stream != null) {
                            int numRead = stream.read(buf);
                            if (numRead <= 0) {
                                break;
                            } else {
                                fos.write(buf, 0, numRead);
                            }

                        } else {
                            break;
                        }
                    }
                }

                conn.disconnect();
            } catch (IOException e) {
                Log.error(TAG, "downLoadFile   try { HttpURLConnection conn = (HttpURLConnection) url ... " + "} catch (IOException e) {" + e.getMessage());
            } finally {
                try {
                    if (fos != null) {
                        fos.close();
                    }

                } catch (IOException e) {
                    Log.error("IOException", e.getMessage());
                }
            }
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    Log.error("IOException", e.getMessage());
                }
            }
        } catch (MalformedURLException e) {
            Log.error(TAG, "downLoadFile   try {  URL url = new URL(httpUrl); ... " + "} catch (IOException e) {" + e.getMessage());
        }

        return file;
    }

    /**
     * 打开APK程序代码 暂时无方案实现
     *
     * @param context c
     * @param file    f
     */

    public static void openFile(Ability context, File file) {
        if (context == null) {
            Log.error(TAG, "openFile  context == null >> return;");
            return;
        }
    }
}
