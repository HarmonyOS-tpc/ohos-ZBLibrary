package zuo.biao.library.interfaces;

import ohos.agp.components.ComponentContainer;

/**
 * 封装的基类presenter
 *
 * @param <V> v
 */
public interface AdapterViewPresenter<V> {
    /**
     * 生成新的BV
     *
     * @param viewType v
     * @param parent   p
     * @return view
     */
    abstract V createView(int viewType, ComponentContainer parent);

    /**
     * 设置BV显示
     *
     * @param position p
     * @param bv       b
     * @return view
     */
    abstract void bindView(int position, V bv);
}
