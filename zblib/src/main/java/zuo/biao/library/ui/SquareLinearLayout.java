/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
limitations under the License.*/

package zuo.biao.library.ui;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.app.Context;

/**通用正方形视图
 * @author Lemon
 * @use 写在xml文件中即可
 */
public class SquareLinearLayout extends DirectionalLayout implements Component.EstimateSizeListener {
    public SquareLinearLayout(Context context) {
        super(context);
    }

    public SquareLinearLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public SquareLinearLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        setEstimateSizeListener(this);
    }

    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        int childWidthSize = getEstimatedWidth();
        // 高度和宽度一样
        heightEstimateConfig = widthEstimateConfig = MeasureSpec.getMeasureSpec(childWidthSize, MeasureSpec.PRECISE);
        return onEstimateSize(widthEstimateConfig, heightEstimateConfig);
    }
}
