/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

package zuo.biao.library.ui;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.app.Context;
import zuo.biao.library.ResourceTable;

import java.util.ArrayList;
import java.util.Arrays;


/**通用顶部弹出菜单
 * @author Lemon
 * @use
 * <br> toActivity或startActivityForResult (TopMenuWindow.createIntent(...), requestCode);
 * <br> 然后在onActivityResult方法内
 * <br> data.getIntExtra(TopMenuWindow.RESULT_POSITION); 可得到点击的 position
 * <br> 或
 * <br> data.getIntExtra(TopMenuWindow.RESULT_INTENT_CODE); 可得到点击的 intentCode
 */
public class TopMenuWindow extends Ability implements  Component.ClickedListener, ListContainer.ItemClickedListener {
	private static final String TAG = "TopMenuWindow";

	//启动方法<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

	/**启动TopMenuWindow的Intent
	 * @param context 上下文
	 * @param names names
	 * @return Intent
	 */
	public static Intent createIntent(Context context, String[] names) {
		return createIntent(context, names, new ArrayList<Integer>());
	}

	/**启动TopMenuWindow的Intent
	 * @param context 上下文
	 * @param nameList nameList
	 * @return Intent
	 */
	public static Intent createIntent(Context context, ArrayList<String> nameList) {
		return createIntent(context, nameList, null);
	}

	/**启动TopMenuWindow的Intent
	 * @param context 上下文
	 * @param names names
	 * @param intentCodes intentCodes
	 * @return Intent
	 */
	public static Intent createIntent(Context context, String[] names, int[] intentCodes) {
		Intent intent=new Intent();
		Operation operationBuilder =
				new Intent.OperationBuilder()
						.withAbilityName("zuo.biao.library.ui.TopMenuWindow" )
						.withDeviceId("")
						.withFlags(Intent.FLAG_ABILITY_NEW_MISSION)
						.withBundleName(context.getBundleName())
						.build();

		intent.setOperation(operationBuilder);
		intent.setParam(INTENT_NAMES, names);
		intent.setParam(INTENT_INTENTCODES, intentCodes);

		return intent;
	}

	/**启动TopMenuWindow的Intent
	 * @param context 上下文
	 * @param names names
	 * @param intentCodeList intentCodeList
	 * @return Intent
	 */
	public static Intent createIntent(Context context, String[] names, ArrayList<Integer> intentCodeList) {
		Intent intent=new Intent();
		Operation operationBuilder =
				new Intent.OperationBuilder()
						.withAbilityName("zuo.biao.library.ui.TopMenuWindow" )
						.withDeviceId("")
						.withFlags(Intent.FLAG_ABILITY_NEW_MISSION)
						.withBundleName(context.getBundleName())
						.build();

		intent.setOperation(operationBuilder);
		intent.setParam(INTENT_NAMES, names);
		intent.setParam(INTENT_INTENTCODES, intentCodeList);

		return intent;
	}

	/**启动TopMenuWindow的Intent
	 * @param context 上下文
	 * @param nameList nameList
	 * @param intentCodeList intentCodeList
	 * @return Intent
	 */
	public static Intent createIntent(Context context,
			ArrayList<String> nameList, ArrayList<Integer> intentCodeList) {
		Intent intent=new Intent();
		Operation operationBuilder =
				new Intent.OperationBuilder()
						.withAbilityName("zuo.biao.library.ui.TopMenuWindow" )
						.withDeviceId("")
						.withFlags(Intent.FLAG_ABILITY_NEW_MISSION)
						.withBundleName(context.getBundleName())
						.build();

		intent.setOperation(operationBuilder);
		intent.setParam(INTENT_NAMES, nameList);
		intent.setParam(INTENT_INTENTCODES, intentCodeList);
		return intent;
	}

	//启动方法>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


	private boolean isAlive;

	@Override
	protected void onStart(Intent intent) {
		super.onStart(intent);
		setUIContent(ResourceTable.Layout_top_menu_window);
		isAlive = true;

		init();
	}


	public static final String INTENT_NAMES = "INTENT_NAMES";
	public static final String INTENT_INTENTCODES = "INTENT_INTENTCODES";

	public static final String RESULT_NAME = "RESULT_NAME";
	public static final String RESULT_POSITION = "RESULT_POSITION";
	public static final String RESULT_INTENT_CODE = "RESULT_INTENT_CODE";

	private ArrayList<String> nameList = null;
	private ArrayList<Integer> intentCodeList = null;
	private ItemProvider adapter;
	private ListContainer lvTopMenu;
	private Component llTopMenuWindowBg;

	public void init() {

		llTopMenuWindowBg = findComponentById(ResourceTable.Id_llTopMenuWindowBg);
		llTopMenuWindowBg.setClickedListener(this);

		Intent intent = getIntent();

		int[] intentCodes = intent.getIntArrayParam(INTENT_INTENTCODES);
		if (intentCodes == null || intentCodes.length <= 0) {
			intentCodeList = intent.getIntegerArrayListParam(INTENT_INTENTCODES);
		} else {
			intentCodeList = new ArrayList<Integer>();
			for (int code : intentCodes) {
				intentCodeList.add(code);
			}
		}

		String[] menuItems = intent.getStringArrayParam(INTENT_NAMES);
		if (menuItems == null || menuItems.length <= 0) {
			nameList = intent.getStringArrayListParam(INTENT_NAMES);
		} else {
			nameList = new ArrayList<String>(Arrays.asList(menuItems));
		}

		if (nameList == null || nameList.size() <= 0) {
			terminateAbility();
			return;
		}

		adapter = new ItemProvider(this, nameList);

		lvTopMenu = (ListContainer) findComponentById(ResourceTable.Id_lvTopMenuWindowMenu);
		lvTopMenu.setItemProvider(adapter);
		lvTopMenu.setItemClickedListener(this);
	}




	@Override
	public void onBackPressed() {
		terminateAbility();
	}


	@Override
	protected void onStop() {
		isAlive = false;
		if (isAlive == false) {
			return;
		}

		llTopMenuWindowBg.setEnabled(false);
		super.onStop();
	}


	@Override
	public void onClick(Component component) {
		terminateAbility();
	}

	@Override
	public void onItemClicked(ListContainer listContainer, Component component, int position, long l) {
		Intent intent=new Intent();
		Operation operationBuilder =
				new Intent.OperationBuilder()
						.withAbilityName("zuo.biao.library.ui.TopMenuWindow" )
						.withDeviceId("")
						.withFlags(Intent.FLAG_ABILITY_NEW_MISSION)
						.withBundleName(getBundleName())
						.build();

		intent.setOperation(operationBuilder);
		intent.setParam(RESULT_POSITION, position);

		if (intentCodeList != null && intentCodeList.size() > position) {
			intent.setParam(RESULT_INTENT_CODE, intentCodeList.get(position));
		}

		setResult(-1, intent);
		terminateAbility();
	}
}
