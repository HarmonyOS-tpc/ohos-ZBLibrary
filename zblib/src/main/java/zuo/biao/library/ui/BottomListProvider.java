package zuo.biao.library.ui;

import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.app.Context;
import zuo.biao.library.ResourceTable;
import zuo.biao.library.model.BottomSheetItem;
import zuo.biao.library.util.ResTUtil;

import java.util.ArrayList;
import java.util.List;

/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * 底部弹框adapter
 */
public class BottomListProvider extends BaseItemProvider {
    private Context context;
    private List<BottomSheetItem> itemList;

    public BottomListProvider(Context context) {
        this.context = context;
        itemList = new ArrayList<>();
    }

    /**
     * 设置数据
     * @param itemData item
     */
    public void setItemData(List<BottomSheetItem> itemData) {
        this.itemList = itemData;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int pos) {
        return itemList.get(pos);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        BottomSheetItem item = itemList.get(position);

        component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_provider_list_item, null, false);

        Text text = (Text) component.findComponentById(ResourceTable.Id_txt_item);
        DirectionalLayout line = (DirectionalLayout) component.findComponentById(ResourceTable.Id_line);
        Image image = (Image) component.findComponentById(ResourceTable.Id_img_icon);
        DependentLayout dlListRoot = (DependentLayout) component.findComponentById(ResourceTable.Id_dl_list_root);
        text.setText(item.text);

        image.setVisibility(item.icon != null ? Component.VISIBLE : Component.HIDE);
        if (item.icon != null) {
            image.setImageElement(item.icon);
            text.setPaddingLeft(AttrHelper.vp2px(20, context));
        }

        if (position != itemList.size() - 1) {
            line.setVisibility(Component.VISIBLE);
            line.setBackground(ResTUtil.buildDrawableByColor(0xFF575757));
        }
        return component;
    }
}
