package zuo.biao.library.ui;

import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.PopupDialog;
import ohos.app.Context;
import zuo.biao.library.ResourceTable;

import java.util.ArrayList;

import static zuo.biao.library.base.BaseBottomWindow.INTENT_ITEMS;
import static zuo.biao.library.base.BaseBottomWindow.INTENT_ITEM_IDS;

/**
 * 底部弹框
 */
public class BottomMenuWindow {
    Context mContext;
    CommonDialog popupDialog;
    Text txtHeadTitle;
    Text textOne;
    Text textTow;
    Text txt_cancel;
    ItemClickListener itemClickListener;
    /**启动BottomMenuWindow的Intent
     * @param context 上下文
     * @param names name
     * @return Intent
     */
    public static Intent createIntent(Context context, String[] names,ItemClickListener itemClickListener) {
        return createIntent(context, itemClickListener);
    }

    /**启动BottomMenuWindow的Intent
     * @param context 上下文
     * @param nameList  nameList
     * @return Intent
     */
    public static Intent createIntent(Context context, ArrayList<String> nameList,ItemClickListener itemClickListener) {
        return createIntent(context, itemClickListener);
    }

    /**启动BottomMenuWindow的Intent
     * @param context 上下文
     * @param names names
     * @param ids ids
     * @return Intent
     */
    public static Intent createIntent(Context context, String[] names, int[] ids,ItemClickListener itemClickListener) {
        return createIntent(context,itemClickListener);
    }

    /**启动BottomMenuWindow的Intent
     * @param context 上下文
     * @param names names
     * @param idList idList
     * @return Intent
     */
    public static Intent createIntent(Context context, String[] names, ArrayList<Integer> idList,ItemClickListener itemClickListener) {
        return createIntent(context,itemClickListener);
    }

    /**启动BottomMenuWindow的Intent
     * @param context 上下 文
     * @param nameList nameList
     * @param idList idList
     * @return Intent
     */
    public static Intent createIntent(Context context,
                                      ArrayList<String> nameList, ArrayList<Integer> idList,ItemClickListener itemClickListener) {
       return createIntent(context,itemClickListener);
    }


    public static Intent createIntent(Context context,ItemClickListener itemClickListener) {
        new BottomMenuWindow(context)
                .setTitle("选择图片")
                .setClickListener(itemClickListener)
                .show();
        return new Intent();
    }
    /**
     * 设置监听
     * @param itemClickListener 监听
     * @return view
     */
    public BottomMenuWindow setClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
        return this;
    }

    public BottomMenuWindow(Context context) {
        this.mContext = context;
        initData();
    }

    private void initData() {
        Component  component =
                LayoutScatter.getInstance(mContext)
                        .parse(ResourceTable.Layout_ui_layout_bottom, new DirectionalLayout(mContext), true);

        txtHeadTitle = (Text) component.findComponentById(ResourceTable.Id_txt_head);
        txt_cancel = (Text) component.findComponentById(ResourceTable.Id_txt_cancel);

        textOne = (Text) component.findComponentById(ResourceTable.Id_txt_one);
        textOne.setText("拍照");

        textTow = (Text) component.findComponentById(ResourceTable.Id_txt_tow);
        textTow.setText("图库");

        popupDialog =
                new CommonDialog(
                        mContext);
        popupDialog.setAutoClosable(true);
        popupDialog.setDuration(10000);
        popupDialog.setAlignment(LayoutAlignment.BOTTOM);
        popupDialog.setContentCustomComponent(component);
        popupDialog.setSize(ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_CONTENT);
        popupDialog.setAutoClosable(false);

        textOne.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        itemClickListener.click(0);
                        popupDialog.hide();
                        popupDialog.destroy();
                    }
                });

        textTow.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        itemClickListener.click(1);
                        popupDialog.hide();
                        popupDialog.destroy();
                    }
                });

        txt_cancel.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        popupDialog.hide();
                        popupDialog.destroy();
                    }
                });
    }

    /**
     * 设置title
     * @param msg msg
     * @return view
     */
    public BottomMenuWindow setTitle(String msg) {
        txtHeadTitle.setText(msg);
        return this;
    }

    /**
     * show
     * @return view
     */
    public BottomMenuWindow show() {
        popupDialog.show();
        return this;
    }

    /**
     * 设置item监听
     */
    public interface ItemClickListener {
        /**
         * 点击回调
         * @param position p
         */
        void click(int position);
    }
}
