/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
limitations under the License.*/

package zuo.biao.library.ui;

import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.ProgressBar;
import ohos.agp.components.webengine.ResourceError;
import ohos.agp.components.webengine.ResourceRequest;
import ohos.agp.components.webengine.WebAgent;
import ohos.agp.components.webengine.WebView;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import zuo.biao.library.ResourceTable;
import zuo.biao.library.base.BaseAbility;
import zuo.biao.library.interfaces.OnBottomDragListener;
import zuo.biao.library.util.Log;
import zuo.biao.library.util.StringUtil;

/**通用网页Activity
 * @author Lemon
 * @use toActivity(WebViewActivity.createIntent(...));
 */
public class WebViewActivity extends BaseAbility implements OnBottomDragListener, Component.ClickedListener {

    private static final String TAG = "WebViewActivity";

    //启动方法<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    private static final String INTENT_RETURN = "INTENT_RETURN";
    private static final String INTENT_URL = "INTENT_URL";

    /**获取启动这个Activity的Intent
     * @param title 标题
     * @param url url
     */
    public static Intent createIntent(Context context, String title, String url) {
        Intent intent=new Intent();
        Operation operationBuilder =
                new Intent.OperationBuilder()
                        .withAbilityName("zuo.biao.library.ui.WebViewActivity" )
                        .withDeviceId("")
                        .withFlags(Intent.FLAG_ABILITY_NEW_MISSION)
                        .withBundleName(context.getBundleName())
                        .build();

        intent.setOperation(operationBuilder);
        intent.setParam(INTENT_TYPE, title);
        intent.setParam(INTENT_URL, url);
        return intent;
    }

    //启动方法<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


    private String url;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_web_view_activity);

        url = StringUtil.getCorrectUrl(getIntent().getStringParam(INTENT_URL));
        if (StringUtil.isNotEmpty(url, true) == false) {
            terminateAbility();
            return;
        }

        //功能归类分区方法，必须调用<<<<<<<<<<
        initView();
        initData();
        initEvent();
        //功能归类分区方法，必须调用>>>>>>>>>>

    }



    //UI显示区(操作UI，但不存在数据获取或处理代码，也不存在事件监听代码)<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    private ProgressBar pbWebView;
    private WebView wvWebView;
    @Override
    public void initView() {
        autoSetTitle();

        pbWebView = findView(ResourceTable.Id_pbWebView);
        wvWebView = findView(ResourceTable.Id_wvWebView);
    }



    //UI显示区(操作UI，但不存在数据获取或处理代码，也不存在事件监听代码)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>










    //Data数据区(存在数据获取或处理代码，但不存在事件监听代码)<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


    @Override
    public void initData() {

        wvWebView.getWebConfig().setJavaScriptPermit(true);
        wvWebView.load(url);

        wvWebView.setWebAgent(new WebAgent(){
            @Override
            public void onLoadingPage(WebView webView, String url, PixelMap icon) {
                super.onLoadingPage(webView, url, icon);
                pbWebView.setVisibility(Component.VISIBLE);
            }

            @Override
            public void onPageLoaded(WebView webView, String url) {
                super.onPageLoaded(webView, url);
                pbWebView.setVisibility(Component.HIDE);
            }

            @Override
            public void onLoadingContent(WebView webView, String url) {
                super.onLoadingContent(webView, url);
            }

            @Override
            public void onError(WebView webView, ResourceRequest request, ResourceError error) {
                super.onError(webView, request, error);
                pbWebView.setVisibility(Component.HIDE);

            }
        });
    }



    //Data数据区(存在数据获取或处理代码，但不存在事件监听代码)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>








    //Event事件区(只要存在事件监听代码就是)<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    @Override
    public void initEvent() {

        tvBaseTitle.setClickedListener(this);
    }

    @Override
    public void onDragBottom(boolean rightToLeft) {
        if (rightToLeft) {
            return;
        }
        onBackPressed();
    }



    //系统自带监听方法<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    //类相关监听<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


    @Override
    protected void onActive() {
        super.onActive();
        if (wvWebView != null) {
            wvWebView.onActive();
        }
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        if (wvWebView != null) {
            wvWebView.onInactive();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (wvWebView != null) {
            wvWebView.onStop();
            wvWebView = null;
        }
    }


    protected static final int REQUEST_TO_EDIT_TEXT_WINDOW = 1;

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent data) {
        super.onAbilityResult(requestCode, resultCode, data);
        if (resultCode != -1) {
            return;
        }
        switch (requestCode) {
            case REQUEST_TO_EDIT_TEXT_WINDOW:
                if (data != null) {
                    wvWebView.load(StringUtil.getCorrectUrl(data.getStringParam(EditTextInfoActivity.RESULT_VALUE)));
                }
                break;
        }
    }


    @Override
    public void onClick(Component component) {
        toActivity(EditTextInfoActivity.createIntent(context
                , EditTextInfoWindow.TYPE_WEBSITE
                , StringUtil.getTrimedString(tvBaseTitle)
                ,url),
                REQUEST_TO_EDIT_TEXT_WINDOW, false);
    }

    //类相关监听>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    //系统自带监听方法<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


    //Event事件区(只要存在事件监听代码就是)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


}
