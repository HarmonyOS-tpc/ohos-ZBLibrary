/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

package zuo.biao.library.ui;

import java.util.List;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.OnClickListener;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import zuo.biao.library.ResourceTable;
import zuo.biao.library.base.BaseView;
import zuo.biao.library.util.Log;
import zuo.biao.library.util.ResTUtil;
import zuo.biao.library.util.StringUtil;

/**自定义顶栏切换标签View
 * @warn 复制到其它工程内使用时务必修改import R文件路径为所在应用包名
 * @author Lemon
 * @use
 * <br> TopTabView modleView = new TopTabView(context, inflater);
 * <br> adapter中使用:[具体见.BaseTabActivity]
 * <br> convertView = modleView.getView();
 * <br> 或  其它类中使用
 * <br> containerView.addView(modleView.getConvertView());
 * <br> 然后
 * <br> modleView.bindView(object);
 * <br> modleView.setOnTabSelectedListener(onItemSelectedListener);
 */
public class TopTabView extends BaseView<String[]> {
	private static final String TAG = "TopTabView";

	/**
	 * tab OnTabSelectedListener
	 */
	public interface OnTabSelectedListener {
		void onTabSelected(Text tvTab, int position, int id);
	}

	private OnTabSelectedListener onTabSelectedListener;

	/**
	 * setOnTabSelectedListener
	 * @param onTabSelectedListener 选中监听
	 */
	public void setOnTabSelectedListener(OnTabSelectedListener onTabSelectedListener) {
		this.onTabSelectedListener = onTabSelectedListener;
	}

	private LayoutScatter inflater;
	public TopTabView(Ability context) {
		super(context, ResourceTable.Layout_top_tab_view);
		this.inflater = LayoutScatter.getInstance(context);
	}
	private int minWidth;
	public TopTabView(Ability context, int minWidth) {
		this(context);
		this.minWidth = minWidth;
	}

	public TopTabView(Ability context, int minWidth,  int resource){
		super(context, resource);
		this.minWidth = minWidth;
		this.inflater = LayoutScatter.getInstance(context);
	}


	private int currentPosition = 0;

	/**
	 * setCurrentPosition
	 * @param currentPosition 当前位置
	 */
	public void setCurrentPosition(int currentPosition) {
		this.currentPosition = currentPosition;
	}


	private Text tvTopTabViewTabFirst;
	private Text tvTopTabViewTabLast;

	private DirectionalLayout llTopTabViewContainer;
	@Override
	public Component createView() {
		tvTopTabViewTabFirst = findView(ResourceTable.Id_tvTopTabViewTabFirst);
		tvTopTabViewTabLast = findView(ResourceTable.Id_tvTopTabViewTabLast);

		llTopTabViewContainer = findView(ResourceTable.Id_llTopTabViewContainer);

		return super.createView();
	}


	private String[] names;//传进来的数据

	/**
	 * 长度
	 * @return int
	 */
	public int getCount() {
		return names.length;
	}

	/**
	 * 当前位置
	 * @return int
	 */
	public int getCurrentPosition() {
		return currentPosition;
	}

	/**
	 * getCurrentTab
	 * @return Text
	 */
	public Text getCurrentTab() {
		return tvTabs[getCurrentPosition()];
	}

	private int lastPosition = 1;
	/**
	 * bindView
	 * @param nameList
	 */
	public void bindView(List<String> nameList){
		if (nameList != null) {
			for (int i = 0; i < nameList.size(); i++) {
				names[i] = nameList.get(i);
			}
		}
		bindView(names);
	}
	private int width;
	private int maxWidth;
	@Override
	public void bindView(String[] names){
		if (names == null || names.length < 2) {
			return;
		}
		super.bindView(names);
		this.names = names;
		this.lastPosition = getCount() - 1;

		tvTabs = new Text[getCount()];

		tvTabs[0] = tvTopTabViewTabFirst;
		tvTabs[lastPosition] = tvTopTabViewTabLast;

		llTopTabViewContainer.removeAllComponents();
		for (int i = 0; i < tvTabs.length; i++) {
			final int position = i;

			if (tvTabs[position] == null) {
				//viewgroup.addView(child)中的child相同，否则会崩溃
				tvTabs[position] = (Text) inflater.parse(ResourceTable.Layout_top_tab_tv_center, llTopTabViewContainer, false);
				llTopTabViewContainer.addComponent(tvTabs[position]);

				Component divider = inflater.parse(ResourceTable.Layout_divider_vertical_1dp, llTopTabViewContainer, false);
				divider.setBackground(ResTUtil.getElement(context,ResourceTable.Color_white));
				llTopTabViewContainer.addComponent(divider);
			}
			tvTabs[position].setText(StringUtil.getTrimedString(names[position]));
			tvTabs[position].setClickedListener(new Component.ClickedListener() {

				@Override
				public void onClick(Component component) {
					select(position);
				}

			});

			width = tvTabs[position].getWidth();
			if (minWidth < width) {
				minWidth = width;
			}
		}

		//防止超出
		maxWidth = llTopTabViewContainer.getEstimatedWidth() / tvTabs.length;
		if (minWidth > maxWidth) {
			minWidth = maxWidth;
		}
		for (int i = 0; i < tvTabs.length; i++) {
			//保持一致
			tvTabs[i].setMinWidth(minWidth);

			//防止超出
			if (tvTabs[i].getWidth() > maxWidth) {
				tvTabs[i].setWidth(maxWidth);
			}
		}


		select(currentPosition);
	}

	private Text[] tvTabs;
	/**选择tab
	 * @param position
	 */
	public void select(int position) {
		if (position < 0 || position >= getCount()) {
			return;
		}

		for (int i = 0; i < tvTabs.length; i++) {
			tvTabs[i].setSelected(i == position);
		}

		if (onTabSelectedListener != null) {
			onTabSelectedListener.onTabSelected(tvTabs[position], position, tvTabs[position].getId());
		}

		this.currentPosition = position;
	}

}
