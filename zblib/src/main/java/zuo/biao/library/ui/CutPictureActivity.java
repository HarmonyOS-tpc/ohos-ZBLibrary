/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
limitations under the License.*/

package zuo.biao.library.ui;

import ohos.aafwk.content.Intent;
import ohos.app.Context;
import ohos.utils.net.Uri;
import zuo.biao.library.base.BaseAbility;
import zuo.biao.library.util.Log;
import zuo.biao.library.util.StringUtil;

import java.io.File;

/**通用获取裁剪单张照片Activity
 * <br> toActivity或startActivityForResult (CutPictureActivity.createIntent(...), requestCode);
 * <br> 然后在onActivityResult方法内
 * <br> data.getStringExtra(CutPictureActivity.RESULT_PICTURE_PATH); 可得到图片存储路径
 */

public class CutPictureActivity extends BaseAbility {
    private static final String TAG = "CutPictureActivity";

    // 启动方法<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    private static final String INTENT_ORIGINAL_PICTURE_PATH = "INTENT_ORIGINAL_PICTURE_PATH";
    private static final String INTENT_CUTTED_PICTURE_PATH = "INTENT_CUTTED_PICTURE_PATH";
    private static final String INTENT_CUTTED_PICTURE_NAME = "INTENT_CUTTED_PICTURE_NAME";

    private static final String INTENT_CUT_WIDTH = "INTENT_CUT_WIDTH";
    private static final String INTENT_CUT_HEIGHT = "INTENT_CUT_HEIGHT";

    /**启动这个Activity的Intent
     * @param context context
     * @param originalPath 原始路径
     * @param cuttedPath cu
     * @param cuttedName c
     * @param cuttedSize c
     * @return intent
     */
    public static Intent createIntent(
            Context context, String originalPath, String cuttedPath, String cuttedName, int cuttedSize) {
        return createIntent(context, originalPath, cuttedPath, cuttedName, cuttedSize, cuttedSize);
    }
    /**启动这个Activity的Intent
     * @param context context
     * @param originalPath originalPath
     * @param cuttedPath cuttedPath
     * @param cuttedName cuttedName
     * @param cuttedWidth cuttedWidth
     * @param cuttedHeight cuttedHeight
     * @return intent
     */
    public static Intent createIntent(
            Context context,
            String originalPath,
            String cuttedPath,
            String cuttedName,
            int cuttedWidth,
            int cuttedHeight) {
        Intent intent = new Intent();
        intent.setParam(INTENT_ORIGINAL_PICTURE_PATH, originalPath);
        intent.setParam(INTENT_CUTTED_PICTURE_PATH, cuttedPath);
        intent.setParam(INTENT_CUTTED_PICTURE_NAME, cuttedName);
        intent.setParam(INTENT_CUT_WIDTH, cuttedWidth);
        intent.setParam(INTENT_CUT_HEIGHT, cuttedHeight);
        return intent;
    }

    // 启动方法>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    private String originalPicturePath;
    private String cuttedPicturePath;
    private String cuttedPictureName;
    private int cuttedWidth;
    private int cuttedHeight;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);

        intent = getIntent();

        originalPicturePath = intent.getStringParam(INTENT_ORIGINAL_PICTURE_PATH);
        cuttedWidth = intent.getIntParam(INTENT_CUT_WIDTH, 0);
        cuttedHeight = intent.getIntParam(INTENT_CUT_HEIGHT, 0);
        if (cuttedWidth <= 0) {
            cuttedWidth = cuttedHeight;
        }
        if (cuttedHeight <= 0) {
            cuttedHeight = cuttedWidth;
        }

        if (StringUtil.isNotEmpty(originalPicturePath, true) == false || cuttedWidth <= 0) {
            Log.error(
                    TAG,
                    "onCreate  StringUtil.isNotEmpty(originalPicturePath, true)"
                            + " == false || cuttedWidth <= 0 >> finish(); return;");
            showShortToast("图片不存在，请先选择图片");
            terminateAbility();
            return;
        }

        // 功能归类分区方法，必须调用<<<<<<<<<<
        initData();
        initView();
        initEvent();
        // 功能归类分区方法，必须调用>>>>>>>>>>
    }

    // UI显示区(操作UI，但不存在数据获取或处理代码，也不存在事件监听代码)<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    @Override
    public void initView() { // 必须调用
    }

    /**照片裁剪
     * @param path path
     * @param width width
     * @param height height
     */
    public void startPhotoZoom(String path, int width, int height) {
        startPhotoZoom(Uri.getUriFromFile(new File(path)), width, height);
    }
    /**照片裁剪
     * @param fileUri fileUri
     * @param width width
     * @param height height
     */
    public void startPhotoZoom(Uri fileUri, int width, int height) {}

    // UI显示区(操作UI，但不存在数据获取或处理代码，也不存在事件监听代码)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // Data数据区(存在数据获取或处理代码，但不存在事件监听代码)<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    @Override
    public void initData() { // 必须调用
        startPhotoZoom(originalPicturePath, cuttedWidth, cuttedHeight);
    }

    // Data数据区(存在数据获取或处理代码，但不存在事件监听代码)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // Event事件区(只要存在事件监听代码就是)<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    @Override
    public void initEvent() { // 必须调用
    }

    // 系统自带监听方法<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    // 类相关监听<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    // 类相关监听>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // 系统自带监听方法>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // Event事件区(只要存在事件监听代码就是)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // 内部类,尽量少用<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    // 内部类,尽量少用>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

}
