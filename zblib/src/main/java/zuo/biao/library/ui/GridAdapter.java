package zuo.biao.library.ui;

import static ohos.agp.utils.LayoutAlignment.TOP;

import ohos.agp.components.*;
import ohos.app.Context;
import zuo.biao.library.model.Entry;
import zuo.biao.library.util.Log;

import java.util.HashMap;
import java.util.List;

/**
 * grid 适配器
 * @param <T> T
 */
public abstract class GridAdapter<T> extends RecycleItemProvider {
    private Context context;
    private List<T> data;
    private int mLayoutId;
    private int numColumns = 1;
    private OnItemClickListener onItemClickListener;

    public GridAdapter(Context context){
        this.context = context;
    }

    public GridAdapter(Context context, int mLayoutId){
        this.context = context;
        this.mLayoutId = mLayoutId;
    }
    /**
     * 初始化
     * @param context   上下文
     * @param data      数据源
     * @param mLayoutId 条目的资源文件id
     * @return GridAdapter
     */
    public GridAdapter(Context context, int mLayoutId, List<T> data) {
        this.context = context;
        this.data = data;
        this.mLayoutId = mLayoutId;
    }

    /**
     * 设置列数
     * @param numColumns s
     */
    public void setNumColumns(int numColumns) {
        this.numColumns = numColumns;
    }

    /**
     * 设置数据
     * @param data d
     */
    public void setData(List<T> data) {
        this.data = data;
        notifyDataChanged();
    }

    /**
     * 设置数据
     */
    public void setLayoutRes(int layoutRes) {
        this.mLayoutId = layoutRes;
    }

    private boolean hasName;
    public GridAdapter setHasName(boolean hasName) {
        this.hasName = hasName;
        return this;
    }
    private boolean hasCheck = false;//是否使用标记功能
    public GridAdapter setHasCheck(boolean hasCheck) {
        this.hasCheck = hasCheck;
        return this;
    }

    //item标记功能，不需要可以删除<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    private HashMap<Integer, Boolean> hashMap;//实现选中标记的列表，不需要可以删除。这里可用List<Integer> checkedList代替
    public boolean getItemChecked(int position) {
        if (hasCheck == false) {
            return false;
        }
        return hashMap.get(position);
    }
    public void setItemChecked(int position, boolean isChecked) {
        if (hasCheck == false) {
            return;
        }
        hashMap.put(position, isChecked);
        notifyDataChanged();
    }

    private List<Entry<String, String>> list;
    /**刷新列表
     * @param list
     */
    public synchronized void refresh(List<Entry<String, String>> list) {
        this.list = list;
        if (hasCheck) {
            hashMap = new HashMap<Integer, Boolean>();
            if (list != null) {
                for (int i = 0; i < list.size(); i++) {
                    hashMap.put(i, false);
                }
            }
        }
        notifyDataChanged();
    }

    /**
     * 获取数据
     * @return data
     */
    public List<T> getData() {
        return data;
    }

    @Override
    public int getCount() {
        if (data != null) {
            return data.size() % numColumns == 0 ? data.size() / numColumns : data.size() / numColumns + 1;
        } else {
            return 0;
        }
    }

    @Override
    public T getItem(int position) {
        if (data != null) {
            return data.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer parent) {
        ViewHolder viewHolder;
        convertComponent = new DirectionalLayout(context);
        ((DirectionalLayout) convertComponent).setOrientation(Component.HORIZONTAL);
        ComponentContainer.LayoutConfig layoutConfig = convertComponent.getLayoutConfig();
        layoutConfig.width = DependentLayout.LayoutConfig.MATCH_PARENT;
        layoutConfig.height = DependentLayout.LayoutConfig.MATCH_CONTENT;
        convertComponent.setLayoutConfig(layoutConfig);
        for (int i = 0; i < numColumns; i++) {
            if (position * numColumns + i < data.size()) {
                DirectionalLayout dlItemParent = new DirectionalLayout(context);
                dlItemParent.setLayoutConfig(
                        new DirectionalLayout.LayoutConfig(0, DirectionalLayout.LayoutConfig.MATCH_CONTENT, TOP, 1));
                Component childConvertComponent = LayoutScatter.getInstance(context).parse(mLayoutId, null, false);
                int finalI = i;
                childConvertComponent.setClickedListener(
                        new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                if (onItemClickListener != null) {
                                    onItemClickListener.onItemClick(component, position * numColumns + finalI);
                                }
                            }
                        });
                dlItemParent.addComponent(childConvertComponent);
                ((ComponentContainer) convertComponent).addComponent(dlItemParent);
                viewHolder = new ViewHolder(childConvertComponent);
                bind(viewHolder, getItem(position * numColumns + i), position * numColumns + i);
            } else {
                // 用Component占位会导致高度为MATCH_PARENT,所以此处用DirectionalLayout占位
                DirectionalLayout childConvertComponent = new DirectionalLayout(context);
                childConvertComponent.setLayoutConfig(
                        new DirectionalLayout.LayoutConfig(0, DirectionalLayout.LayoutConfig.MATCH_CONTENT, TOP, 1));
                ((ComponentContainer) convertComponent).addComponent(childConvertComponent);
            }
        }
        return convertComponent;
    }

    /**
     * 设置item监听
     * @param onItemClickListener onItemClickListener
     */
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    /**
     * view绑定数据
     * @param holder h
     * @param s s
     * @param position p
     */
    protected abstract void bind(ViewHolder holder, T s, int position);

    /**
     * 自定义viewhoder
     */
    protected static class ViewHolder {
        HashMap<Integer, Component> mViews = new HashMap<>();
        /**
         * 根view
         */
        public Component itemView;

        ViewHolder(Component component) {
            this.itemView = component;
        }

        /**
         * 设置text
         * @param viewId i
         * @param text t
         * @return v
         */
        public ViewHolder setText(int viewId, String text) {
            ((Text) getView(viewId)).setText(text);
            return this;
        }

        /**
         * 获取view
         * @param viewId id
         * @param <E> e
         * @return view
         */
        public <E extends Component> E getView(int viewId) {
            E view = (E) mViews.get(viewId);
            if (view == null) {
                view = (E) itemView.findComponentById(viewId);
                mViews.put(viewId, view);
            }
            return view;
        }
    }

    /**
     * 点击监听
     */
    public abstract static class OnItemClickListener {
        /**
         * 点击监听
         * @param component c
         * @param position p
         */
        public abstract void onItemClick(Component component, int position);
    }
}
