/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

package zuo.biao.library.ui;

import ohos.agp.components.*;
import ohos.app.Context;
import zuo.biao.library.ResourceTable;
import zuo.biao.library.util.ScreenUtil;
import zuo.biao.library.util.StringUtil;

/**通用对话框类
 * @author Lemon
 * @use new AlertDialog(...).show();
 */
public class AlertDialog  extends ohos.agp.window.dialog.CommonDialog implements Component.ClickedListener {

	/**
	 * 自定义Dialog监听器 
	 */  
	public interface OnDialogButtonClickListener {  

		/**点击按钮事件的回调方法
		 * @param requestCode 传入的用于区分某种情况下的showDialog
		 * @param isPositive 确定
		 */
		void onDialogButtonClick(int requestCode, boolean isPositive);  
	}


	@SuppressWarnings("unused")
	private Context context;
	private String title; 
	private String message; 
	private String strPositive;
	private String strNegative;
	private boolean showNegativeButton = true;
	private int requestCode;
	private OnDialogButtonClickListener listener;

	/** 
	 * 带监听器参数的构造函数 
	 */  
	public AlertDialog(Context context, String title, String message, boolean showNegativeButton,
			int requestCode, OnDialogButtonClickListener listener) {
		super(context);

		this.context = context;
		this.title = title;
		this.message = message;
		this.showNegativeButton = showNegativeButton;
		this.requestCode = requestCode;
		this.listener = listener;  
	}
	public AlertDialog(Context context, String title, String message, boolean showNegativeButton,
			String strPositive, int requestCode, OnDialogButtonClickListener listener) {
		super(context);

		this.context = context;
		this.title = title;
		this.message = message;
		this.showNegativeButton = showNegativeButton;
		this.strPositive = strPositive;
		this.requestCode = requestCode;
		this.listener = listener;  
	}
	public AlertDialog(Context context, String title, String message, 
			String strPositive, String strNegative, int requestCode, OnDialogButtonClickListener listener) {
		super(context);

		this.context = context;
		this.title = title;
		this.message = message;
		this.strPositive = strPositive;
		this.strNegative = strNegative;
		this.requestCode = requestCode;
		this.listener = listener;  
	}

	private Text tvTitle;
	private Text tvMessage;
	private Button btnPositive;
	private Button btnNegative;

	@Override
	protected void onCreate() {
		super.onCreate();
		ComponentContainer rootLayout =
				(ComponentContainer)
						LayoutScatter.getInstance(context).parse(ResourceTable.Layout_alert_layout, null, false);

		setSize(ScreenUtil.getScreenWidth(context) * 4 / 5, ComponentContainer.LayoutConfig.MATCH_CONTENT);


		tvTitle = (Text) rootLayout.findComponentById(ResourceTable.Id_tvAlertDialogTitle);
		tvMessage = (Text) rootLayout.findComponentById(ResourceTable.Id_tvAlertDialogMessage);
		btnPositive = (Button) rootLayout.findComponentById(ResourceTable.Id_btnAlertDialogPositive);
		btnNegative = (Button) rootLayout.findComponentById(ResourceTable.Id_btnAlertDialogNegative);

		tvTitle.setVisibility(StringUtil.isNotEmpty(title, true) ? Component.VISIBLE : Component.HIDE);
		tvTitle.setText("" + StringUtil.getCurrentString());

		if (StringUtil.isNotEmpty(strPositive, true)) {
			btnPositive.setText(StringUtil.getCurrentString());
		}
		btnPositive.setClickedListener(this);

		if (showNegativeButton) {
			if (StringUtil.isNotEmpty(strNegative, true)) {
				btnNegative.setText(StringUtil.getCurrentString());
			}
			btnNegative.setClickedListener(this);
		} else {
			btnNegative.setVisibility(Component.HIDE);
		}

		tvMessage.setText(StringUtil.getTrimedString(message));
		setContentCustomComponent(rootLayout);
	}


	@Override
	public void onClick(Component v) {
		if (v.getId() == ResourceTable.Id_btnAlertDialogPositive) {
			listener.onDialogButtonClick(requestCode, true);
		} else if (v.getId() == ResourceTable.Id_btnAlertDialogNegative) {
			listener.onDialogButtonClick(requestCode, false);
		}

		hide();
		destroy();
	}

}

