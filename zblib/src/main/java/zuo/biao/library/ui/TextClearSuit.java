/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

package zuo.biao.library.ui;

import ohos.aafwk.ability.OnClickListener;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import zuo.biao.library.util.Log;
import zuo.biao.library.util.StringUtil;

/**带清除按钮EditText或TextView套件，如果输入为空则隐藏清除按钮
 * @author Lemon
 * @use new TextClearSuit().addClearListener(...);
 */
public class TextClearSuit {
	private static final String TAG = "TextClearSuit";

	private Text tv;
	private Component clearView;
	
	private String inputedString;
	private int cursorPosition = 0;

	/**
	 * getTextView
	 * @return Text
	 */
	public Text getTextView() {
		return tv;
	}

	/**
	 * getClearView
	 * @return Component
	 */
	public Component getClearView() {
		return clearView;
	}

	/**
	 * getInputedString
	 * @return String
	 */
	public String getInputedString() {
		return inputedString;
	}

	/**
	 * getCursorPosition
	 * @return int
	 */
	public int getCursorPosition() {
		return cursorPosition;
	}


	/**
	 * BLANK_TYPE_DEFAULT
	 */
	public static final int BLANK_TYPE_DEFAULT = 0;
	/**
	 * BLANK_TYPE_TRIM
	 */
	public static final int BLANK_TYPE_TRIM = 1;
	/**
	 * BLANK_TYPE_NO_BLANK
	 */
	public static final int BLANK_TYPE_NO_BLANK = 2;
	/**默认trim，隐藏方式为gone
	 * @param tv Text
	 * @param clearView Component
	 */
	public void addClearListener(final Text tv, final Component clearView) {
		addClearListener(tv, BLANK_TYPE_TRIM, clearView, false);
	}
	/**默认隐藏方式为gone
	 * @param tv Text
	 * @param clearView Component
	 */
	public void addClearListener(final Text tv, final int blankType, final Component clearView) {
		addClearListener(tv, blankType, clearView, false);
	}
	/**
	 * @param tv 输入框
	 * @param clearView 清除输入框内容按钮
	 * @param isClearViewInvisible  如果et输入为空，隐藏clearView的方式为gone(false)还是invisible(true)
	 */
	public void addClearListener(final Text tv, final int blankType, final Component clearView, final boolean isClearViewInvisible) {
		if (tv == null || clearView == null) {
			return;
		}

		this.tv = tv;
		this.clearView = clearView;
		if (tv.getText() != null) {
			inputedString = tv.getText().toString();
		}

		clearView.setVisibility(StringUtil.isNotEmpty(tv, false) ? Component.VISIBLE : Component.HIDE);
		clearView.setClickedListener(new Component.ClickedListener() {
			@Override
			public void onClick(Component component) {
				tv.setText("");
				tv.requestFocus();
			}
		});
		tv.addTextObserver(new Text.TextObserver() {
			@Override
			public void onTextUpdated(String s, int i, int i1, int i2) {
				if (s == null || StringUtil.isNotEmpty(s.toString(), false) == false) {
					inputedString = "";
					if (isClearViewInvisible == false) {
						clearView.setVisibility(Component.HIDE);
					} else {
						clearView.setVisibility(Component.INVISIBLE);
					}
				} else {
					inputedString = "" + s.toString();
					clearView.setVisibility(Component.VISIBLE);
				}
			}
		});

	}

	/**
	 * 输入监听
	 */
	public interface onTextChangedListener {
		/**
		 * 监听
		 * @param s CharSequence
		 * @param start start
		 * @param before before
		 * @param count count
		 */
		void onTextChanged(CharSequence s, int start, int before, int count);
	}

}
