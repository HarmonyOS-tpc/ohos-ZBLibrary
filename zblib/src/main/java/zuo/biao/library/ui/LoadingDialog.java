package zuo.biao.library.ui;
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import ohos.agp.components.*;
import ohos.app.Context;
import zuo.biao.library.ResourceTable;

/**
 * 加载框弹框
 */
public class LoadingDialog extends ohos.agp.window.dialog.CommonDialog {
    Context mContext;
    Text txtLoading;
    RoundProgressBar roundProgressBar;
    DirectionalLayout load_root;
    boolean isClickClose;

    public LoadingDialog(Context context) {
        super(context);
        this.mContext = context;
        initView();
    }

    private void initView() {
        Component component = null;
        component =
                LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_ui_layout_loading_dialog, null, false);
        txtLoading = (Text) component.findComponentById(ResourceTable.Id_txt_loading);
        roundProgressBar = (RoundProgressBar) component.findComponentById(ResourceTable.Id_rProgress);
        load_root = (DirectionalLayout) component.findComponentById(ResourceTable.Id_load_root);

        setSize(AttrHelper.vp2px(60, mContext), AttrHelper.vp2px(60, mContext));
        setCornerRadius(10);
        setContentCustomComponent(component);

        load_root.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        if (isClickClose) {
                            hide();
                            destroy();
                        }
                    }
                });
    }

    @Override
    public void hide() {
        super.hide();
        roundProgressBar.release();
    }

    /**
     * 设置是否点击关闭
     * @param is boolean
     * @return b
     */
    public LoadingDialog setClickClose(boolean is) {
        isClickClose = is;
        return this;
    }

    /**
     * 设置加载内容
     * @param text String
     * @return v
     */
    public LoadingDialog setLoadText(String text) {
        txtLoading.setText(text);
        return this;
    }
}
