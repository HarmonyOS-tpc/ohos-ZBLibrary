package zuo.biao.library.ui;

import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.PopupDialog;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import zuo.biao.library.ResourceTable;
import zuo.biao.library.model.PlaceBean;
import zuo.biao.library.util.CommonUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 地址选择器
 */
public class PlacePickWindow {
    Context mContext;
    CommonDialog popupDialog;
    String oneSelectStr;
    String towSelectStr;
    Text oneShow;
    Text towShow;
    Builder builder;
    Picker pickerOne;
    Picker pickerTow;
    List<PlaceBean> oneList;
    Map<String, List<PlaceBean>> towListMap;
    String[] oneStrs;
    String[] towStrs;
    private Button btnCancel;
    private Button btnSure;
    private Component component;

    /**启动这个Activity的Intent
     * @param context
     * @return
     */
    public static Intent createIntent(Context context, List<PlaceBean> placeBeans ,Map<String, List<PlaceBean>> towPlaceMap,PlacePickWindow.OnSureSelectListener listener) {
        new PlacePickWindow.Builder(context)
                .setData(placeBeans, towPlaceMap)
                .setSelectListener(listener)
                .show();
        return new Intent();
    }


    public PlacePickWindow(Builder builder) {
        this.builder = builder;
        this.mContext = builder.context;
        this.oneList = builder.oneSelectData;
        this.towListMap = builder.towSelectData;
        init();
    }

    /**
     * 初始化
     *
     * @return v
     */
    private PlacePickWindow init() {
        component =
                LayoutScatter.getInstance(mContext)
                        .parse(ResourceTable.Layout_ui_layout_pick_place, new DirectionalLayout(mContext), true);

        oneShow = (Text) component.findComponentById(ResourceTable.Id_txt_one_show);
        towShow = (Text) component.findComponentById(ResourceTable.Id_txt_tow_show);
        pickerOne = (Picker) component.findComponentById(ResourceTable.Id_picker_one);
        pickerTow = (Picker) component.findComponentById(ResourceTable.Id_picker_tow);
        btnCancel = (Button) component.findComponentById(ResourceTable.Id_btn_cancel);
        btnSure = (Button) component.findComponentById(ResourceTable.Id_btn_sure);

        initViewData();
        initListener();

        popupDialog =
                new CommonDialog(
                        mContext);
        popupDialog.setAlignment(LayoutAlignment.BOTTOM);
        popupDialog.setContentCustomComponent(component);
        popupDialog.setSize(ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_CONTENT);
        popupDialog.setAutoClosable(false);

        return this;
    }

    private void initViewData() {
        ShapeElement shape = new ShapeElement();
        shape.setShape(ShapeElement.RECTANGLE);
        shape.setRgbColor(RgbColor.fromArgbInt(0xFFEEEEEE));
        pickerOne.setDisplayedLinesElements(shape, shape);
        pickerTow.setDisplayedLinesElements(shape, shape);
        pickerOne.setWheelModeEnabled(true);
        pickerTow.setWheelModeEnabled(true);
        List<String> strOnes = new ArrayList<>();
        for (PlaceBean bean : oneList) {
            strOnes.add(bean.name);
        }
        oneStrs = strOnes.toArray(new String[strOnes.size()]);
        pickerOne.setValue(0);
        pickerOne.setDisplayedData(oneStrs);
        oneSelectStr = oneStrs[0];
        if (towListMap != null && towListMap.size() > 0) {
            List<PlaceBean> placeBeansBean = towListMap.get(oneList.get(0).code);
            List<String> towListStrs = new ArrayList<>();
            for (PlaceBean bean : placeBeansBean) {
                towListStrs.add(bean.name);
            }
            towStrs = towListStrs.toArray(new String[towListStrs.size()]);
            pickerTow.setDisplayedData(towStrs);
            pickerTow.setValue(0);
            towSelectStr = towStrs[0];
        }
    }

    private void initListener() {
        pickerOne.setValueChangedListener(
                new Picker.ValueChangedListener() {
                    @Override
                    public void onValueChanged(Picker picker, int i, int i1) {
                        if (oneStrs.length <= i1) {
                            return;
                        }
                        oneSelectStr = oneStrs[i1];

                        setOneText(oneSelectStr);
                    }
                });

        pickerTow.setValueChangedListener(
                new Picker.ValueChangedListener() {
                    @Override
                    public void onValueChanged(Picker picker, int i, int i1) {
                        if (towStrs.length <= i1) {
                            return;
                        }

                        towSelectStr = towStrs[i1];
                        setTowText(towSelectStr);
                    }
                });

        btnSure.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        builder.sureSelect.select(oneSelectStr, towSelectStr);
                        popupDialog.hide();
                        popupDialog.destroy();
                    }
                });
        btnCancel.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        popupDialog.hide();
                        popupDialog.destroy();
                    }
                });
    }

    /**
     * 展示
     *
     * @return v
     */
    public PlacePickWindow show() {
        popupDialog.show();
        return this;
    }

    private void setOneText(String oneSelectStr) {
        String code = "";
        for (int i = 0; i < oneList.size(); i++) {
            if (oneList.get(i).name.equals(oneSelectStr)) {
                code = oneList.get(i).code;
            }
        }
        List<PlaceBean> placeBeansBean = towListMap.get(code);
        List<String> towListStrs = new ArrayList<>();
        for (PlaceBean bean : placeBeansBean) {
            towListStrs.add(bean.name);
        }

        towStrs = towListStrs.toArray(new String[towListStrs.size()]);

        new EventHandler(EventRunner.getMainEventRunner())
                .postTask(
                        new Runnable() {
                            @Override
                            public void run() {
                                oneShow.setText(oneSelectStr);
                                pickerTow.setDisplayedData(towStrs);
                            }
                        });
    }

    private void setTowText(String towSelectStr) {
        new EventHandler(EventRunner.getMainEventRunner())
                .postTask(
                        new Runnable() {
                            @Override
                            public void run() {
                                towShow.setText(towSelectStr);
                            }
                        });
    }

    /**
     * builder 模式
     */
    public static class Builder {
        Context context;
        List<PlaceBean> oneSelectData;
        Map<String, List<PlaceBean>> towSelectData;
        OnSureSelectListener sureSelect;

        public Builder(Context context) {
            this.context = context;
        }

        /**
         * 设置数据
         *
         * @param oneSelectData one
         * @param towSelectData tow
         * @return v
         */
        public PlacePickWindow.Builder setData(
                List<PlaceBean> oneSelectData, Map<String, List<PlaceBean>> towSelectData) {
            this.oneSelectData = oneSelectData;
            this.towSelectData = towSelectData;

            return this;
        }

        /**
         * 设置监听
         *
         * @param sureSelect 监听
         * @return v
         */
        public PlacePickWindow.Builder setSelectListener(OnSureSelectListener sureSelect) {
            this.sureSelect = sureSelect;
            return this;
        }

        /**
         * 根show
         */
        public void show() {
            new PlacePickWindow(this).show();
        }
    }

    /**
     * 监听接口
     */
    public interface OnSureSelectListener {
        /**
         * 选择结果
         *
         * @param oneSelect o
         * @param towSelect t
         */
        void select(String oneSelect, String towSelect);
    }
}
