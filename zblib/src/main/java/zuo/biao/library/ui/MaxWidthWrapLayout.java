/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
limitations under the License.*/

package zuo.biao.library.ui;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.app.Context;

/**限制控件最大宽度的类
 * @author Lemon
 * @use 写在xml文件中即可
 */
public class MaxWidthWrapLayout extends DirectionalLayout implements Component.EstimateSizeListener {
    public MaxWidthWrapLayout(Context context) {
        super(context);
    }

    public MaxWidthWrapLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public MaxWidthWrapLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        setEstimateSizeListener(this);
    }

    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        // Children are just made to fill our space.
        int childWidthSize = getEstimatedWidth();
        int childHeightSize = getEstimatedHeight();

        widthEstimateConfig = MeasureSpec.getMeasureSpec(childWidthSize, MeasureSpec.PRECISE);
        heightEstimateConfig = MeasureSpec.getMeasureSpec(childHeightSize, MeasureSpec.PRECISE);
        int maxWidth = (int) AttrHelper.vp2px(300, getContext());
        if (widthEstimateConfig > maxWidth) {
            widthEstimateConfig = maxWidth;
        }
        return onEstimateSize(widthEstimateConfig, heightEstimateConfig);
    }
}
