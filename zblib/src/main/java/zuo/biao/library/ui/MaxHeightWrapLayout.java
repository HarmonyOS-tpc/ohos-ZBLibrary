/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
limitations under the License.*/

package zuo.biao.library.ui;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.app.Context;

/**限制最大高度为屏幕宽度的类
 * @author Lemon
 * @use 使用方法：写在xml文件中即可
 */
public class MaxHeightWrapLayout extends DirectionalLayout implements Component.EstimateSizeListener {
    public MaxHeightWrapLayout(Context context) {
        super(context);
    }

    public MaxHeightWrapLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public MaxHeightWrapLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        setEstimateSizeListener(this);
    }

    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        widthEstimateConfig = MeasureSpec.getMeasureSpec(getEstimatedWidth(), MeasureSpec.PRECISE);
        if (heightEstimateConfig > widthEstimateConfig) { // 没用，在显示前height和width都为0
            heightEstimateConfig = widthEstimateConfig;
        }
        return onEstimateSize(widthEstimateConfig, heightEstimateConfig);
    }
}
