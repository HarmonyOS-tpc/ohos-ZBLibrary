package zuo.biao.library.ui;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.PopupDialog;
import ohos.app.Context;
import zuo.biao.library.ResourceTable;
import zuo.biao.library.base.BaseView;
import zuo.biao.library.model.BottomSheetItem;
import zuo.biao.library.model.Menu;

import java.util.List;

/**
 * 底部弹框
 */
public class BottomMenuView {
    Context mContext;
    CommonDialog popupDialog;
    Text txtHeadTitle;
    OnBottomMenuItemClickListener itemClickListener;
    ListContainer listContainer;
    List<BottomSheetItem> bottomSheetItems;
    int id = 0;

    public BottomMenuView(Ability context, int id) {
        this.mContext = context;
        this.id = id;
    }

    public BottomMenuView(Context context, List<BottomSheetItem> bottomsheetItems) {
        this.mContext = context;
        this.bottomSheetItems = bottomsheetItems;
        initView();
    }

    /**
     * 设置监听
     * @param itemClickListener item
     * @return view
     */
    public BottomMenuView setOnMenuItemClickListener(OnBottomMenuItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
        return this;
    }

    public void bindView(final List<BottomSheetItem> bottomsheetItems){
        this.bottomSheetItems = bottomsheetItems;
    }

    private void initView() {
        Component component = null;
        component =
                LayoutScatter.getInstance(mContext)
                        .parse(ResourceTable.Layout_ui_layout_bottom_view, new DirectionalLayout(mContext), true);

        txtHeadTitle = (Text) component.findComponentById(ResourceTable.Id_txt_head);

        BottomListProvider bottomListProvider = new BottomListProvider(mContext);
        listContainer.setItemProvider(bottomListProvider);
        listContainer.setItemClickedListener(
                new ListContainer.ItemClickedListener() {
                    @Override
                    public void onItemClicked(ListContainer listContainer, Component component, int pos, long l) {
                        itemClickListener.onBottomMenuItemClick(pos);
                    }
                });

        bottomListProvider.setItemData(bottomSheetItems);
        bottomListProvider.notifyDataChanged();

        popupDialog =
                new CommonDialog(
                        mContext);
        popupDialog.setAutoClosable(true);
        popupDialog.setDuration(10000);
        popupDialog.setAlignment(LayoutAlignment.BOTTOM);
        popupDialog.setContentCustomComponent(component);
        popupDialog.setSize(ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_CONTENT);
        popupDialog.setAutoClosable(false);
    }

    /**
     * 设置title
     * @param msg m
     * @return view
     */
    public BottomMenuView setTitle(String msg) {
        txtHeadTitle.setText(msg);
        return this;
    }

    /**
     * show
     * @return view
     */
    public BottomMenuView show() {
        popupDialog.show();
        return this;
    }

    /**
     * 监听
     */
    public interface OnBottomMenuItemClickListener {
        /**
         * 点击
         * @param position p
         */
        void onBottomMenuItemClick(int position);
    }
}
