package zuo.biao.library.ui;
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import ohos.agp.components.*;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.global.configuration.Configuration;
import zuo.biao.library.ResourceTable;
/**
 * ListContainerAdapter类 列表
 */
public class ListContainerAdapter extends BaseItemProvider {
    private static final String TAG = ListContainerAdapter.class.getSimpleName();
    private CommonDialog mDialog;
    private InternalListCallback mCallback;
    private Context mContext;

    ListContainerAdapter(CommonDialog dialog, Context context) {
        mDialog = dialog;
        mContext = context;
    }

    void setCallback(InternalListCallback callback) {
        mCallback = callback;
    }

    @Override
    public int getCount() {
        return mDialog.builder.items.size();
    }

    @Override
    public Object getItem(int pos) {
        return mDialog.builder.items.get(pos);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public Component getComponent(int index, Component component, ComponentContainer componentContainer) {
        if (component == null) {
            component =
                    LayoutScatter.getInstance(mContext)
                            .parse(ResourceTable.Layout_uilayouts_stack_layout_item, componentContainer, true);
        }
        onPrepareView(index, component);
        return component;
    }

    private void onPrepareView(int index, Component component) {
        Text title = (Text) component.findComponentById(ResourceTable.Id_txt_title);
        title.setText((String) getItem(index));
        component.setClickedListener(
                view -> {
                    onItemClicked(view, index);
                });
    }

    private void setupGravity(ComponentContainer view) {
        final DirectionalLayout itemRoot = (DirectionalLayout) view;
        itemRoot.setAlignment(TextAlignment.START);

        if (view.getChildCount() == 2) {
            if (isRTL() && view.getComponentAt(1) instanceof Button) {
                Button first = (Button) view.getComponentAt(1);
                view.removeComponent(first);

                Text second = (Text) view.getComponentAt(0);
                view.removeComponent(second);
                second.setPadding(
                        second.getPaddingRight(),
                        second.getPaddingTop(),
                        second.getPaddingRight(),
                        second.getPaddingBottom());

                view.addComponent(first);
                view.addComponent(second);
            }
        }
    }

    private boolean isRTL() {
        Configuration config = mDialog.builder.getContext().getResourceManager().getConfiguration();
        return config.isLayoutRTL;
    }

    private void onItemClicked(Component component, int index) {
        if (mCallback != null) {
            CharSequence text = null;
            if (mDialog.builder.items != null) {
                text = mDialog.builder.items.get(index);
            }
            mCallback.onItemSelected(mDialog, component, index, text, false);
        }
    }

    interface InternalListCallback {
        boolean onItemSelected(
                CommonDialog dialog, Component itemView, int position, CharSequence text, boolean longPress);
    }
}
