/*Copyright ©2015 TommyLemon(https://github.com/TommyLemon)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
limitations under the License.*/

package zuo.biao.library.ui;

import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.app.Context;
import zuo.biao.library.ResourceTable;
import zuo.biao.library.base.BaseAbility;
import zuo.biao.library.interfaces.OnBottomDragListener;
import zuo.biao.library.util.DataKeeper;
import zuo.biao.library.util.SettingUtil;
import zuo.biao.library.util.StringUtil;

/**服务器设置activity
 * @author Lemon
 * @use toActivity(ServerSettingActivity.createIntent(...));
 */
public class ServerSettingActivity extends BaseAbility implements Component.ClickedListener, OnBottomDragListener {

    // 启动方法<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    private static final String INTENT_NORMAL_ADDRESS = "INTENT_NORMAL_ADDRESS";
    private static final String INTENT_TEST_ADDRESS = "INTENT_TEST_ADDRESS";
    private static final String INTENT_SHARED_PREFERENCES_PATH = "INTENT_SHARED_PREFERENCES_PATH";
    private static final String INTENT_PATH_MODE = "INTENT_PATH_MODE";
    private static final String INTENT_NORMAL_KEY = "INTENT_NORMAL_KEY";
    private static final String INTENT_TEST_KEY = "INTENT_TEST_KEY";

    private static final String RESULT_NORMAL_ADDRESS = "RESULT_NORMAL_ADDRESS";
    private static final String RESULT_TEST_ADDRESS = "RESULT_TEST_ADDRESS";

    /**启动这个Activity的Intent
     * 通过setResult返回结果,而不是直接在这个界面保存设置
     * @param context context
     * @param normalAddress normalAddress
     * @param testAddress testAddress
     * @return intent
     */
    public static Intent createIntent(Context context, String normalAddress, String testAddress) {
        return createIntent(context, normalAddress, testAddress, null, 0, null, null);
    }
    /**启动这个Activity的Intent
     * 只有一个服务器
     * @param context context
     * @param address address
     * @param sharedPreferencesPath sharedPreferencesPath
     * @param pathMode pathMode
     * @param key key
     * @return intent
     */
    public static Intent createIntent(
            Context context, String address, String sharedPreferencesPath, int pathMode, String key) {
        return createIntent(context, address, null, sharedPreferencesPath, pathMode, key, null);
    }
    /**启动这个Activity的Intent
     * @param context context
     * @param normalAddress normalAddress
     * @param testAddress testAddress
     * @param sharedPreferencesPath sharedPreferencesPath
     * @param pathMode pathMode
     * @param normalKey normalKey
     * @param testKey testKey
     * @return intent
     */
    public static Intent createIntent(
            Context context,
            String normalAddress,
            String testAddress,
            String sharedPreferencesPath,
            int pathMode,
            String normalKey,
            String testKey) {
        return new Intent()
                .setParam(INTENT_NORMAL_ADDRESS, normalAddress)
                .setParam(INTENT_TEST_ADDRESS, testAddress)
                .setParam(INTENT_SHARED_PREFERENCES_PATH, sharedPreferencesPath)
                .setParam(INTENT_PATH_MODE, pathMode)
                .setParam(INTENT_NORMAL_KEY, normalKey)
                .setParam(INTENT_TEST_KEY, testKey);
    }

    // 启动方法>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    private String normalAddress;
    private String testAddress;
    private String sharedPreferencesPath;
    private int pathMode = Context.MODE_PRIVATE;
    private String normalKey;
    private String testKey;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_server_setting_ability);

        intent = getIntent();
        normalAddress = intent.getStringParam(INTENT_NORMAL_ADDRESS);
        testAddress = intent.getStringParam(INTENT_TEST_ADDRESS);
        sharedPreferencesPath = intent.getStringParam(INTENT_SHARED_PREFERENCES_PATH);
        pathMode = intent.getIntParam(INTENT_PATH_MODE, pathMode);
        normalKey = intent.getStringParam(INTENT_NORMAL_KEY);
        testKey = intent.getStringParam(INTENT_TEST_KEY);

        // 功能归类分区方法，必须调用<<<<<<<<<<
        initView();
        initData();
        initEvent();
        // 功能归类分区方法，必须调用>>>>>>>>>>
    }

    // UI显示区(操作UI，但不存在数据获取或处理代码，也不存在事件监听代码)<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    private Text tvServerSettingNormalName;
    private Text tvServerSettingTestName;

    private TextField etServerSettingNormal;
    private TextField etServerSettingTest;

    @Override
    public void initView() { // 必须调用
        tvServerSettingNormalName = findView(ResourceTable.Id_tvServerSettingNormalName);
        tvServerSettingTestName = findView(ResourceTable.Id_tvServerSettingTestName);

        etServerSettingNormal = findView(ResourceTable.Id_etServerSettingNormal);
        etServerSettingTest = findView(ResourceTable.Id_etServerSettingTest);
    }

    // UI显示区(操作UI，但不存在数据获取或处理代码，也不存在事件监听代码)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // Data数据区(存在数据获取或处理代码，但不存在事件监听代码)<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    private static final String[] SERVER_NAMES = {"正式服务器", "测试服务器"};

    @Override
    public void initData() { // 必须调用
        // 获取并显网址
        etServerSettingNormal.setText(StringUtil.getNoBlankString(normalAddress));
        etServerSettingTest.setText(StringUtil.getNoBlankString(testAddress));

        tvServerSettingNormalName.setText(SERVER_NAMES[0] + (SettingUtil.isOnTestMode == false ? "[正在使用]" : ""));
        tvServerSettingTestName.setText(SERVER_NAMES[1] + (SettingUtil.isOnTestMode ? "[正在使用]" : ""));
    }

    /**保存并退出
     * @param isTest isTest
     */
    private void saveAndExit(boolean isTest) {
        if (StringUtil.isNotEmpty(sharedPreferencesPath, true)
                && StringUtil.isNotEmpty(isTest ? testKey : normalKey, true)) {
            SettingUtil.putBoolean(SettingUtil.KEY_IS_ON_TEST_MODE, isTest);
            DataKeeper.save(
                    sharedPreferencesPath,
                    pathMode,
                    isTest ? testKey : normalKey,
                    StringUtil.getNoBlankString(isTest ? etServerSettingTest : etServerSettingNormal));
            showShortToast("已保存并切换至" + SERVER_NAMES[SettingUtil.isOnTestMode ? 1 : 0] + "，请不要退出登录。重启后生效");
        }
        setResult(
                0,
                new Intent()
                        .setParam(
                                isTest ? RESULT_TEST_ADDRESS : RESULT_NORMAL_ADDRESS,
                                StringUtil.getNoBlankString(isTest ? etServerSettingTest : etServerSettingNormal)));
        terminateAbility();
    }

    // Data数据区(存在数据获取或处理代码，但不存在事件监听代码)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // Event事件区(只要存在事件监听代码就是)<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    @Override
    public void initEvent() { // 必须调用
        findView(ResourceTable.Id_tvServerSettingNormalSet).setClickedListener(this);
        findView(ResourceTable.Id_tvServerSettingNormalOpen).setClickedListener(this);

        findView(ResourceTable.Id_tvServerSettingTestSet).setClickedListener(this);
        findView(ResourceTable.Id_tvServerSettingTestOpen).setClickedListener(this);
    }

    @Override
    public void onDragBottom(boolean rightToLeft) {
        if (rightToLeft) {
            etServerSettingNormal.setText(StringUtil.getTrimedString(SettingUtil.URL_SERVER_ADDRESS_NORMAL_HTTP));
            etServerSettingTest.setText(StringUtil.getTrimedString(SettingUtil.URL_SERVER_ADDRESS_TEST));
            return;
        }

        terminateAbility();
    }

    // 系统自带监听方法<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    @Override
    public void onClick(Component v) {
        if (v.getId() == ResourceTable.Id_tvServerSettingNormalSet) {
            saveAndExit(false);
        } else if (v.getId() == ResourceTable.Id_tvServerSettingTestSet) {
            saveAndExit(true);
        }
    }

    // 类相关监听<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    // 类相关监听>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // 系统自带监听方法>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // Event事件区(只要存在事件监听代码就是)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    // 内部类,尽量少用<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    // 内部类,尽量少用>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

}
