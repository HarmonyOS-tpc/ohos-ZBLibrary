package zuo.biao.library.ui;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.RecycleItemProvider;
import ohos.agp.components.Text;
import ohos.app.Context;
import zuo.biao.library.model.Entry;
import zuo.biao.library.util.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static ohos.agp.utils.LayoutAlignment.TOP;

/**
 * grid 适配器
 * @param <T> T
 */
public abstract class GridPickerAdapter<T> extends RecycleItemProvider {
    /**
     * TYPE_CONTNET_ENABLE
     */
    public static final int TYPE_CONTNET_ENABLE = 0;
    /**
     * TYPE_CONTNET_UNABLE
     */
    public static final int TYPE_CONTNET_UNABLE = 1;
    /**
     * TYPE_TITLE
     */
    public static final int TYPE_TITLE = 2;
    private Context context;
    public List<T> data;
    private int mLayoutId;
    private int numColumns = 1;
    private OnItemClickListener onItemClickListener;
    private int currentPosition;//初始选中位置

    private OnItemClickListener onItemSelectedListener;

    /**
     * 设置被选中监听
     * @param onItemSelectedListener 监听
     */
    public void setOnItemSelectedListener(OnItemClickListener onItemSelectedListener) {
        this.onItemSelectedListener = onItemSelectedListener;
    }

    /**
     * 获取当前位置
     * @return
     */
    public int getCurrentPosition() {
        return currentPosition;
    }

    /**
     * 获取当前位置名称
     * @return
     */
    public String getCurrentItemName() {
        return StringUtil.getTrimedString(getItem(getCurrentPosition()));
    }


    /**
     * 初始化
     * @param context   上下文
     * @param data      数据源
     * @param mLayoutId 条目的资源文件id
     * @return GridAdapter
     */
    public GridPickerAdapter(Context context, int mLayoutId, List<T> data) {
        this.context = context;
        this.data = data;
        this.mLayoutId = mLayoutId;
    }



    public List<Entry<Integer, String>> list;
    /**刷新列表
     * @param list data
     */
    public synchronized void refresh(List<Entry<Integer, String>> list) {
        this.list = list == null ? null : new ArrayList<Entry<Integer, String>>(list);
        notifyDataChanged();
    }

    /**
     * 设置列数
     * @param numColumns s
     */
    public void setNumColumns(int numColumns) {
        this.numColumns = numColumns;
    }

    /**
     * 设置数据
     * @param data d
     */
    public void setData(List<T> data) {
        this.data = data;
        notifyDataChanged();
    }

    /**
     * 获取数据
     * @return data
     */
    public List<T> getData() {
        return data;
    }

    @Override
    public int getCount() {
        if (data != null) {
            return data.size() % numColumns == 0 ? data.size() / numColumns : data.size() / numColumns + 1;
        } else {
            return 0;
        }
    }

    @Override
    public T getItem(int position) {
        if (data != null) {
            return data.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer parent) {
        ViewHolder viewHolder;
        convertComponent = new DirectionalLayout(context);
        ((DirectionalLayout) convertComponent).setOrientation(Component.HORIZONTAL);
        ComponentContainer.LayoutConfig layoutConfig = convertComponent.getLayoutConfig();
        layoutConfig.width = DependentLayout.LayoutConfig.MATCH_PARENT;
        layoutConfig.height = DependentLayout.LayoutConfig.MATCH_CONTENT;
        convertComponent.setLayoutConfig(layoutConfig);
        for (int i = 0; i < numColumns; i++) {
            if (position * numColumns + i < data.size()) {
                DirectionalLayout dlItemParent = new DirectionalLayout(context);
                dlItemParent.setLayoutConfig(
                        new DirectionalLayout.LayoutConfig(0, DirectionalLayout.LayoutConfig.MATCH_CONTENT, TOP, 1));
                Component childConvertComponent = LayoutScatter.getInstance(context).parse(mLayoutId, null, false);
                int finalI = i;
                childConvertComponent.setClickedListener(
                        new Component.ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                if (onItemClickListener != null) {
                                    onItemClickListener.onItemClick(component, position * numColumns + finalI);
                                }
                            }
                        });
                dlItemParent.addComponent(childConvertComponent);
                ((ComponentContainer) convertComponent).addComponent(dlItemParent);
                viewHolder = new ViewHolder(childConvertComponent);
                bind(viewHolder, getItem(position * numColumns + i), position * numColumns + i);
            } else {
                // 用Component占位会导致高度为MATCH_PARENT,所以此处用DirectionalLayout占位
                DirectionalLayout childConvertComponent = new DirectionalLayout(context);
                childConvertComponent.setLayoutConfig(
                        new DirectionalLayout.LayoutConfig(0, DirectionalLayout.LayoutConfig.MATCH_CONTENT, TOP, 1));
                ((ComponentContainer) convertComponent).addComponent(childConvertComponent);
            }
        }
        return convertComponent;
    }

    /**
     * 设置item监听
     * @param onItemClickListener onItemClickListener
     */
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    /**
     * view绑定数据
     * @param holder h
     * @param s s
     * @param position p
     */
    protected abstract void bind(ViewHolder holder, T s, int position);

    /**
     * 自定义viewhoder
     */
    protected static class ViewHolder {
        HashMap<Integer, Component> mViews = new HashMap<>();
        /**
         * 根view
         */
        public Component itemView;

        ViewHolder(Component component) {
            this.itemView = component;
        }

        /**
         * 设置text
         * @param viewId i
         * @param text t
         * @return v
         */
        public ViewHolder setText(int viewId, String text) {
            ((Text) getView(viewId)).setText(text);
            return this;
        }

        /**
         * 获取view
         * @param viewId id
         * @param <E> e
         * @return view
         */
        public <E extends Component> E getView(int viewId) {
            E view = (E) mViews.get(viewId);
            if (view == null) {
                view = (E) itemView.findComponentById(viewId);
                mViews.put(viewId, view);
            }
            return view;
        }
    }

    /**
     * 点击监听
     */
    public abstract static class OnItemClickListener {
        /**
         * 点击监听
         * @param component c
         * @param position p
         */
        public abstract void onItemClick(Component component, int position);
    }
}
