# Ohos_ZBLibrary 快速开发框架

Ohos_ZBLibrary 架构，提供一套开发标准（View, Data, Event）以及模板和工具类并规范代码。封装层级少，简单高效兼容性好。OKHttp 网络请求、Glide 图片加载、ZXing 二维码、自动缓存以及各种 Base、Demo、UI、Util 直接用。

## 概述
用 [BaseView]，自定义 View 竟然如此简单;
用 [Entry<K, V>]，两个变量的 Model/JavaBean 再也不用写了;
用 [BaseHttpListActivity]，几行代码搞定 HTTP 请求列表 加载和缓存;
一行搞定 View 属性，一键统一配置UI...
点击右边链接查看如何使用 http://my.oschina.net/u/2437072/blog/665241

## 演示
<img src="screenshot/Screenshot_1.jpg" height="30%" width="30%"/><img src="screenshot/Screenshot_2.jpg" height="30%" width="30%"/><img src="screenshot/Screenshot_3.jpg" height="30%" width="30%"/>
<img src="screenshot/Screenshot_4.jpg" height="30%" width="30%"/><img src="screenshot/Screenshot_5.jpg" height="30%" width="30%"/><img src="screenshot/Screenshot_6.jpg" height="30%" width="30%"/>
<img src="screenshot/Screenshot_7.jpg" height="30%" width="30%"/><img src="screenshot/Screenshot_8.jpg" height="30%" width="30%"/><img src="screenshot/Screenshot_9.jpg" height="30%" width="30%"/>
<img src="screenshot/Screenshot_10.jpg" height="30%" width="30%"/><img src="screenshot/Screenshot_11.jpg" height="30%" width="30%"/><img src="screenshot/Screenshot_12.jpg" height="30%" width="30%"/>
<img src="screenshot/Screenshot_13.jpg" height="30%" width="30%"/><img src="screenshot/Screenshot_14.jpg" height="30%" width="30%"/><img src="screenshot/Screenshot_15.jpg" height="30%" width="30%"/>
<img src="screenshot/Screenshot_16.jpg" height="30%" width="30%"/><img src="screenshot/Screenshot_17.jpg" height="30%" width="30%"/><img src="screenshot/Screenshot_18.jpg" height="30%" width="30%"/>
<img src="screenshot/Screenshot_19.jpg" height="30%" width="30%"/><img src="screenshot/Screenshot_20.jpg" height="30%" width="30%"/><img src="screenshot/Screenshot_21.jpg" height="30%" width="30%"/>

## 集成配置
通过DevEco studio 2.0+,并下载SDK Native 版本2.0+
方式一
在entry的gradle中集成如下操作：
修改implementation fileTree(dir: 'libs', include: ['*.jar','*.har'])增加*.har

在simple有使用其他har包，在simple demo entry工程下libs中需要的har如下，如需运行请到对应开源组件去获取，或删除对应功能代码

glide:glide.har；

ImagePicker:imagepicker.har；

PhotoView:photo_view.har；

UniversalImageLoader:universal_imageloader.har

或者
```
implementation "io.openharmony.tpc.thirdlib:ImagePicker:1.0.0"
implementation 'io.openharmony.tpc.thirdlib:ohos-Universal-Image-Loader:1.0.2'
implementation 'io.openharmony.tpc.thirdlib:glide:1.0.4'
```

Notice：在zblib库中已经引入其他三方开源如下

implementation 'com.alibaba:fastjson:1.2.70'

implementation 'com.squareup.okhttp3:okhttp:3.3.1'

implementation 'org.jetbrains:annotations:15.0'

方式二

allprojects{

    repositories{

        mavenCentral()
            
    }

}
```
implementation 'io.openharmony.tpc.thirdlib:ohos-zblibrary-qrcode:1.0.4'
implementation 'io.openharmony.tpc.thirdlib:ohos-zblibrary-zblib:1.0.4'
```

### 二维码
1，调用二维码扫描的两种方式
a，继承抽象类ScanAbility extends CaptureAbility，可以自定义布局。
b，使用默认布局，只需跳转到QRLibrary中DefaultLayoutAbility去。
```
        Intent intent = new Intent();
        Operation operationBuilder =
                new Intent.OperationBuilder()
                        .withAbilityName("com.zxing.ability.DefaultLayoutAbility")
                        .withDeviceId("")
                        .withBundleName(getBundleName())
                        .build();

        intent.setParam("isShowTopBar", true); // 设置顶部栏是否显示，不传默认显示，false不显示
        intent.setOperation(operationBuilder);
        startAbilityForResult(intent, CODE);
```
在entry中的config.json中添加如下代码：

```
{
        "orientation": "portrait",
        "formEnabled": false,
        "name": "com.zxing.ability.DefaultLayoutAbility",
        "label": "QRCodeLibrary",
        "type": "page",
        "launchType": "standard"
}
```
暂不支持横屏扫描

2,生成二维码图片
```
 PixelMap pixelMap = null;
        try {
            pixelMap = EncodingHandler.createQRCode(textContent, 400);
        } catch (WriterException e) {
            e.printStackTrace();
        }
        image.setPixelMap(pixelMap);
```

### ZBLib库部分功能指导（由于是工具库，太多功能，这里只提及部分功能）
1，集成BaseAbility
```
public class ZxingAbility extends BaseAbility {
    @Override
      protected void onStart(Intent intent) {
    setUIContent(ResourceTable.Layout_ability_zxing);

    initView();
    initData();
    initEvent();
    }
}
```

2,使用Http功能
```
//初始化
HttpManager.init(getContext());
//使用
String url = "https://tcc.taobao.com/cc/json/mobile_tel_segment.htm?tel=" + phone;
        Map<String, Object> map = new WeakHashMap<>();

        HttpManager.getInstance()
                .post(
                        map,
                        url,
                        123,
                        new OnHttpResponseListener() {
                            @Override
                            public void onHttpResponse(int requestCode, String resultJson, Exception e) {
                                Log.info("HttpManager", resultJson + "1");
                                settextInfo(resultJson);
                            }
                        });
```

3，dialog使用
```
new AlertDialog(getContext(),"退出登录","确定要退出登录？",
                "sure","cancel",1,new AlertDialog.OnDialogButtonClickListener(){

            @Override
            public void onDialogButtonClick(int requestCode, boolean isPositive) {
                toastMag(isPositive?"sure":"cancel");
            }
        })
                .show();
```

4,时间选择器弹框
```
 new DatePickWindow.Builder(this)
                .setSelectListener(
                        new DatePickWindow.OnSureSelectListener() {
                            @Override
                            public void select(String yearSelect, String monthSelect, String daySelect) {
                                toastMag(yearSelect + "年" + monthSelect + "月" + daySelect + "日");
                            }
                        })
                .show();
```

还有很多工具类，就不一一列举，请查看simple或者library

注册 权限和 Ability 等代码可复制 ZBLibrary 中 config.json里的 \<uses-permission/>, \<Ability/> 等相关代码。

### 生成代码
可使用 APIAuto 自动化接口管理工具来生成接口相关代码：
* 自动生成封装请求 JSON 的代码
* 自动生成解析结果 JSON 的代码
* 自动生成 Modle/JavaBean


### 编程思想
* 能复制就复制，节约时间避免出错
* 保留原本结构，简单上手容易调试
* 增加必要注释，说明功能和使用方法
* 说明随手可得，不用上网或打开文档
* 命名必须规范，容易查找一看就懂
* 重载尽量转发，减少代码容易修改
* 最先校验参数，任意调用不会崩溃
* 代码模块分区，方便浏览容易查找
* 封装常用代码，方便使用降低耦合
* 回收多余占用，优化内存提高性能
* 分包结构合理，模块清晰浏览方便
* 多用工具和快捷键，增删改查快捷高效

### 问题
1,大部分功能已经实现，但是因为系统的原因导致部分api和功能无法实现，需后期迭代。
2,entry simple中的布局和demo,并没有按照原先UI一致。因为需要其他三方库的限制和工作量极其庞大，导致只是展示大部分重要功能的demo。
3,一部分功能的UI与原功能的UI有一定的差异，因为当时系统组件的限制导致，无法对UI一致。
4,涉及到的类以及原因，请查看ChangeLog.md


## 许可证

    版权所有2020-2025
    根据Apache许可证2.0版（“许可证”）获得许可；
    除非符合许可证，否则不得使用此文件。
    您可以从以下网址获取许可证副本：

    http://www.apache.org/licenses/LICENSE-2.0

    除非适用法律要求或书面同意，否则软件
    根据许可证发放，按“原样”发放，
    没有任何明示或暗示的保证或条件。
    请参阅许可证以了解特定语言控制权限和
    许可证的限制。


